//
//  SubCategoryCell.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit
import Cosmos

class SubCategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblCompName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblBought: UILabel!
    
}
