//
//  HomeCell.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    
}
