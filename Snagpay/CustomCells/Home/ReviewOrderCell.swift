//
//  ReviewOrderCell.swift
//  Snagpay
//
//  Created by Apple on 17/03/21.
//

import UIKit

class ReviewOrderCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnQty: UILabel!
    @IBOutlet weak var stackview: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
