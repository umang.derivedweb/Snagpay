//
//  SenderChatCell.swift
//  Snagpay
//
//  Created by Apple on 11/03/22.
//

import UIKit

class SenderChatCell: UITableViewCell {

    @IBOutlet weak var viewBack: RoundedEndsSender!
    @IBOutlet weak var lblSeen: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
