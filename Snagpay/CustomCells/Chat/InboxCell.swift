//
//  InboxCell.swift
//  Snagpay
//
//  Created by Apple on 11/03/22.
//

import UIKit

class InboxCell: UITableViewCell {

    @IBOutlet weak var viewRead: UIView!
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblMessageCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
