//
//  PhysicalLocationCell.swift
//  Snagpay
//
//  Created by Apple on 22/10/21.
//

import UIKit

class PhysicalLocationCell: UITableViewCell {

    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblWorkphone: UILabel!
    @IBOutlet weak var txtWorkphone: UITextField!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var lblPostcode: UILabel!
    @IBOutlet weak var txtPostcode: UITextField!
    @IBOutlet weak var lblEIN: UILabel!
    @IBOutlet weak var txtEIN: UITextField!
    @IBOutlet weak var lblEntityName: UILabel!
    @IBOutlet weak var txtEntityName: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
