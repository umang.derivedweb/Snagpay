//
//  FeedbackCell.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit
import Cosmos

class FeedbackCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblFeedback: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    
}
