//
//  DiscountCell.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class DiscountCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        (self.viewWithTag(101)!).createDottedLine(width: 2.0, color: UIColor.lightGray.cgColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
