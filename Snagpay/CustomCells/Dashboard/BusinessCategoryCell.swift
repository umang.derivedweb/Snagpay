//
//  BusinessCategoryCell.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class BusinessCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}
