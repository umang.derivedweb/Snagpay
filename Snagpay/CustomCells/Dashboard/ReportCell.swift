//
//  ReportCell.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.borderWidth = 1
        viewBg.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
