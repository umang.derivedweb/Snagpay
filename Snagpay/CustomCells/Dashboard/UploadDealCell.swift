//
//  UploadDealCell.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class UploadDealCell: UITableViewCell {

    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblDeal: UILabel!
    @IBOutlet weak var txtDealName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var btnMaxDays: UIButton!
    @IBOutlet weak var txtEgiftsAvailable: UITextField!
    @IBOutlet weak var btnEgiftsPurchased: UIButton!
    @IBOutlet weak var txtMaxGifts: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
