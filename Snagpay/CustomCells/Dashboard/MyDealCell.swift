//
//  MyDealCell.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class MyDealCell: UITableViewCell {

    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSold: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRedeemed: UILabel!
    @IBOutlet weak var lblAvail: UILabel!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
