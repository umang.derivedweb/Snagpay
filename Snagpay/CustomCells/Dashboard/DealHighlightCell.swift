//
//  DealHighlightCell.swift
//  Snagpay
//
//  Created by Apple on 20/09/21.
//

import UIKit

class DealHighlightCell: UITableViewCell {

    @IBOutlet weak var lblDealName: UILabel!
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var lblCharCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtview.layer.borderWidth = 1
        txtview.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
