//
//  PhotoCell.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
}
