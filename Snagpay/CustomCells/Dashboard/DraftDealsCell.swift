//
//  DraftDealsCell.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class DraftDealsCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnReport1: UIButton!
    @IBOutlet weak var btnReport2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
