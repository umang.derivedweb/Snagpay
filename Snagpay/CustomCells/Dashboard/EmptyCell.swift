//
//  EmptyCell.swift
//  Snagpay
//
//  Created by Apple on 12/11/21.
//

import UIKit

class EmptyCell: UICollectionViewCell {
    
    
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        viewBg.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            viewBg.layer.borderWidth = 1
    }
}
