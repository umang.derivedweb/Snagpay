//
//  AlphabetsCell.swift
//  Snagpay
//
//  Created by Apple on 15/12/21.
//

import UIKit

class AlphabetsCell: UITableViewCell {

    @IBOutlet weak var lbLetter: UILabel!
    @IBOutlet weak var lblPurchases: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.shadowToView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
