//
//  PaymentMethodCell.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit

class PaymentMethodCell: UITableViewCell {

    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var btnDefault: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
