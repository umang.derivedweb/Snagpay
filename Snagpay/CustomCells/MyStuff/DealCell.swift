//
//  DealCell.swift
//  Snagpay
//
//  Created by Apple on 24/08/21.
//

import UIKit

class DealCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var txtAmountRemaining: UITextField!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var lblRedeemedAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
