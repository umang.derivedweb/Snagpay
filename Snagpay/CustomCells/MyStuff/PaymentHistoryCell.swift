//
//  PaymentHistoryCell.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit

class PaymentHistoryCell: UITableViewCell {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblDealName: UILabel!
    @IBOutlet weak var lblTransactionType: UILabel!
    @IBOutlet weak var lblPaidUsing: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTransactionTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
