//
//  GiveGiftCell.swift
//  Snagpay
//
//  Created by Apple on 30/11/21.
//

import UIKit

class GiveGiftCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblRedeemedAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
