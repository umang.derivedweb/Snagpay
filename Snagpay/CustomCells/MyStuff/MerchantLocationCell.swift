//
//  MerchantLocationCell.swift
//  Snagpay
//
//  Created by Apple on 09/02/22.
//

import UIKit

class MerchantLocationCell: UITableViewCell {

    
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblEntityName: UILabel!
    @IBOutlet weak var lblEIN: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblZip: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
