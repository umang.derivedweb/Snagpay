//
//  CreditLineCell.swift
//  Snagpay
//
//  Created by Apple on 11/12/21.
//

import UIKit

class CreditLineCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnDealName: UIButton!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
