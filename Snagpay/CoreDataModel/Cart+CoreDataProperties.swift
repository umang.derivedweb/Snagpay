//
//  Cart+CoreDataProperties.swift
//  
//
//  Created by Apple on 19/03/21.
//
//

import Foundation
import CoreData


extension Cart {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cart> {
        return NSFetchRequest<Cart>(entityName: "Cart")
    }

    @NSManaged public var deal_id: String?
    @NSManaged public var descriptions: String?
    @NSManaged public var deal_image: String?
    @NSManaged public var title: String?
    @NSManaged public var bought: String?
    @NSManaged public var price: String?
    @NSManaged public var deal_option_id: String?
    @NSManaged public var qty: String?

}
