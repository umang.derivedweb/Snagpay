//
//  UniversalPicker.swift
//  FashionHouse
//
//  Created by Apple on 09/09/21.
//

import Foundation
import UIKit

class  SingletonPicker:NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    
    static let shared = SingletonPicker()
    
    var arrTitle:[String]?
    var arrId:[String]?
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    var dictSelectedItem:[String:String] = [:]
    
    
    func showPicker(vc:UIViewController, arrTitle:[String], arrId:[String]) {
        dictSelectedItem = [:]
        self.arrTitle = arrTitle
        self.arrId = arrId
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        vc.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        vc.view.addSubview(toolBar)
    }
    
    @objc func onDoneButtonTapped() {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        if dictSelectedItem.isEmpty == true {
            return
        }
        NotificationCenter.default.post(name: Notification.Name("PickerIdentifier"), object: dictSelectedItem)
        
    }
    @objc func onCancelButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrTitle?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrTitle?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        dictSelectedItem["name"] = arrTitle?[row]
        dictSelectedItem["id"] = arrId?[row]
    }
}
