//
//  Helper.swift
//  Hungriji
//
//  Created by 360Technosoft on 04/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVKit
import CoreData
import JJFloatingActionButton
import Firebase

class Helper: NSObject {

    static let shared = Helper()
    
    let stripe_key = "pk_test_51KCVrbL0yzqBJi9nUyNrl2hdT8YjSQuKQUzCwuIki6jSaJYWiShabqlzzhbbrP0JBQVl5yuMzCJLbpsdFaLQ7C2Z00QyGWbfPx"
    
    var userId = ""
    var api_token = ""
    var email = ""
    var strCity = ""
    var strCityId = ""
    var strAvailableCredits = ""
    var arrCategories:[Int] = []
    var arrCities:[Int] = []
    var strCatId = ""
    var strBusinessName = ""
    var strUsername = ""
    
    var arrNamePreview:[String] = []
    var arrPricePreview:[String] = []
    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    func showHUD() {
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setBackgroundLayerColor(UIColor.black.withAlphaComponent(0.5))
    }
    func hideHUD() {
        
        SVProgressHUD.dismiss()
    }
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
    func logoutFromApp() {
        
        if Auth.auth().currentUser?.uid == "" || Auth.auth().currentUser?.uid == nil {
            return
        }
        Constants.refs.databaseUsers.child(Auth.auth().currentUser!.uid).child("online_status").setValue("offline")
        
        do{
            try Auth.auth().signOut()
        }catch{ }
        
        UserDefaults.standard.set(false, forKey: "isLogin")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(nil,forKey: "city")
        UserDefaults.standard.synchronize()
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "navMain") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = obj
        return
    }
    
    func convertDateFormat(date:String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newdate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: newdate!)
    }
    
    func getCurrentTime(date:Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let newdate = dateFormatter.date(from: dateFormatter.string(from: date))
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: newdate!)
    }
    func myPickerView(vc:UIViewController, txtfield:UITextField)->UIPickerView {
        
        let picker = UIPickerView(frame:CGRect(x: 0, y: 0, width: vc.view.frame.size.width, height: 216))
        picker.delegate = vc as? UIPickerViewDelegate
        picker.dataSource = vc as? UIPickerViewDataSource
        picker.backgroundColor = UIColor.white
        txtfield.inputView = picker
        return picker
        
    }
    
    func changeDateFormat(fromFormat:String,toFormat:String, strDate:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        //dateFormatter.locale = Locale(identifier: "your_loc_id")
        let date = dateFormatter.date(from: strDate)

        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: date!)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func saveToCoredata(dict:DealsDetailsData, strDealOptionId:String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        // 1
        let context = appDelegate.persistentContainer.viewContext
        // 2
        let mediumArticle = Cart(context: context)
        
        // 3
        mediumArticle.title = dict.deal_details?.title
        mediumArticle.descriptions = dict.deal_details?.description
        mediumArticle.price = "\(dict.deal_details?.sell_price ?? 0)"
        mediumArticle.bought = dict.deal_details?.bought
        mediumArticle.deal_id = "\(dict.deal_details?.deal_id ?? 0)"
        mediumArticle.deal_image = dict.deal_details?.deal_image
        mediumArticle.qty = "1"
        mediumArticle.deal_option_id = strDealOptionId
        // 4
        appDelegate.saveContext()
    }
    func fetchFromCoredata() -> [Cart] {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return []
            }

            let context = appDelegate.persistentContainer.viewContext

            // 1
            let request: NSFetchRequest<Cart> = Cart.fetchRequest()

            // 2
    //        request.predicate = NSPredicate(format: "title = %@", "MEDIUM")
            do {
                // 3
                let articles = try context.fetch(request)

                // 4
                articles.forEach { article in
                    guard
                        let title = article.title
                    else {
                        fatalError("This was not supposed to happen")
                    }

                    print(title)
                }

                return articles
            }  catch {
                fatalError("This was not supposed to happen")
            }
        
        return []
    }
    func deleteFromCoredata(dict:Cart) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        context.delete(dict)
        appDelegate.saveContext()
    }
    func updateInCoredata(dict:Cart, qty:String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = appDelegate.persistentContainer.viewContext
        
        // 1
        let request: NSFetchRequest<Cart> = Cart.fetchRequest()
        
        request.predicate = NSPredicate(format: "deal_id = %@", "\(dict.deal_id ?? "")")
        
        let results = try? context.fetch(request)

         if results?.count != 0 {
            // here you are updating
            let value = results?.first
            value?.qty = qty
         }

        appDelegate.saveContext()
    }
}


enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad   // iPad style UI (also includes macOS Catalyst)
}
