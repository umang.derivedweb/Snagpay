//
//  Constants.swift
//  Hungriji
//
//  Created by 360Technosoft on 04/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase



struct URLs {
    static let baseurl = "http://chessmafia.com/php/snagpay/web/api/"
    
    static let getCities = baseurl + "get-cities"
    static let get_categories = baseurl + "get-categories"
    static let register = baseurl + "register"
    static let login = baseurl + "login"
    static let check_otp = baseurl + "check-otp"
    static let forgot_password = baseurl + "forgot-password"
    static let new_password = baseurl + "new-password"
    static let login_with_facebook = baseurl + "login-with-facebook"
    static let login_with_google = baseurl + "login-with-google"
    static let category_details = baseurl + "category-details"
    static let deal_details = baseurl + "deal-details"
    static let remove_wishlist_deals = baseurl + "remove-wishlist-deals"
    static let add_to_wishlist = baseurl + "add-to-wishlist"
    static let get_wishlist = baseurl + "get-wishlist"
    static let categories = baseurl + "categories"
    static let review_your_order = baseurl + "review-your-order"
    static let create_order = baseurl + "create-order"
    static let get_shipping_addresses = baseurl + "get-shipping-addresses"
    static let add_shipping_address = baseurl + "add-shipping-address"
    static let get_states = baseurl + "get-states?country_id=1"
    static let set_default_shipping_address = baseurl + "set-default-shipping-address"
    static let edit_shipping_address = baseurl + "edit-shipping-address"
    static let delete_shipping_address = baseurl + "delete-shipping-address"
    static let credit_available_in_wallet = baseurl + "credit-available-in-wallet"
    static let get_my_snagpay_deals = baseurl + "get-my-snagpay-deals"
    static let current_order_history = baseurl + "current-order-history"
    static let completed_order_history = baseurl + "completed-order-history"
    static let canceled_order_history = baseurl + "canceled-order-history"
    static let recent_payment_history = baseurl + "recent-payment-history"
    static let monthly_payment_history = baseurl + "monthly-payment-history?"
    static let monthly_payment_download = baseurl + "monthly-payment-download?"
    static let get_my_wishlist = baseurl + "get-my-wishlist"
    static let manage_my_wishlist = baseurl + "manage-my-wishlist"
    static let change_password = baseurl + "change-password"
    static let get_snagpay_wallet = baseurl + "get-snagpay-wallet"
    static let change_notification_settings = baseurl + "change-notification-settings"
    static let get_notification_settings = baseurl + "get-notification-settings"
    static let get_sub_categories = baseurl + "get-sub-categories?sub_category_id="
    static let e_gift_cards = baseurl + "e-gift-cards"
    static let redeem_e_gift_card = baseurl + "redeem-e-gift-card"
    static let e_gift_card_transactions = baseurl + "e-gift-card-transactions"
    static let full_order_details = baseurl + "view-full-order-details?order_id="
    static let order_details = baseurl + "order-details?e_gift_card_id="
    static let order_cancel = baseurl + "order-cancel"
    
    static let get_my_trading_history = baseurl + "get-my-trading-history"
    static let get_profile = baseurl + "get-profile"
    static let update_profile = baseurl + "update-profile"
    static let my_deals = baseurl + "my-deals?status="
    static let draft_deals = baseurl + "draft-deals"
    static let draft_deal_delete = baseurl + "draft-deal-delete?deal_id="
    static let sell_report = baseurl + "sell-report"
    static let send_sell_report = baseurl + "send-sell-report"
    static let buy_report = baseurl + "buy-report"
    static let send_buy_report = baseurl + "send-buy-report"
    static let summary_report = baseurl + "summary-report"
    static let send_summary_report = baseurl + "send-summary-report"
    static let monthly_statement = baseurl + "monthly-statement"
    static let send_monthly_statement = baseurl + "send-monthly-statement"
    static let my_deals_download = baseurl + "my-deals-download"
    static let create_deal = baseurl + "create-deal"
    static let update_deal = baseurl + "update-deal"
    static let save_draft = baseurl + "save-draft"
    static let deal_draft_details = baseurl + "deal-draft-details?deal_id="
    static let get_global_notifications = baseurl + "get-global-notifications"
    static let goods_service_e_gift_cards = baseurl + "goods-service-e-gift-cards"
    static let get_merchants = baseurl + "get-merchants?search="
    static let give_gifts = baseurl + "give-gifts"
    static let scan_e_gift = baseurl + "scan-e-gift"
    static let get_my_merchants = baseurl + "get-my-merchants"
    static let credit_line_history = baseurl + "credit-line-history"
    static let get_merchant_alphabets = baseurl + "get-merchant-alphabets?category_id="
    static let add_deal_review = baseurl + "add-deal-review"
    static let change_city = baseurl + "change-city"
    static let search_deals = baseurl + "search-deals"
    static let get_user_locations = baseurl + "get-user-locations"
    static let get_user_location = baseurl + "get-user-location"
    static let add_user_location = baseurl + "add-user-location"
    static let edit_user_location = baseurl + "edit-user-location"
    static let delete_user_location = baseurl + "delete-user-location"
    static let e_gift_cards_from_location = baseurl + "e-gift-cards-from-location"
    static let qr_sales_report = baseurl + "qr-sales-report"
    static let stripe_delete_card = baseurl + "stripe-delete-card"
    static let stripe_get_credit_cards = baseurl + "stripe-get-credit-cards"
    static let stripe_add_card = baseurl + "stripe-add-card"
    static let delete_my_deal = baseurl + "delete-my-deal"
    static let stripe_make_default_card = baseurl + "stripe-make-default-card"
}

struct Message {
    
    static let enterValidEmail = "Please enter valid Email"
    static let enterEmail = "Please enter Email"
    static let enterPassword = "Please enter password"
    static let confirmPassword = "Please confirm password"
    static let passwordUnmatched = "Password doesn't match"
    static let enterName = "Please enter Name"
    static let enterValidNumber = "Please enter Valid Mobile Number"
    static let enterNumber = "Please enter Mobile Number"
    static let tickBox = "Please tick the box"
    static let reqFields = "Please fill all the fields"
    static let checkBox = "Please check the above box to continue"
    static let addAddress = "Please add address to continue"
    static let selectAddress = "Please select address to continue"
    static let ssSaved = "Screenshot saved in Gallery"
    static let pdfSavedSuccess = "pdf successfully saved!"
    static let pdfSavedFailure = "Pdf could not be saved"
    static let checkBoxCreditline = "Please check the credit line box to continue"
    static let noDealsAvailable = "No deals available"
    static let yourDeal = "It's your deal"
    static let invalidCard = "Invalid card"
}


struct Constants
{
    struct refs
    {
        static let databaseRoot = Database.database().reference()
        
        static let databaseChat = databaseRoot.child("Chat")
        static let databaseUsers = databaseRoot.child("Users")
        static let databaseInbox = databaseRoot.child("Inbox")
        
        static let storageMedia = Storage.storage().reference()
    }
}
