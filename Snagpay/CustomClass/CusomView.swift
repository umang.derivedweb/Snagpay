//
//  CusomView.swift
//  ChatDemo
//
//  Created by 360Technosoft on 17/02/20.
//  Copyright © 2020 360Technosoft. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class RoundedEndsSender: UIView {

    override func layoutSubviews() { setup() } // "layoutSubviews" is best

    func setup() {
        let r = 15
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [.topLeft, .topRight, .bottomLeft],
        cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

@IBDesignable
class RoundedEndsReceiver: UIView {

    override func layoutSubviews() { setup() } // "layoutSubviews" is best

    func setup() {
        let r = 15
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [.topLeft, .topRight, .bottomRight],
        cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

class RoundedEndsSenderImage: UIView {

    override func layoutSubviews() { setup() } // "layoutSubviews" is best

    func setup() {
        let r = 15
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [.bottomRight, .topLeft, .bottomLeft],
        cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

@IBDesignable
class RoundedEndsReceiverImage: UIView {

    override func layoutSubviews() { setup() } // "layoutSubviews" is best

    func setup() {
        let r = 15
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [.bottomRight, .topRight, .bottomLeft],
        cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
