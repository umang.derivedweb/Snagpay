//
//  TravelHeaderView.swift
//  Snagpay
//
//  Created by Apple on 09/08/21.
//

import UIKit

class TravelHeaderView: UICollectionReusableView {

    var arrCitiesData:[PopularCityData]?
    
    @IBOutlet weak var collview: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collview.dataSource = self
        collview.delegate = self
        
        collview.register(UINib(nibName: "TravelHeaderCell", bundle: nil), forCellWithReuseIdentifier: "TravelHeaderCell")
    }
    
}
extension TravelHeaderView:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrCitiesData?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TravelHeaderCell", for: indexPath) as! TravelHeaderCell
        
        cell.lblTitle.text = arrCitiesData?[indexPath.row].city_name
        cell.img.sd_setImage(with: URL(string: arrCitiesData?[indexPath.row].city_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 167)
    }
    
}
