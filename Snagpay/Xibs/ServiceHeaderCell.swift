//
//  ServiceHeaderCell.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit

class ServiceHeaderCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
