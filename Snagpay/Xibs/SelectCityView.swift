//
//  SelectCityView.swift
//  Snagpay
//
//  Created by Apple on 21/12/21.
//

import UIKit

class SelectCityView: UIView {
    
    var arrCities:[GetCitiesData]?
    
    @IBOutlet weak var table: UITableView!
    
    override func awakeFromNib() {
        
        table.dataSource = self
        table.delegate = self
        table.rowHeight = 60
        table.tableFooterView = UIView()
        
        self.table.register(UINib(nibName: "AddNewCityCell", bundle: nil), forCellReuseIdentifier: "AddNewCityCell")

        getCitiesAPI()
    }
    
    

    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    
    
    
    //MARK: - Webservice
    func getCitiesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=3", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.table.reloadData()
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func changeCityAPI(cityId:String) {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["city_id"] = cityId
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.change_city, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.removeFromSuperview()
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
}
extension SelectCityView:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewCityCell") as! AddNewCityCell
        cell.lblCity.text = arrCities?[indexPath.row].city_name
        cell.btnCheckbox.isHidden = true
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        changeCityAPI(cityId:"\(arrCities?[indexPath.row].city_id ?? 0)")
        UserDefaults.standard.setValue(arrCities?[indexPath.row].city_name ?? "", forKey: "city")
        UserDefaults.standard.setValue("\(arrCities?[indexPath.row].city_id ?? 0)", forKey: "cityId")
        UserDefaults.standard.synchronize()
        
        Helper.shared.strCity = arrCities?[indexPath.row].city_name ?? ""
        Helper.shared.strCityId = "\(arrCities?[indexPath.row].city_id ?? 0)"
        
        NotificationCenter.default.post(name: Notification.Name("SetCity"), object: nil)

        
    }
    
}
