//
//  TicketsHeaderView.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit
import SDWebImage

class TicketsHeaderView: UICollectionReusableView {

    var arrSubCatData:[GetCategoryData]?
    var arrSubSubCatData:[GetCategoryData]?
    
    var arrImg = ["slide1","slide1"]
    
    var intSelectedIndexColl1:Int?
    var intSelectedIndexColl3:Int?
    
    @IBOutlet weak var collview2: UICollectionView!
    @IBOutlet weak var collview1: UICollectionView!
    @IBOutlet weak var collview3: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collview1.dataSource = self
        collview2.dataSource = self
        collview3.dataSource = self
        collview1.delegate = self
        collview2.delegate = self
        collview3.delegate = self
        
        collview1.register(UINib(nibName: "TicketCategoryCell", bundle: nil), forCellWithReuseIdentifier: "TicketCategoryCell")
        collview2.register(UINib(nibName: "TicketSliderCell", bundle: nil), forCellWithReuseIdentifier: "TicketSliderCell")
        collview3.register(UINib(nibName: "HomeServiceCell", bundle: nil), forCellWithReuseIdentifier: "HomeServiceCell")
        
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            // Put your code which should be executed with a delay here
            //self.collectionView(self.collview1, didSelectItemAt: IndexPath(row: 0, section: 0))
        }
    }
    
    @IBAction func btnSliderImage(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let collectionBounds = self.collview2.bounds
                    let contentOffset = CGFloat(floor(self.collview2.contentOffset.x - collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset, collectionview:collview2)
        } else {
            let collectionBounds = self.collview2.bounds
                    let contentOffset = CGFloat(floor(self.collview2.contentOffset.x + collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset, collectionview:collview2)
        }
    }
    @IBAction func btnSliderCategory(_ sender: UIButton) {
        if sender.tag == 1 {
            
            let collectionBounds = self.collview3.bounds
                    let contentOffset = CGFloat(floor(self.collview3.contentOffset.x - collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset, collectionview:collview3)
        } else {
            let collectionBounds = self.collview3.bounds
                    let contentOffset = CGFloat(floor(self.collview3.contentOffset.x + collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset, collectionview:collview3)
        }
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat, collectionview:UICollectionView) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : collectionview.contentOffset.y ,width : collectionview.frame.width,height : collectionview.frame.height)
        collectionview.scrollRectToVisible(frame, animated: true)
    }
    
    //MARK: - Webservice
    func getSubCategoryAPI(cat_id:Int) {
        //Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetCategoryDeals(url: "\(URLs.get_sub_categories)\(cat_id)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.arrSubSubCatData = response.data?.sub_categories
                
                self.collview3.reloadData()
                
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(self.arrSubSubCatData?[0].category_id ?? 0)"])
                
                
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }
}
extension TicketsHeaderView:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collview1 {
            return arrSubCatData?.count ?? 0
        } else if collectionView == collview2 {
            return arrImg.count
        } else {
            return arrSubSubCatData?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collview1 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicketCategoryCell", for: indexPath) as! TicketCategoryCell
            
            cell.lblTitle.text = arrSubCatData?[indexPath.row].category_name
            
            if indexPath.row == intSelectedIndexColl1 {
                cell.viewBg.backgroundColor = #colorLiteral(red: 0.002139341086, green: 0.06779965013, blue: 0.4206510782, alpha: 1)
                cell.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                cell.viewBg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            return cell
        } else if collectionView == collview2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicketSliderCell", for: indexPath) as! TicketSliderCell
            
            cell.img.sd_setImage(with: URL(string: arrImg[indexPath.row]), placeholderImage: UIImage(named: "placeholder.png"))
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeServiceCell", for: indexPath) as! HomeServiceCell
            
            cell.lblTitle.text = arrSubSubCatData?[indexPath.row].category_name
            if indexPath.row == intSelectedIndexColl3 {
                cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            } else {
                cell.lblTitle.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collview1 {
            if let cell = collview1.cellForItem(at: indexPath) as? TicketCategoryCell {
                cell.viewBg.backgroundColor = #colorLiteral(red: 0.002139341086, green: 0.06779965013, blue: 0.4206510782, alpha: 1)
                cell.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                
                
                intSelectedIndexColl1 = indexPath.row
                collview1.reloadData()
                intSelectedIndexColl3 = 0
                getSubCategoryAPI(cat_id: arrSubCatData?[indexPath.row].category_id ?? 0)
            }
            
        }
        if collectionView == collview3 {
            let cell = collview3.cellForItem(at: indexPath) as! HomeServiceCell
            cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            intSelectedIndexColl3 = indexPath.row
            
            collview3.reloadData()
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(arrSubSubCatData?[indexPath.row].category_id ?? 0)"])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collview1 {
            return CGSize(width: 135, height: 135)
        } else if collectionView == collview2 {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        } else {
            
            let label = UILabel(frame: CGRect.zero)
            label.text = arrSubSubCatData?[indexPath.row].category_name
            label.sizeToFit()
            return CGSize(width: label.frame.width+10, height: 30)
        }
    }
    
}
