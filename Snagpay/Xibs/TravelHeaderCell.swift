//
//  TravelHeaderCell.swift
//  Snagpay
//
//  Created by Apple on 09/08/21.
//

import UIKit

class TravelHeaderCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
