//
//  FoodHeaderCell.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit

class FoodHeaderCell: UICollectionViewCell {

    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
