//
//  CustomInfoWindow.swift
//  BitesFunds
//
//  Created by Apple on 24/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Cosmos

class CustomInfoWindow: UIView {

    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblBought: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBg: UIView!
    

    override func awakeFromNib() {
        
    }
}
