//
//  ReportFilterView.swift
//  Snagpay
//
//  Created by Apple on 06/12/21.
//

import UIKit

class ReportFilterView: UIView {

    var arrBtn:[UIButton]?
    var strWTD = ""
    var strMTD = ""
    var strYTD = ""
    var strFromDate = ""
    var strToDate = ""
    
    let datePicker = UIDatePicker()
    
    var txtfield:UITextField?
    
    @IBOutlet weak var btnWTD: UIButton!
    @IBOutlet weak var btnYTD: UIButton!
    @IBOutlet weak var btnMTD: UIButton!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    
    override func awakeFromNib() {
        txtFromDate.delegate = self
        txtToDate.delegate = self
        arrBtn = [btnWTD, btnYTD, btnMTD]
        showDatePicker()
    }
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtFromDate.inputAccessoryView = toolbar
        txtFromDate.inputView = datePicker
        txtToDate.inputAccessoryView = toolbar
        txtToDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtfield?.text = formatter.string(from: datePicker.date)
        self.endEditing(true)
        
        if txtfield == txtFromDate {
            strFromDate = txtfield?.text ?? ""
        }
        if txtfield == txtToDate {
            strToDate = txtfield?.text ?? ""
        }
        
        for i in 0..<(arrBtn!.count) {
            arrBtn?[i].setImage(#imageLiteral(resourceName: "empty_radio"), for: .normal)
        }
        strWTD = ""
        strMTD = ""
        strYTD = ""
    }
    
    @objc func cancelDatePicker() {
        self.endEditing(true)
    }
    
    @IBAction func btnRadio(_ sender: UIButton) {
        
        strFromDate = ""
        strToDate = ""
        txtFromDate.text = ""
        txtToDate.text = ""
        
        if sender.tag == 1 {
            strWTD = "1"
            strMTD = ""
            strYTD = ""
        }
        if sender.tag == 2 {
            strWTD = ""
            strMTD = "1"
            strYTD = ""
        }
        if sender.tag == 3 {
            strWTD = ""
            strMTD = ""
            strYTD = "1"
        }
        
        for i in 0..<(arrBtn!.count) {
            
            if sender.tag == arrBtn?[i].tag {
                
                arrBtn?[i].setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            } else {
                
                arrBtn?[i].setImage(#imageLiteral(resourceName: "empty_radio"), for: .normal)
            }
        }
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        var dict:[String:String] = [:]
        
        dict["from_date"] = strFromDate
        dict["to_date"] = strToDate
        dict["is_week_filter"] = strWTD
        dict["is_month_filter"] = strMTD
        dict["is_year_filter"] = strYTD
        
        NotificationCenter.default.post(name: Notification.Name("ReportFilter"), object: nil, userInfo: dict)
        
        self.removeFromSuperview()
        
    }
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
extension ReportFilterView:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txtfield = textField
    }
}
