//
//  FoodHeaderView.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit

class FoodHeaderView: UICollectionReusableView {

    var arrSubCatData:[GetCategoryData]?
    
    @IBOutlet weak var collview: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collview.dataSource = self
        collview.delegate = self
        
        collview.register(UINib(nibName: "FoodHeaderCell", bundle: nil), forCellWithReuseIdentifier: "FoodHeaderCell")
    }
    
}
extension FoodHeaderView:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrSubCatData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodHeaderCell", for: indexPath) as! FoodHeaderCell
        
        cell.lblTitle.text = arrSubCatData?[indexPath.row].category_name
        cell.img.sd_setImage(with: URL(string: arrSubCatData?[indexPath.row].category_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(arrSubCatData?[indexPath.row].category_id ?? 0)"])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 80, height: (collectionView.frame.height/2)-5)
        //return CGSize(width: (collectionView.frame.width/3)-10, height: (collectionView.frame.width/3)-10)
        
    }
    
}

