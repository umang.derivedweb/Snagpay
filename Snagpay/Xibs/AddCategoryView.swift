//
//  AddCategoryView.swift
//  Snagpay
//
//  Created by Apple on 23/03/21.
//

import UIKit

class AddCategoryView: UIView {
    
    var arrAllCategories:[AllCategoriesData]?
    var arrSub_categories:[SubCategoriesData]?
    var arrSub_subCategories:[SubCategoriesData]?
    var intSelectedRow:Int!
    var tappedTextfieldTag:Int?
    var categoryId = 0
    var subCategoryId = 0
    var sub_subCategoryId = 0
    
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    override func awakeFromNib() {
        txtFirst.delegate = self
        txtSecond.delegate = self
        txtThird.delegate = self
        getCategoryAPI()
        toolBar()
    }

    func toolBar() {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFirst.inputAccessoryView = toolBar
        txtSecond.inputAccessoryView = toolBar
        txtThird.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        if tappedTextfieldTag == 1 {
            categoryId = (arrAllCategories?[intSelectedRow].category_id)!
            txtFirst.text = arrAllCategories?[intSelectedRow].category_name
            txtFirst.resignFirstResponder()
            
            arrSub_categories = arrAllCategories?[intSelectedRow].sub_categories
        }
        if tappedTextfieldTag == 2 {
            if arrSub_categories?.count != 0 {
                subCategoryId = (arrSub_categories?[intSelectedRow].category_id)!
                txtSecond.text = arrSub_categories?[intSelectedRow].category_name
                txtSecond.resignFirstResponder()
                arrSub_subCategories = arrSub_categories?[intSelectedRow].sub_sub_categories
            }
        }
        if tappedTextfieldTag == 3 {
            if arrSub_subCategories?.count != 0 {
                sub_subCategoryId = (arrSub_subCategories?[intSelectedRow].category_id)!
                txtThird.text = arrSub_subCategories?[intSelectedRow].category_name
                txtThird.resignFirstResponder()
            }
        }
        print(categoryId)
        print(subCategoryId)
        print(sub_subCategoryId)
    }
    
    @objc func cancelClick() {
        txtFirst.resignFirstResponder()
        txtSecond.resignFirstResponder()
        txtThird.resignFirstResponder()
    }
    
    func getCategoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetOnlyCategory(url: "\(URLs.categories)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrAllCategories = response.data?.all_categories
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    @IBAction func btnTransparent(_ sender: Any) {
        
        self.removeFromSuperview()
    }
    @IBAction func btnSave(_ sender: Any) {
        
        if sub_subCategoryId != 0 {
            
            if let _ = Helper.shared.arrCategories.firstIndex(of: sub_subCategoryId) {
                
            } else {
                Helper.shared.arrCategories.append(sub_subCategoryId)
            }
            
        } else {
            if let _ = Helper.shared.arrCategories.firstIndex(of: subCategoryId) {
                
            } else {
                Helper.shared.arrCategories.append(subCategoryId)
            }
            
        }
        NotificationCenter.default.post(name: Notification.Name("ManageWishlist"), object: nil)
        self.removeFromSuperview()

    }
}
extension AddCategoryView:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfieldTag == 1 {
            return arrAllCategories?.count ?? 0
        }
        if tappedTextfieldTag == 2 {
            return arrSub_categories?.count ?? 0
        }
        if tappedTextfieldTag == 3 {
            return arrSub_subCategories?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfieldTag == 1 {
            return arrAllCategories?[row].category_name
        }
        if tappedTextfieldTag == 2 {
            return arrSub_categories?[row].category_name
        }
        if tappedTextfieldTag == 3 {
            return arrSub_subCategories?[row].category_name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
}

extension AddCategoryView:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        tappedTextfieldTag = textField.tag
        let picker = UIPickerView(frame:CGRect(x: 0, y: 0, width: Helper.shared.screenWidth, height: 216))
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        textField.inputView = picker
        picker.reloadAllComponents()
    }
}
