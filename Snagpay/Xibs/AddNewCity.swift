//
//  AddNewCity.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit

class AddNewCity: UIView {

    var arrCities:[GetCitiesData]?
    
    var arrLocalCities:[Int] = []
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    
    override func awakeFromNib() {
        
        table.dataSource = self
        table.rowHeight = 60
        table.tableFooterView = UIView()
        
        self.table.register(UINib(nibName: "AddNewCityCell", bundle: nil), forCellReuseIdentifier: "AddNewCityCell")

        getCitiesAPI()
    }
    
    @objc func btnCheckbox(sender:UIButton) {
        
        if sender.accessibilityIdentifier  == "0" {
            arrLocalCities.append((arrCities?[sender.tag].city_id)!)
        } else {
            if let index = arrLocalCities.firstIndex(of: (arrCities?[sender.tag].city_id)!) {
                arrLocalCities.remove(at: index)
            }
        }
        table.reloadData()
        print(arrLocalCities)
    }

    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        Helper.shared.arrCities = arrLocalCities
        NotificationCenter.default.post(name: Notification.Name("ManageWishlist"), object: nil)
        self.removeFromSuperview()
    }
    
    
    //MARK: - Webservice
    func getCitiesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=3", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.table.reloadData()
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
}
extension AddNewCity:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewCityCell") as! AddNewCityCell
        cell.lblCity.text = arrCities?[indexPath.row].city_name
        cell.btnCheckbox.tag = indexPath.row
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        
        if let _ = arrLocalCities.firstIndex(of: (arrCities?[indexPath.row].city_id)!) {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            cell.btnCheckbox.accessibilityIdentifier = "1"
        } else {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            cell.btnCheckbox.accessibilityIdentifier = "0"
        }
        
        return cell
        
    }
    
    
}
