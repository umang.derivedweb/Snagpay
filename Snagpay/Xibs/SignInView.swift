//
//  SignInView.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class SignInView: UIView {

    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnCheckbox: UIButton!
    
    @IBOutlet weak var btnSignInAction: UIButton!
    
    override func awakeFromNib() {
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnSignInAction.layer.cornerRadius = 30
            btnFb.layer.cornerRadius = 30
            btnGoogle.layer.cornerRadius = 30
        }
    }
    
    @IBAction func btnPassword(_ sender: UIButton) {
        
        if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
            sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
            sender.image(for: .normal)
            txtPassword.isSecureTextEntry = false
        } else {
            sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
    }
}
