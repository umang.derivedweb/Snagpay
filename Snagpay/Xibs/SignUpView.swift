//
//  SignUpView.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class SignUpView: UIView {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var txtBusinessName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtTypeBusiness: UITextField!
    @IBOutlet weak var txtCostGoods: UITextField!
    @IBOutlet weak var txtWebsite: UITextField!
    @IBOutlet weak var txtLocations: UITextField!
    @IBOutlet weak var txtHowlong: UITextField!
    @IBOutlet weak var txtSales: UITextField!
    
    override func awakeFromNib() {
        
        txtZip.delegate = self
        txtPhone.delegate = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnSignUp.layer.cornerRadius = 30
        }
        
    }
    
    
}
extension SignUpView:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtZip {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXXXX", phone: newString)
                return false
        }
        if textField == txtPhone {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXX-XXX-XXXX", phone: newString)
                return false
        }
        return true
    }
}
