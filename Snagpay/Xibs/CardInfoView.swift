//
//  CardInfoView.swift
//  BitesFunds
//
//  Created by Apple on 02/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CardInfoView: UIView {

    @IBOutlet weak var txtExpiry: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblPay: UILabel!
    
    override func awakeFromNib() {
        txtCardNumber.becomeFirstResponder()
        
    }

    @IBAction func btnTransparent(_ sender: Any) {
        
        self.removeFromSuperview()
    }


}
