//
//  CustomTabbar.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class CustomTabbar: UIView {

    @IBOutlet weak var btnDashboard: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblMyProfile: UILabel!
    @IBOutlet weak var lblDashboard: UILabel!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgNotification: UIImageView!
    
//    @IBAction func btnDashboardSelected(_ sender: UIButton) {
//        
//        imgHome.image = #imageLiteral(resourceName: "home_active")
//        lblDashboard.textColor = UIColor(named: "App Blue")
//        imgNotification.image = #imageLiteral(resourceName: "notification")
//        lblNotification.textColor = UIColor.lightGray
//        imgProfile.image = #imageLiteral(resourceName: "profile")
//        lblMyProfile.textColor = UIColor.lightGray
//    }
//    
//    @IBAction func btnNotificationSelected(_ sender: UIButton) {
//        imgHome.image = #imageLiteral(resourceName: "home")
//        lblDashboard.textColor = UIColor.lightGray
//        imgNotification.image = #imageLiteral(resourceName: "notification_active")
//        lblNotification.textColor = UIColor(named: "App Blue")
//        imgProfile.image = #imageLiteral(resourceName: "profile")
//        lblMyProfile.textColor = UIColor.lightGray
//    }
//    
//    @IBAction func btnMyProfileSelected(_ sender: UIButton) {
//        imgHome.image = #imageLiteral(resourceName: "home")
//        lblDashboard.textColor = UIColor.lightGray
//        imgNotification.image = #imageLiteral(resourceName: "notification")
//        lblNotification.textColor = UIColor.lightGray
//        imgProfile.image = #imageLiteral(resourceName: "profile_active")
//        lblMyProfile.textColor = UIColor(named: "App Blue")
//    }
}
