//
//  AddReviewView.swift
//  Snagpay
//
//  Created by Apple on 17/12/21.
//

import UIKit
import Cosmos

class AddReviewView: UIView {

    @IBOutlet weak var btnSubmitReview: UIButton!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var txtview: UITextView!
    
    override func awakeFromNib() {
        
        txtview.layer.borderWidth = 1
        txtview.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        txtview.layer.cornerRadius = 5
    }
    
    
    @IBAction func btnTransparent(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
