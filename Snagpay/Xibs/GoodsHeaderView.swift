//
//  GoodsHeaderView.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit
import TagListView

class GoodsHeaderView: UICollectionReusableView, TagListViewDelegate {

    var arrSubCatData:[GetCategoryData]?
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tagListView.delegate = self
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        let index = arrSubCatData?.firstIndex{$0.category_name == title}
        
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(arrSubCatData?[index!].category_id ?? 0)"])

    }
    
}
