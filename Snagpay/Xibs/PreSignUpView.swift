//
//  PreSignUpView.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit

class PreSignUpView: UIView {

    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFb: UIButton!
    @IBOutlet weak var txtFName: UITextField!
    
    override func awakeFromNib() {
        
        
    }

    @IBAction func btnPassword(_ sender: UIButton) {
        if sender.tag == 1 {
            
            if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
                sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
                sender.image(for: .normal)
                txtPassword.isSecureTextEntry = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
                txtPassword.isSecureTextEntry = true
            }
            
        } else {
           if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
               sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
               sender.image(for: .normal)
               txtConfPassword.isSecureTextEntry = false
           } else {
               sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
               txtConfPassword.isSecureTextEntry = true
           }
        }
    }
    
    
    
    @IBAction func btnCheck(_ sender: UIButton) {
        if sender.tag == 1 {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            sender.tag = 2
        } else {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 1
        }
    }
}
