//
//  ServicesHeaderView.swift
//  Snagpay
//
//  Created by Apple on 06/08/21.
//

import UIKit
import Alamofire

class ServicesHeaderView: UICollectionReusableView {

    var arrSubCatData:[GetCategoryData]?
    var arrSubSubCatData:[GetCategoryData]?
    var intSelectedIndexColl1:Int?
    var intSelectedIndexColl2:Int?
    
    var intSubCatId:Int?
    
    @IBOutlet weak var lblSubCat: UILabel!
    @IBOutlet weak var collview2: UICollectionView!
    @IBOutlet weak var collview1: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collview1.dataSource = self
        collview2.dataSource = self
        collview1.delegate = self
        collview2.delegate = self
        collview1.register(UINib(nibName: "ServiceHeaderCell", bundle: nil), forCellWithReuseIdentifier: "ServiceHeaderCell")
        collview2.register(UINib(nibName: "HomeServiceCell", bundle: nil), forCellWithReuseIdentifier: "HomeServiceCell")
        
        lblSubCat.text = ""
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            // Put your code which should be executed with a delay here
            //self.collectionView(self.collview1, didSelectItemAt: IndexPath(row: 0, section: 0))
        }
    }
    
    //MARK: - Webservice
    func getSubCategoryAPI(cat_id:Int) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetCategoryDeals(url: "\(URLs.get_sub_categories)\(cat_id)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.arrSubSubCatData = response.data?.sub_categories
                
                self.collview2.reloadData()
                
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(self.arrSubSubCatData?[0].category_id ?? 0)"])
                
                
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
}
extension ServicesHeaderView:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collview1 {
            return arrSubCatData?.count ?? 0
        } else {
            return arrSubSubCatData?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collview1 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceHeaderCell", for: indexPath) as! ServiceHeaderCell
            
            cell.lblTitle.text = arrSubCatData?[indexPath.row].category_name
            
            if indexPath.row == intSelectedIndexColl1 {
                cell.imgTick.image = #imageLiteral(resourceName: "select_service")
            } else {
                cell.imgTick.image = nil
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeServiceCell", for: indexPath) as! HomeServiceCell
            
            cell.lblTitle.text = arrSubSubCatData?[indexPath.row].category_name
            if indexPath.row == intSelectedIndexColl2 {
                cell.viewBg.backgroundColor = #colorLiteral(red: 0.002139341086, green: 0.06779965013, blue: 0.4206510782, alpha: 1)
                cell.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                cell.viewBg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell.lblTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collview1 {
            if let cell = collview1.cellForItem(at: indexPath) as? ServiceHeaderCell {
                cell.imgTick.image = #imageLiteral(resourceName: "select_service")
                lblSubCat.text = arrSubCatData?[indexPath.row].category_name
                intSelectedIndexColl1 = indexPath.row
                collview1.reloadData()
                intSelectedIndexColl2 = 0
                getSubCategoryAPI(cat_id: arrSubCatData?[indexPath.row].category_id ?? 0)
            }
            
        }
        if collectionView == collview2 {
            let cell = collview2.cellForItem(at: indexPath) as! HomeServiceCell
            cell.viewBg.backgroundColor = #colorLiteral(red: 0.002139341086, green: 0.06779965013, blue: 0.4206510782, alpha: 1)
            cell.lblTitle.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            intSelectedIndexColl2 = indexPath.row
            collview2.reloadData()
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["cat_id":"\(arrSubSubCatData?[indexPath.row].category_id ?? 0)"])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collview1 {
            return CGSize(width: 135, height: 135)
        } else {
            
            let label = UILabel(frame: CGRect.zero)
            label.text = arrSubSubCatData?[indexPath.row].category_name
            label.sizeToFit()
            return CGSize(width: label.frame.width+20, height: 30)
        }
    }
    
}
