//
//  ResponseObject.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import ObjectMapper

class GetCitiesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[GetCitiesData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetCategoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CategoryData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class LoginRegisterResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:UserData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class CommonResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
    }
    
}
class CategoryDealsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CategoryDealsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}

class DealDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:DealsDetailsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetSubCategoriesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GetSubCategoriesData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetWishlistResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:WishlistRecentlyViewdData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}

class CategoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CategoryOnlyData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class ReviewOrderResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:ReviewOrderData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetStatesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[StatesData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetShippingAddressesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[ShippingAddressesData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetCreditsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CreditsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}

class AllOrdersResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:AllOrdersData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class RecentPaymentHistoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:RecentPaymentHistoryData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class PaymentDownloadResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:PaymentDownloadData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetMyWishlistResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var category:[WishlistCategoriesData]?
    var city:[WishlistCitiesData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        category <- map["category"]
        city <- map["city"]
    }
}
class GetSnagpayWalletResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:SnagpayWalletData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class NotiSettingsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:NotiSettingsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GiftCardsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[GiftCardsData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GiftCardsTransactionsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var e_gift_card_amount:String?
    var redeemed_amount:String?
    var data:[GiftCardsTransactionsData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
        redeemed_amount <- map["redeemed_amount"]
        e_gift_card_amount <- map["e_gift_card_amount"]
    }
}
class FullOrderDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:FullOrderDetailsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class CreateOrderResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CreateOrderData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class OrderDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:OrderDetailsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class TradeHistoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:TradeHistoryData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class MyAndDraftDealsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:MyAndDraftDealsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class ReportsDocumentsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:ReportsDocumentsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetProfileResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GetProfileData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class DealDraftDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:DealDraftDetailsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GlobalNotificationResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GlobalNotificationData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GetMerchantsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GetMerchantsData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class GiveGiftResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[OrdersListData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class CreditLineHistoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CreditLineHistoryData?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class MerchantsAlphabetsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[MerchantsAlphabetsData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
}
class SearchDealsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:SearchDealsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}

class UserLocationsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:[LocationData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetLocationResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:LocationData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetCardsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:GetCardsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class GetCardsData: Mappable {
    
    var data:[CardData]?
    var default_card_id:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        default_card_id <- map["default_card_id"]
    }
    
}
class CardData: Mappable {
    
    var id:String?
    var brand:String?
    var last4:String?
    var exp_month:Int?
    var exp_year:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        id <- map["id"]
        brand <- map["brand"]
        last4 <- map["last4"]
        exp_month <- map["exp_month"]
        exp_year <- map["exp_year"]
    }
    
}
