//
//  Model.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import ObjectMapper


class GetCitiesData: Mappable {
    
    var city_id:Int?
    var state_id:Int?
    var city_name:String?
    var country:String?
    var latitude:Float?
    var longitude:Float?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        city_id <- map["city_id"]
        state_id <- map["state_id"]
        city_name <- map["city_name"]
        country <- map["country"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
    
}
class CategoryData: Mappable {
    
    var categories:[GetCategoryData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        categories <- map["categories"]
        
    }
    
}

class GetCategoryData: Mappable {
    
    var category_id:Int?
    var parent_id:Int?
    var parent_level:Int?
    var category_name:String?
    var slug:String?
    var backround_color:String?
    var category_image:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        category_id <- map["category_id"]
        parent_id <- map["parent_id"]
        parent_level <- map["parent_level"]
        category_name <- map["category_name"]
        slug <- map["slug"]
        backround_color <- map["backround_color"]
        category_image <- map["category_image"]
    }
    
}
class PopularCityData: Mappable {
    
    var travel_city_id:Int?
    var city_name:String?
    var city_image:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        travel_city_id <- map["travel_city_id"]
        city_name <- map["city_name"]
        city_image <- map["city_image"]
    }
    
}
class UserData: Mappable {
    
    var user_id:Int?
    var account_no:String?
    var sales_person_id:Int?
    var first_name:String?
    var last_name:String?
    var email:String?
    var phone_no:String?
    var facebook_id:Int?
    var google_id:Int?
    var type:String?
    var address:String?
    var city_id:Int?
    var state_id:Int?
    var city:String?
    var state:String?
    var country:String?
    var postcode:String?
    var is_email_verified:Int?
    var otp:String?
    var latitude:Float?
    var longitude:Float?
    var business_name:String?
    var type_of_business:Int?
    var can_we_run_credit_report:Int?
    var avg_sales_per_month:Double?
    var how_long_have_you:String?
    var no_of_physical_locations:String?
    var website_or_page:String?
    var cost_of_goods:Int?
    var api_token:String?
    var work_phone:String?
    var cell_phone:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        account_no <- map["account_no"]
        sales_person_id <- map["sales_person_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        phone_no <- map["phone_no"]
        facebook_id <- map["facebook_id"]
        google_id <- map["google_id"]
        type <- map["type"]
        address <- map["address"]
        city_id <- map["city_id"]
        state_id <- map["state_id"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        postcode <- map["postcode"]
        is_email_verified <- map["is_email_verified"]
        otp <- map["otp"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        business_name <- map["business_name"]
        type_of_business <- map["type_of_business"]
        cost_of_goods <- map["cost_of_goods"]
        can_we_run_credit_report <- map["can_we_run_credit_report"]
        avg_sales_per_month <- map["avg_sales_per_month"]
        how_long_have_you <- map["how_long_have_you"]
        no_of_physical_locations <- map["no_of_physical_locations"]
        website_or_page <- map["website_or_page"]
        api_token <- map["api_token"]
        work_phone <- map["work_phone"]
        cell_phone <- map["cell_phone"]
    }
    
}

class CategoryDealsData: Mappable {
    
    var deals:DealsData?
    var sub_categories:[GetCategoryData]?
    var sub_sub_categories:[GetCategoryData]?
    var popular_cities:[PopularCityData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        sub_categories <- map["sub_categories"]
        sub_sub_categories <- map["sub_sub_categories"]
        deals <- map["deals"]
        popular_cities <- map["popular_cities"]
    }
    
}

class GetSubCategoriesData: Mappable {
    
    var sub_categories:[GetCategoryData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        sub_categories <- map["sub_categories"]
    }
    
}
class DealsData: Mappable {
    
    var data:[DealsListData]?
    var current_page:Int?
    var last_page:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        last_page <- map["last_page"]
    }
    
}
class DealsListData: Mappable {
    
    var deal_id:Int?
    var deal_code:String?
    var title:String?
    var deal_image:String?
    var slug:String?
    var category_id:Int?
    var description:String?
    var description_2:String?
    var description_3:String?
    var deal_description:String?
    var avg_rating:Float?
    var is_deal_of_the_day:Int?
    var is_trending:Int?
    var is_new:Int?
    var address:String?
    var zip_code:String?
    var street:String?
    var city_name:String?
    var latitude:Float?
    var longitude:Float?
    var state_name:String?
    var regular_price:Int?
    var sell_price:Int?
    var bought:String?
    var total_rating:Int?
    var is_wishlist:Int?
    var total_price:Int?
    var discount:Int?
    var snagpay_bucks:Int?
    var main_category_id:Int?
    var share_url:String?
    var business_name:String?
    var deal_date:String?
    var is_deal_purchased:Int?
    var company_name:String?
    var user_id:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_id <- map["deal_id"]
        deal_code <- map["deal_code"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        slug <- map["slug"]
        category_id <- map["category_id"]
        description <- map["description"]
        avg_rating <- map["avg_rating"]
        is_deal_of_the_day <- map["is_deal_of_the_day"]
        is_trending <- map["is_trending"]
        is_new <- map["is_new"]
        address <- map["address"]
        zip_code <- map["zip_code"]
        street <- map["street"]
        city_name <- map["city_name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        state_name <- map["state_name"]
        regular_price <- map["regular_price"]
        sell_price <- map["sell_price"]
        bought <- map["bought"]
        total_rating <- map["total_rating"]
        is_wishlist <- map["is_wishlist"]
        total_price <- map["total_price"]
        discount <- map["discount"]
        snagpay_bucks <- map["snagpay_bucks"]
        main_category_id <- map["main_category_id"]
        share_url <- map["share_url"]
        business_name <- map["business_name"]
        deal_date <- map["deal_date"]
        is_deal_purchased <- map["is_deal_purchased"]
        description_2 <- map["description_2"]
        description_3 <- map["description_3"]
        deal_description <- map["deal_description"]
        company_name <- map["company_name"]
        user_id <- map["user_id"]
    }
    
}

class DealsDetailsData: Mappable {
    
    var deal_details:DealsListData?
    var deal_options:[DealOptionData]?
    var customer_reviews:[ReviewsData]?
    var images:[DealImagesData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_details <- map["deal_details"]
        deal_options <- map["deal_options"]
        customer_reviews <- map["customer_reviews"]
        images <- map["images"]
    }
    
}
class DealOptionData: Mappable {
    
    var deal_option_id:Int?
    var deal_option_name:String?
    var deal_option_code:String?
    var deal_option_description:String?
    var regular_price:Int?
    var sell_price:Int?
    var deal_id:Int?
    var qty:Int?
    var no_of_egifts_available:Int?
    var urgency_price:Int?
    var post_deal_for_a:Int?
    var maximum_e_gift_cards_purchased:Int?
    var maximum_days_to_run_deal:Int?
   
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_option_id <- map["deal_option_id"]
        deal_option_name <- map["deal_option_name"]
        deal_option_code <- map["deal_option_code"]
        deal_option_description <- map["deal_option_description"]
        regular_price <- map["regular_price"]
        deal_id <- map["deal_id"]
        sell_price <- map["sell_price"]
        qty <- map["qty"]
        no_of_egifts_available <- map["no_of_egifts_available"]
        urgency_price <- map["urgency_price"]
        post_deal_for_a <- map["post_deal_for_a"]
        maximum_e_gift_cards_purchased <- map["maximum_e_gift_cards_purchased"]
        maximum_days_to_run_deal <- map["maximum_days_to_run_deal"]
    }
    
}

class ReviewsData: Mappable {
    
    var deal_rating_id:Int?
    var first_name:String?
    var last_name:String?
    var deal_id:Int?
    var review:String?
    var date:String?
    var rating:Float?
   
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_rating_id <- map["deal_rating_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        deal_id <- map["deal_id"]
        review <- map["review"]
        date <- map["date"]
        rating <- map["rating"]
    }
}

class WishlistRecentlyViewdData: Mappable {
    
    var wishlist:[DealsListData]?
    var recently_viewed:[DealsListData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        
        wishlist <- map["wishlist"]
        recently_viewed <- map["recently_viewed"]
    }
}


class CategoryOnlyData: Mappable {
    
    var popular_categories:[GetCategoryData]?
    var all_categories:[AllCategoriesData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        popular_categories <- map["popular_categories"]
        all_categories <- map["all_categories"]
    }
}
class AllCategoriesData: Mappable {
    
    var category_name:String?
    var category_id:Int?
    var sub_categories:[SubCategoriesData]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        category_name <- map["category_name"]
        category_id <- map["category_id"]
        sub_categories <- map["sub_categories"]
    }
}
class SubCategoriesData: Mappable {
    
    var category_id:Int?
    var category_name:String?
    var sub_sub_categories:[SubCategoriesData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        category_id <- map["category_id"]
        category_name <- map["category_name"]
        sub_sub_categories <- map["sub_sub_categories"]
    }
}

class ReviewOrderData: Mappable {
    
    var total_price:Int?
    var discount:Int?
    var snagpay_bucks:Int?
    var required_snagpay_bucks:Int?
    var use_credit_line_balance:Int?
    var deals:[OptionsDealsData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        total_price <- map["total_price"]
        discount <- map["discount"]
        snagpay_bucks <- map["snagpay_bucks"]
        required_snagpay_bucks <- map["required_snagpay_bucks"]
        use_credit_line_balance <- map["use_credit_line_balance"]
        deals <- map["deals"]
    }
}
class OptionsDealsData: Mappable {
    
    var deal_id:Int?
    var title:String?
    var deal_image:String?
    var deal_option_id:Int?
    var deal_option_code:String?
    var deal_option_name:String?
    var regular_price:Int?
    var sell_price:Int?
    var qty:String?
    var company_name:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_id <- map["deal_id"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        deal_option_id <- map["deal_option_id"]
        deal_option_code <- map["deal_option_code"]
        deal_option_name <- map["deal_option_name"]
        regular_price <- map["regular_price"]
        sell_price <- map["sell_price"]
        qty <- map["qty"]
        company_name <- map["company_name"]
    }
}

class StatesData: Mappable {
    
    var state_id:Int?
    var state_code:String?
    var state_name:String?
    var country_id:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        state_id <- map["state_id"]
        state_code <- map["state_code"]
        state_name <- map["state_name"]
        country_id <- map["country_id"]
    }
}

class ShippingAddressesData: Mappable {
    
    var shipping_address_id:Int?
    var name:String?
    var address:String?
    var street:String?
    var zip_code:String?
    var phone:String?
    var is_default:Int?
    var city_name:String?
    var state_name:String?
    var country_name:String?
    var state_id:Int?
    var city_id:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        shipping_address_id <- map["shipping_address_id"]
        name <- map["name"]
        address <- map["address"]
        street <- map["street"]
        zip_code <- map["zip_code"]
        phone <- map["phone"]
        is_default <- map["is_default"]
        city_name <- map["city_name"]
        state_name <- map["state_name"]
        country_name <- map["country_name"]
        state_id <- map["state_id"]
        city_id <- map["city_id"]
    }
}

class CreditsData: Mappable {
    
    var trade_credit:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        trade_credit <- map["trade_credit"]
    }
}

class AllOrdersData: Mappable {
    
    var data:[OrdersListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}
class OrdersListData: Mappable {
    
    var e_gift_card_id:Int?
    var e_gift_card_code:String?
    var user_id:Int?
    var order_id:Int?
    var amount:Int?
    var deal_option_id:Int?
    var qty:Int?
    var qrcode:String?
    var status:String?
    var give_as_a_gift_id:Int?
    var deal_id:Int?
    var deal_code:String?
    var title:String?
    var deal_image:String?
    var description:Int?
    var website:Int?
    var deal_location_id:Int?
    var avg_rating:Float?
    var is_new:Int?
    var is_deal_of_the_day:Int?
    var is_trending:Int?
    var is_paid:Int?
    var sell_price:Int?
    var address:String?
    var zip_code:String?
    var street:String?
    var city_name:String?
    var state_name:String?
    var state_code:String?
    var bought:String?
    var total_rating:Int?
    var is_wishlist:Int?
    var redeemed_amount:Int?
    var company_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        e_gift_card_id <- map["e_gift_card_id"]
        e_gift_card_code <- map["e_gift_card_code"]
        user_id <- map["user_id"]
        order_id <- map["order_id"]
        amount <- map["amount"]
        deal_option_id <- map["deal_option_id"]
        qty <- map["qty"]
        qrcode <- map["qrcode"]
        status <- map["status"]
        give_as_a_gift_id <- map["give_as_a_gift_id"]
        deal_id <- map["deal_id"]
        deal_code <- map["deal_code"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        description <- map["description"]
        website <- map["website"]
        deal_location_id <- map["deal_location_id"]
        avg_rating <- map["avg_rating"]
        is_new <- map["is_new"]
        is_deal_of_the_day <- map["is_deal_of_the_day"]
        is_trending <- map["is_trending"]
        is_paid <- map["is_paid"]
        sell_price <- map["sell_price"]
        address <- map["address"]
        zip_code <- map["zip_code"]
        street <- map["street"]
        city_name <- map["city_name"]
        state_name <- map["state_name"]
        state_code <- map["state_code"]
        bought <- map["bought"]
        total_rating <- map["total_rating"]
        is_wishlist <- map["is_wishlist"]
        redeemed_amount <- map["redeemed_amount"]
        company_name <- map["company_name"]
    }
}
class RecentPaymentHistoryData: Mappable {
    
    var data:[PaymentListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}
class PaymentListData: Mappable {
    
    var e_wallet_id:Int?
    var e_wallet_tran_code:String?
    var user_id:Int?
    var wallet_credit:Int?
    var balance:Int?
    var transaction_title:String?
    var transaction_type:String?
    var linked_table:String?
    var datetime:String?
    var title:String?
    var business_name:String?
   
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        e_wallet_id <- map["e_wallet_id"]
        e_wallet_tran_code <- map["e_wallet_tran_code"]
        user_id <- map["user_id"]
        wallet_credit <- map["wallet_credit"]
        balance <- map["balance"]
        transaction_title <- map["transaction_title"]
        transaction_type <- map["transaction_type"]
        linked_table <- map["linked_table"]
        datetime <- map["datetime"]
        title <- map["title"]
        business_name <- map["business_name"]
    }
}

class PaymentDownloadData: Mappable {
    
    var pdf_url:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        pdf_url <- map["pdf_url"]
    }
}

class WishlistCategoriesData: Mappable {
    
    var category_id:Int?
    var category_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        category_id <- map["category_id"]
        category_name <- map["category_name"]
    }
}

class WishlistCitiesData: Mappable {
    
    var city_id:Int?
    var city_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        city_id <- map["city_id"]
        city_name <- map["city_name"]
    }
}

class SnagpayWalletData: Mappable {
    
    var trade_credit:String?
    var recent_transactions:[PaymentListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        trade_credit <- map["trade_credit"]
        recent_transactions <- map["recent_transactions"]
    }
}

class NotiSettingsData: Mappable {
    
    var importat_message_alert:Int?
    var shipments_notifications:Int?
    var personalised_notifications:Int?
    var wishlist_notifications:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        importat_message_alert <- map["importat_message_alert"]
        shipments_notifications <- map["shipments_notifications"]
        personalised_notifications <- map["personalised_notifications"]
        wishlist_notifications <- map["wishlist_notifications"]
    }
}

class GiftCardsData: Mappable {
    
    var e_gift_card_id:Int?
    var e_gift_card_code:String?
    var user_id:Int?
    var amount:Int?
    var qty:Int?
    var qrcode:String?
    var status:String?
    var deal_option_id:Int?
    var title:String?
    var deal_image:String?
    var merchant_id:Int?
    var redeemed_amount:Int?
    var created_at:String?
    var business_name:String?
   
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        e_gift_card_id <- map["e_gift_card_id"]
        e_gift_card_code <- map["e_gift_card_code"]
        user_id <- map["user_id"]
        amount <- map["amount"]
        qty <- map["qty"]
        qrcode <- map["qrcode"]
        status <- map["status"]
        deal_option_id <- map["deal_option_id"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        merchant_id <- map["merchant_id"]
        redeemed_amount <- map["redeemed_amount"]
        created_at <- map["created_at"]
        business_name <- map["business_name"]
    }
}

class GiftCardsTransactionsData: Mappable {
    
    var e_gift_card_transaction_id:Int?
    var e_gift_card_id:Int?
    var e_gift_card_tran_code:String?
    var amount:Int?
    var is_paid_additional_amount:Int?
   
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        e_gift_card_transaction_id <- map["e_gift_card_transaction_id"]
        e_gift_card_id <- map["e_gift_card_id"]
        e_gift_card_tran_code <- map["e_gift_card_tran_code"]
        amount <- map["amount"]
        is_paid_additional_amount <- map["is_paid_additional_amount"]
        
    }
}

class FullOrderDetailsData: Mappable {
    
    var address:String?
    var name:String?
    var street:String?
    var zip_code:String?
    var created_at:String?
    var order_status:String?
    var phone:String?
    var sub_total_amount:Int?
    var total_amount:Int?
    var discount:Int?
    var order_code:String?
    var order_datetime:String?
    var items:[ItemsData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        address <- map["address"]
        name <- map["name"]
        street <- map["street"]
        zip_code <- map["zip_code"]
        created_at <- map["created_at"]
        order_status <- map["order_status"]
        phone <- map["phone"]
        sub_total_amount <- map["sub_total_amount"]
        total_amount <- map["total_amount"]
        discount <- map["discount"]
        items <- map["items"]
        order_code <- map["order_code"]
        order_datetime <- map["order_datetime"]
    }
}
class ItemsData: Mappable {
    
    var amount:Int?
    var avg_rating:Int?
    var bought:String?
    var deal_image:String?
    var description:String?
    var order_id:Int?
    var qty:Int?
    var regular_price:Int?
    var sell_price:Int?
    var title:String?
    var total_rating:Int?
    var is_wishlist:Int?
    var is_trending:Int?
    var is_deal_of_the_day:Int?
    var company_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        amount <- map["amount"]
        avg_rating <- map["avg_rating"]
        bought <- map["bought"]
        deal_image <- map["deal_image"]
        description <- map["description"]
        order_id <- map["order_id"]
        qty <- map["qty"]
        regular_price <- map["regular_price"]
        sell_price <- map["sell_price"]
        title <- map["title"]
        total_rating <- map["total_rating"]
        is_wishlist <- map["is_wishlist"]
        is_trending <- map["is_trending"]
        is_deal_of_the_day <- map["is_deal_of_the_day"]
        company_name <- map["company_name"]
    }
}

class CreateOrderData: Mappable {
    
    var order_id:Int?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        order_id <- map["order_id"]
        
    }
}

class OrderDetailsData: Mappable {
    
    var e_gift_card_id:Int?
    var amount:Int?
    var e_gift_card_code:String?
    var deal_image:String?
    var qty:Int?
    var status:Int?
    var avg_rating:Int?
    var regular_price:Int?
    var sell_price:Int?
    var title:String?
    var bought:String?
    var is_wishlist:Int?
    var is_trending:Int?
    var is_deal_of_the_day:Int?
    var first_name:String?
    var last_name:String?
    var phone_no:String?
    var address:String?
    var city:String?
    var state:String?
    var country:String?
    var postcode:String?
    var deal_id:Int?
    var company_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        e_gift_card_id <- map["e_gift_card_id"]
        amount <- map["amount"]
        e_gift_card_code <- map["e_gift_card_code"]
        deal_image <- map["deal_image"]
        qty <- map["qty"]
        status <- map["status"]
        avg_rating <- map["avg_rating"]
        regular_price <- map["regular_price"]
        sell_price <- map["sell_price"]
        title <- map["title"]
        bought <- map["bought"]
        is_wishlist <- map["is_wishlist"]
        is_trending <- map["is_trending"]
        is_deal_of_the_day <- map["is_deal_of_the_day"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone_no <- map["phone_no"]
        address <- map["address"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        postcode <- map["postcode"]
        deal_id <- map["deal_id"]
        company_name <- map["company_name"]
    }
}

class TradeHistoryData: Mappable {
    
    var cost_of_trade:Float?
    var mtd_sales:String?
    var ytd_sales:String?
    var mtd_purchases:String?
    var ytd_purchases:String?
    var mtd_trade_volume:String?
    var ytd_trade_volume:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        cost_of_trade <- map["cost_of_trade"]
        mtd_sales <- map["mtd_sales"]
        ytd_sales <- map["ytd_sales"]
        mtd_purchases <- map["mtd_purchases"]
        ytd_purchases <- map["ytd_purchases"]
        mtd_trade_volume <- map["mtd_trade_volume"]
        ytd_trade_volume <- map["ytd_trade_volume"]
    }
}

class MyAndDraftDealsData: Mappable {
    
    var data:[ArrDealsData]?
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
        
    }
}
class ArrDealsData: Mappable {
    
    var deal_id:Int?
    var deal_code:String?
    var title:String?
    var deal_image:String?
    var description:String?
    var purchased:Int?
    var redeemed:Int?
    var deal_date:String?
    var status:String?
    var draft_token:String?
    var total_available:Int?
    var number_of_deal:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_id <- map["deal_id"]
        deal_code <- map["deal_code"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        description <- map["description"]
        purchased <- map["purchased"]
        redeemed <- map["redeemed"]
        deal_date <- map["deal_date"]
        status <- map["status"]
        draft_token <- map["draft_token"]
        total_available <- map["total_available"]
        number_of_deal <- map["number_of_deal"]
    }
}

class ReportsDocumentsData: Mappable {
    
    var pdf:String?
    var my_deals_pdf:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        pdf <- map["pdf"]
        my_deals_pdf <- map["my_deals_pdf"]
    }
}

class GetProfileData: Mappable {
    
    var categories:[GetCategoryData]?
    var locations:[LocationData]?
    var user:ProfileUserData?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        categories <- map["categories"]
        locations <- map["locations"]
        user <- map["user"]
        
    }
}
class LocationData: Mappable {
    
    var user_location_id:Int?
    var entity_name:String?
    var ein:String?
    var email:String?
    var work_phone:String?
    var address:String?
    var city_name:String?
    var state_code:String?
    var state_name:String?
    var postcode:String?
    var country:String?
    var user_id:Int?
    var latitude:Float?
    var longitude:Float?
    var city_id:Int?
    var state_id:Int?
    var qrcode:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_location_id <- map["user_location_id"]
        entity_name <- map["entity_name"]
        ein <- map["ein"]
        email <- map["email"]
        work_phone <- map["work_phone"]
        address <- map["address"]
        city_name <- map["city_name"]
        state_code <- map["state_code"]
        state_name <- map["state_name"]
        postcode <- map["postcode"]
        country <- map["country"]
        user_id <- map["user_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        city_id <- map["city_id"]
        state_id <- map["state_id"]
        qrcode <- map["qrcode"]
    }
}
class ProfileUserData: Mappable {
    
    var user_id:Int?
    var account_no:String?
    var sales_person_id:Int?
    var first_name:String?
    var last_name:String?
    var email:String?
    var phone_no:String?
    var work_phone:String?
    var cell_phone:String?
    var google_id:Int?
    var facebook_id:Int?
    var type:String?
    var address:String?
    var city:String?
    var state:String?
    var country:String?
    var postcode:String?
    var entity_name:String?
    var ein:String?
    var business_name:String?
    var describe_business:String?
    var type_of_business:Int?
    var can_we_run_credit_report:Int?
    var avg_sales_per_month:Int?
    var how_long_have_you:String?
    var no_of_physical_locations:String?
    var website_or_page:String?
    var cost_of_goods:Int?
    var agree_to_receive_news_offers:Int?
    var is_used_postcard:Int?
    var category_name:String?
    var qrcode:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        account_no <- map["account_no"]
        sales_person_id <- map["sales_person_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        phone_no <- map["phone_no"]
        work_phone <- map["work_phone"]
        cell_phone <- map["cell_phone"]
        google_id <- map["google_id"]
        facebook_id <- map["facebook_id"]
        type <- map["type"]
        address <- map["address"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        postcode <- map["postcode"]
        entity_name <- map["entity_name"]
        ein <- map["ein"]
        business_name <- map["business_name"]
        describe_business <- map["describe_business"]
        type_of_business <- map["type_of_business"]
        can_we_run_credit_report <- map["can_we_run_credit_report"]
        avg_sales_per_month <- map["avg_sales_per_month"]
        how_long_have_you <- map["how_long_have_you"]
        no_of_physical_locations <- map["no_of_physical_locations"]
        website_or_page <- map["website_or_page"]
        cost_of_goods <- map["cost_of_goods"]
        agree_to_receive_news_offers <- map["agree_to_receive_news_offers"]
        is_used_postcard <- map["is_used_postcard"]
        category_name <- map["category_name"]
        qrcode <- map["qrcode"]
    }
}

class DealDraftDetailsData: Mappable {
    
    var deal_details:DealDetailsData?
    var category:GetCategoryData?
    var sub_category:GetCategoryData?
    var sub_sub_category:GetCategoryData?
    var deal_images:[DealImagesData]?
    var deal_options:[DealOptionData]?
    var deal_locations:[DealLocationData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_details <- map["deal_details"]
        category <- map["category"]
        sub_category <- map["sub_category"]
        sub_sub_category <- map["sub_sub_category"]
        deal_images <- map["deal_images"]
        deal_options <- map["deal_options"]
        deal_locations <- map["deal_locations"]
        
    }
}
class DealDetailsData: Mappable {
    
    var deal_id:Int?
    var deal_code:String?
    var title:String?
    var deal_image:String?
    var slug:String?
    var category_id:Int?
    var description:String?
    var website:String?
    var description_2:String?
    var description_3:String?
    var deal_description:String?
    var avg_rating:Float?
    var is_deal_of_the_day:Int?
    var is_trending:Int?
    var is_new:Int?
    var address:String?
    var zip_code:String?
    var street:String?
    var city_name:String?
    var latitude:Float?
    var longitude:Float?
    var state_name:String?
    var regular_price:Int?
    var sell_price:Int?
    var bought:String?
    var total_rating:Int?
    var is_wishlist:Int?
    var total_price:Int?
    var discount:Int?
    var snagpay_bucks:Int?
    var main_category_id:Int?
    var share_url:String?
    var limit_per_person:Int?
    var buy_additional_as_gift:Int?
    var repurchased_every_days:Int?
    var expires_months_from_date_offer_begins:Int?
    var deal_date:String?
    var draft_token:String?
    var company_name:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_id <- map["deal_id"]
        deal_code <- map["deal_code"]
        title <- map["title"]
        deal_image <- map["deal_image"]
        website <- map["website"]
        slug <- map["slug"]
        category_id <- map["category_id"]
        description <- map["description"]
        avg_rating <- map["avg_rating"]
        is_deal_of_the_day <- map["is_deal_of_the_day"]
        is_trending <- map["is_trending"]
        is_new <- map["is_new"]
        address <- map["address"]
        zip_code <- map["zip_code"]
        street <- map["street"]
        city_name <- map["city_name"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        state_name <- map["state_name"]
        regular_price <- map["regular_price"]
        sell_price <- map["sell_price"]
        bought <- map["bought"]
        total_rating <- map["total_rating"]
        is_wishlist <- map["is_wishlist"]
        total_price <- map["total_price"]
        discount <- map["discount"]
        snagpay_bucks <- map["snagpay_bucks"]
        main_category_id <- map["main_category_id"]
        share_url <- map["share_url"]
        limit_per_person <- map["limit_per_person"]
        buy_additional_as_gift <- map["buy_additional_as_gift"]
        repurchased_every_days <- map["repurchased_every_days"]
        expires_months_from_date_offer_begins <- map["expires_months_from_date_offer_begins"]
        deal_date <- map["deal_date"]
        draft_token <- map["draft_token"]
        description_2 <- map["description_2"]
        description_3 <- map["description_3"]
        deal_description <- map["deal_description"]
        company_name <- map["company_name"]
    }
    
}
class DealLocationData: Mappable {
    
    var deal_location_id:Int?
    var deal_id:String?
    var phone:String?
    var address:String?
    var street:String?
    var city_name:String?
    var state_name:String?
    var city_id:Int?
    var state_id:Int?
    var zip_code:String?
    var country_id:Int?
    var latitude:Float?
    var longitude:Float?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_location_id <- map["deal_location_id"]
        deal_id <- map["deal_id"]
        phone <- map["phone"]
        address <- map["address"]
        street <- map["street"]
        city_name <- map["city_name"]
        state_name <- map["state_name"]
        city_id <- map["city_id"]
        state_id <- map["state_id"]
        zip_code <- map["zip_code"]
        country_id <- map["country_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
class DealImagesData: Mappable {
    
    var deal_image_id:Int?
    var deal_id:String?
    var image_name:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        deal_image_id <- map["deal_image_id"]
        deal_id <- map["deal_id"]
        image_name <- map["image_name"]
        
    }
}

class GlobalNotificationData: Mappable {
    
    var data:[NotificationData]?
    var current_page:Int?
    var first_page_url:String?
    var from:Int?
    var last_page:Int?
    var last_page_url:String?
    var next_page_url:String?
    var path:String?
    var per_page:Int?
    var prev_page_url:String?
    var to:Int?
    var total:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }
    
}
class NotificationData: Mappable {
    
    var description:String?
    var title:String?
    var image:String?
    var date:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        description <- map["description"]
        title <- map["title"]
        image <- map["image"]
        date <- map["date"]
    }
}

class GetMerchantsData: Mappable {
    
    var data:[UserData]?
    var current_page:Int?
    var last_page:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        last_page <- map["last_page"]
    }
    
}

class CreditLineHistoryData: Mappable {
    
    var credit_line:String?
    var available_credit_line:String?
    var discount_txt:DiscountTextData?
    var video_url:String?
    var history:CreditHistoryData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        credit_line <- map["credit_line"]
        available_credit_line <- map["available_credit_line"]
        discount_txt <- map["discount_txt"]
        video_url <- map["video_url"]
        history <- map["history"]
    }
}
class CreditHistoryData: Mappable {
    
    var data:[HistoryData]?
    var current_page:Int?
    var last_page:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        current_page <- map["current_page"]
        last_page <- map["last_page"]
    }
    
}

class HistoryData: Mappable {
    
    var credit_line_id:Int?
    var credit_line_tran_code:String?
    var credit:Int?
    var credit_balance:Int?
    var transaction_title:String?
    var transaction_type:String?
    var datetime:String?
    var title:String?
    var deal_id:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        credit_line_id <- map["credit_line_id"]
        credit_line_tran_code <- map["credit_line_tran_code"]
        credit <- map["credit"]
        credit_balance <- map["credit_balance"]
        transaction_title <- map["transaction_title"]
        transaction_type <- map["transaction_type"]
        datetime <- map["datetime"]
        title <- map["title"]
        deal_id <- map["deal_id"]
    }
}

class DiscountTextData: Mappable {
    
    var cost_of_goods:Int?
    var fee_to_trade:Int?
    var total_cost_of_trade:Int?
    var saving_on_snagpay:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        cost_of_goods <- map["cost_of_goods"]
        fee_to_trade <- map["fee_to_trade"]
        total_cost_of_trade <- map["total_cost_of_trade"]
        saving_on_snagpay <- map["saving_on_snagpay"]
        
    }
}

class MerchantsAlphabetsData: Mappable {
    
    var alphabet:String?
    var cnt:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        alphabet <- map["alphabet"]
        cnt <- map["cnt"]
        
    }
}
class SearchDealsData: Mappable {
    
    var deals:DealsData?
    var flag:String?
    var category_id:Int?
    var filter_category_id:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        flag <- map["flag"]
        category_id <- map["category_id"]
        deals <- map["deals"]
        filter_category_id <- map["filter_category_id"]
    }
    
}

