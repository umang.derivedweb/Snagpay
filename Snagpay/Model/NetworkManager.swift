//
//  NetworkManager.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import AlamofireObjectMapper
import Alamofire

class NetworkManager: NSObject {
    
    static let shared = NetworkManager()
    
    func webserviceCallCommon(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (CommonResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<CommonResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallGetCities(url:String, headers:HTTPHeaders, completion:@escaping (GetCitiesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetCitiesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetCategory(url:String, headers:HTTPHeaders, completion:@escaping (GetCategoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetCategoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallLoginSignUp(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (LoginRegisterResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<LoginRegisterResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetCategoryDeals(url:String, headers:HTTPHeaders, completion:@escaping (CategoryDealsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<CategoryDealsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetSubCategories(url:String, headers:HTTPHeaders, completion:@escaping (GetSubCategoriesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetSubCategoriesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetDealDetails(url:String, headers:HTTPHeaders, completion:@escaping (DealDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<DealDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetWishlist(url:String, headers:HTTPHeaders, completion:@escaping (GetWishlistResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetWishlistResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetOnlyCategory(url:String, headers:HTTPHeaders, completion:@escaping (CategoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<CategoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallReviewOrder(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (ReviewOrderResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<ReviewOrderResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallSearchDeals(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (SearchDealsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<SearchDealsResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetStates(url:String, headers:HTTPHeaders, completion:@escaping (GetStatesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetStatesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetShippingAddresses(url:String, headers:HTTPHeaders, completion:@escaping (GetShippingAddressesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetShippingAddressesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetAvailableCredits(url:String, headers:HTTPHeaders, completion:@escaping (GetCreditsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetCreditsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetAllOrders(url:String, headers:HTTPHeaders, completion:@escaping (AllOrdersResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<AllOrdersResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetRecentPaymentHistory(url:String, headers:HTTPHeaders, completion:@escaping (RecentPaymentHistoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<RecentPaymentHistoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetMonthlyPaymentDownload(url:String, headers:HTTPHeaders, completion:@escaping (PaymentDownloadResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<PaymentDownloadResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallGetMyWishlist(url:String, headers:HTTPHeaders, completion:@escaping (GetMyWishlistResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetMyWishlistResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetSnagpayWallet(url:String, headers:HTTPHeaders, completion:@escaping (GetSnagpayWalletResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetSnagpayWalletResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetNotiSettings(url:String, headers:HTTPHeaders, completion:@escaping (NotiSettingsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<NotiSettingsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetGiftCards(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (GiftCardsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GiftCardsResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetGiftCardsTransactions(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (GiftCardsTransactionsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GiftCardsTransactionsResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetFullOrderDetails(url:String, headers:HTTPHeaders, completion:@escaping (FullOrderDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<FullOrderDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallCreateOrder(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (CreateOrderResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<CreateOrderResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetOrderDetails(url:String, headers:HTTPHeaders, completion:@escaping (OrderDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<OrderDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallMyAndDraftDeals(url:String, headers:HTTPHeaders, completion:@escaping (MyAndDraftDealsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<MyAndDraftDealsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallTradeHistory(url:String, headers:HTTPHeaders, completion:@escaping (TradeHistoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<TradeHistoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallDownloadReports(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (ReportsDocumentsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<ReportsDocumentsResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetProfile(url:String, headers:HTTPHeaders, completion:@escaping (GetProfileResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetProfileResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallCreateDealorSaveDraft(url:String,mainImageData:[Data],imagesData:[Data], parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (CommonResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                // import image to request
                
                for i in 0..<mainImageData.count {
                    
                    multipartFormData.append(mainImageData[i]  , withName: "main_image", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
                for i in 0..<imagesData.count {
                    multipartFormData.append(imagesData[i]  , withName: "deal_images[\(i)]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to:url,headers:headers)
            {
                (result) in
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                        print(progress)
                    })
                    
                    upload.responseObject { (response: DataResponse<CommonResponse>) in
                        
                        print("URL:\(url)")
                        print(response.result.value as Any)
                        if let _ = response.result.value {
                            
                            completion(response.result.value!)
                        } else {
                            
                        }
                    }
                    
                case .failure(_):
                    print(result)
                // completion(responds)
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallDraftDealDetails(url:String, headers:HTTPHeaders, completion:@escaping (DealDraftDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<DealDraftDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetGlobalNotifications(url:String, headers:HTTPHeaders, completion:@escaping (GlobalNotificationResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GlobalNotificationResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGoodsServiceGiftCards(url:String, headers:HTTPHeaders, completion:@escaping (GiftCardsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GiftCardsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetMerchants(url:String, headers:HTTPHeaders, completion:@escaping (GetMerchantsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GetMerchantsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGiveGifts(url:String, headers:HTTPHeaders, completion:@escaping (GiveGiftResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GiveGiftResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallCreditLineHistory(url:String, parameters:[String:String], headers:HTTPHeaders, completion:@escaping (CreditLineHistoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<CreditLineHistoryResponse>) in
                
                print("URL:\(url) PARAM:\(parameters)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallMerchantsAlphabets(url:String, headers:HTTPHeaders, completion:@escaping (MerchantsAlphabetsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<MerchantsAlphabetsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallUserLocations(url:String, headers:HTTPHeaders, completion:@escaping (UserLocationsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<UserLocationsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetLocation(url:String, headers:HTTPHeaders, completion:@escaping (GetLocationResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GetLocationResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    if response.result.value?.ResponseCode == 401 {
                        
                        Helper.shared.logoutFromApp()
                    }
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    func webserviceCallGetCards(url:String, headers:HTTPHeaders, completion:@escaping (GetCardsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: URLEncoding.default, headers: headers).responseObject { (response: DataResponse<GetCardsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
}
