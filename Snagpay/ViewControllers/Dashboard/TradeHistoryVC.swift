//
//  TradeHistoryVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class TradeHistoryVC: UIViewController {
    
    // MARK: - Variable
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblEquation1: UILabel!
    @IBOutlet weak var lblEquation2: UILabel!
    @IBOutlet weak var lblEquation3: UILabel!
    @IBOutlet weak var lblEquation4: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblMtdSales: UILabel!
    @IBOutlet weak var lblYtdSales: UILabel!
    @IBOutlet weak var lblMtdPurchases: UILabel!
    @IBOutlet weak var lblYtdPurchase: UILabel!
    @IBOutlet weak var lblMtdTradeVoulume: UILabel!
    @IBOutlet weak var lblYtdTradeVolume: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getTradeHistoryAPI()
        creditLineHistoryAPI()
    }
    

    // MARK: - Function
    func setupUI() {
    }
    
    // MARK: - Webservice
    func getTradeHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallTradeHistory(url: URLs.get_my_trading_history, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //self.lblBalance.text = "\(response.data?.cost_of_trade ?? 0.0)"
                self.lblMtdSales.text = response.data?.mtd_sales
                self.lblMtdPurchases.text = response.data?.mtd_purchases
                self.lblMtdTradeVoulume.text = response.data?.mtd_trade_volume
                self.lblYtdSales.text = response.data?.ytd_sales
                self.lblYtdPurchase.text = response.data?.ytd_purchases
                self.lblYtdTradeVolume.text = response.data?.ytd_trade_volume
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func creditLineHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallCreditLineHistory(url: "\(URLs.credit_line_history)", parameters: [:], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblEquation1.text = "\(response.data?.discount_txt?.cost_of_goods ?? 0)%"
                self.lblEquation2.text = "\(response.data?.discount_txt?.fee_to_trade ?? 0)%"
                self.lblEquation3.text = "\(response.data?.discount_txt?.total_cost_of_trade ?? 0)%"
                self.lblEquation4.text = "\(response.data?.discount_txt?.saving_on_snagpay ?? 0)%"
                self.lblBalance.text = response.data?.available_credit_line
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}
