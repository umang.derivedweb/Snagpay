//
//  DashboardVC.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class DashboardVC: UIViewController {

    // MARK: - Variable
    var arrSearchDeals:[DealsListData] = []
    let arr = ["SNAGpay Profile","Reports & Documents","Solutions for Your Business","User Feedback"]
    let arrColour:[UIColor] = [UIColor(named: "App Red")!,UIColor(named: "App Green")!,UIColor(named: "App Blue")!,UIColor(named: "App Yellow")!]
    
    let arrImgs = ["snagpay_profile","reports","solution_business","user_feedback"]
    var arrDeals:[ArrDealsData]?
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    var searchText = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var collview: UICollectionView!
    @IBOutlet weak var collviewDeals: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var viewTabbar: UIView!
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblMtdSales: UILabel!
    @IBOutlet weak var lblYtdSales: UILabel!
    @IBOutlet weak var lblMtdPurchases: UILabel!
    @IBOutlet weak var lblYtdPurchase: UILabel!
    @IBOutlet weak var lblMtdTradeVoulume: UILabel!
    @IBOutlet weak var lblYtdTradeVolume: UILabel!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        getMyDealsAPI()
        getTradeHistoryAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        lblName.text = Helper.shared.strUsername
        lblCity.text = Helper.shared.strCity
        lblCredits.text = Helper.shared.strAvailableCredits
        setupTabbar()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 80
        } else {
            table.rowHeight = 60
        }
    }
    
    func setupTabbar() {
        
        let customView = Bundle.main.loadNibNamed("CustomTabbar", owner: self, options: nil)?.first as? CustomTabbar
        
        customView?.imgHome.image = #imageLiteral(resourceName: "home_active")
        customView?.lblDashboard.textColor = UIColor(named: "App Blue")
        customView?.btnDashboard.addTarget(self, action: #selector(btnDashboard), for: .touchUpInside)
        customView?.btnNotification.addTarget(self, action: #selector(btnNotification), for: .touchUpInside)
        customView?.btnMyProfile.addTarget(self, action: #selector(btnMyProfile), for: .touchUpInside)
        customView?.frame = self.viewTabbar.bounds
        self.viewTabbar.addSubview(customView!)
    }
    
    @objc func btnDashboard() {
        
    }
    @objc func btnMyProfile() {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        navigationController?.pushViewController(obj, animated: false)
    }
    @objc func btnNotification() {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DashboardNotiVC") as! DashboardNotiVC
        navigationController?.pushViewController(obj, animated: false)
    }
    
    @objc func btnDealDetails(sender:UIButton) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = "\(arrDeals?[sender.tag].deal_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnEditDeal(sender:UIButton) {
        
        draftDealDetailsAPI(dealId: "\(arrDeals?[sender.tag].deal_id ?? 0)")
    }
    @objc func btnDelete(sender:UIButton) {
        
        deleteMyDealAPI(deal_id:"\(arrDeals?[sender.tag].deal_id ?? 0)")
    }
    func setTableHeight() {
        DispatchQueue.main.async {
          // your code here
            self.heightTable.constant = self.table.contentSize.height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
        }
    }
    // MARK: - Webservice
    
    func getMyDealsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallMyAndDraftDeals(url: URLs.my_deals+"all", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrDeals = response.data?.data
                self.table.reloadData()
                self.setTableHeight()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func getTradeHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallTradeHistory(url: URLs.get_my_trading_history, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblMtdSales.text = response.data?.mtd_sales
                self.lblMtdPurchases.text = response.data?.mtd_purchases
                self.lblMtdTradeVoulume.text = response.data?.mtd_trade_volume
                self.lblYtdSales.text = response.data?.ytd_sales
                self.lblYtdPurchase.text = response.data?.ytd_purchases
                self.lblYtdTradeVolume.text = response.data?.ytd_trade_volume
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func searchDealsAPI(search:String) {
        Helper.shared.showHUD()
        
        
        let params = ["search":search,
                      "city_id":Helper.shared.strCityId]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallSearchDeals(url: "\(URLs.search_deals)?page=\(currentPage)", parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.view.endEditing(true)
                
                if response.data?.flag == "deals" {
                    if response.data?.deals?.data?.count == 0 {
                        self.scrollview.isHidden = false
                        self.collviewDeals.isHidden = true
                        Toast.show(message: Message.noDealsAvailable, controller: self)
                    } else {
                        self.scrollview.isHidden = true
                        self.collviewDeals.isHidden = false
                        //self.view.bringSubviewToFront(self.collviewDeals)
                    }
                    
                    self.last_page = response.data?.deals?.last_page ?? 0
                    self.arrSearchDeals += response.data?.deals?.data ?? []
                    
                    self.collviewDeals.reloadData()
                } else if response.data?.flag == "category" {
                    
                    let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
                    obj.strCategoryId = "\(response.data?.category_id ?? 0)"
                    
                    if let _ = response.data?.filter_category_id {
                        obj.strCatId = "\(response.data?.filter_category_id ?? 0)"
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func draftDealDetailsAPI(dealId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDraftDealDetails(url: URLs.deal_draft_details+dealId, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "UploadDealVC") as! UploadDealVC
                obj.isFromMyDealVC = true
                obj.dictDraftDetails = response.data
                self.navigationController?.pushViewController(obj, animated: true)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteMyDealAPI(deal_id:String) {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["deal_id"] = deal_id
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.delete_my_deal, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                self.getMyDealsAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMyListing(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyListingVC") as! MyListingVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnMyTrading(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TradeHistoryVC") as! TradeHistoryVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func unwindToDashboardVC(segue: UIStoryboardSegue) {
    }
    
}

extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collview {
            return arr.count
        } else {
            
            return arrSearchDeals.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
            cell.title.text = arr[indexPath.row]
            cell.img.image = UIImage(named: arrImgs[indexPath.row])
            cell.viewBg.backgroundColor = arrColour[indexPath.row]
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.lblCompName.text = arrSearchDeals[indexPath.row].company_name
            cell.lblTitle.text = arrSearchDeals[indexPath.row].title
            cell.lblDistance.text = arrSearchDeals[indexPath.row].city_name
            cell.lblPrice.text = "$ \(arrSearchDeals[indexPath.row].sell_price ?? 0)"
            cell.lblBought.text = "\(arrSearchDeals[indexPath.row].bought ?? "")+ bought"
            cell.lblRating.text = "(\(arrSearchDeals[indexPath.row].total_rating ?? 0) rating)"
            cell.viewRating.rating = Double(arrSearchDeals[indexPath.row].avg_rating ?? 0.0)
            cell.img.sd_setImage(with: URL(string: arrSearchDeals[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collview {
            
            if indexPath.row == 0 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "SnagpayProfileVC") as! SnagpayProfileVC
                navigationController?.pushViewController(obj, animated: true)
            }
            
            if indexPath.row == 1 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 2 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "SolutionVC") as! SolutionVC
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 3 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
                navigationController?.pushViewController(obj, animated: true)
            }
        } else {
            
            let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
            obj.strDealId = "\(arrSearchDeals[indexPath.row].deal_id ?? 0)"
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collview {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: (collectionView.frame.width/2)-5, height: 100)
            } else {
                return CGSize(width: (collectionView.frame.width/2)-5, height: 80)
            }
        } else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: (collectionView.frame.width/2)-5, height: 500)
            }
            return CGSize(width: (collectionView.frame.width/2)-5, height: 400)
        }
        
    }
}
extension DashboardVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrDeals?.count ?? 0 > 3 {
            return 3
        }
        return arrDeals?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyDealCell") as! MyDealCell
        cell.lblTitle.text = arrDeals?[indexPath.row].title
        cell.lblDate.text = arrDeals?[indexPath.row].deal_date
        cell.lblSold.text = "\(arrDeals?[indexPath.row].purchased ?? 0)"
        cell.lblRedeemed.text = "\(arrDeals?[indexPath.row].redeemed ?? 0)"
        cell.lblAvail.text = "\((arrDeals?[indexPath.row].number_of_deal ?? 0) - (arrDeals?[indexPath.row].purchased ?? 0))"
        
        
        ///
        
//        let diff = (arrDeals?[indexPath.row].total_available ?? 0) - (arrDeals?[indexPath.row].sold ?? 0)
//
//
//        if diff <= 0 {
//
//            cell.lblAvail.text = "0"
//        } else {
//            cell.lblAvail.text = "\(diff)"
//        }
        
        ///
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditDeal(sender:)), for: .touchUpInside)
        cell.btnReport.tag = indexPath.row
        cell.btnReport.addTarget(self, action: #selector(btnDealDetails(sender:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDelete(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
}
extension DashboardVC:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtSearch {
            searchText = textField.text ?? ""
            
            if searchText == "" {
                self.scrollview.isHidden = false
                self.collviewDeals.isHidden = true
                return
            }
            intCountArr = 0
            self.arrSearchDeals.removeAll()
            currentPage = 1
            searchDealsAPI(search: textField.text ?? "")
        }
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        if textField == txtSearch {
//            
//            NSObject.cancelPreviousPerformRequests(
//                    withTarget: self,
//                    selector: #selector(self.getHintsFromTextField),
//                    object: textField)
//                self.perform(
//                    #selector(self.getHintsFromTextField),
//                    with: textField,
//                    afterDelay: 0.5)
//        }
//        return true
//    }
//    
//    @objc func getHintsFromTextField(textField: UITextField) {
//        print("Hints for textField: \(textField)")
//        searchText = textField.text ?? ""
//        
//        if searchText == "" {
//            self.scrollview.isHidden = false
//            self.collviewDeals.isHidden = true
//            return
//        }
//        intCountArr = 0
//        self.arrSearchDeals.removeAll()
//        currentPage = 1
//        searchDealsAPI(search: textField.text ?? "")
//    }
}
