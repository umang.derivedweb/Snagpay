//
//  MyDealVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class MyDealVC: UIViewController{
    
    // MARK: - Variable
    var arrDeals:[ArrDealsData] = []
    var arrButtons:[UIButton]!
    var strType:String = "all"
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnLive: UIButton!
    @IBOutlet weak var btnClosed: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getMyDealsAPI(filter: "all")
    }
    

    // MARK: - Function
    func setupUI() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 80
        } else {
            table.rowHeight = 60
        }
        arrButtons = [btnAll,btnLive,btnClosed]
    }
    
    func savePdf(urlString:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(Int.getUniqueRandomNumbers(min: 1000, max: 1500, count: 10)).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
                Toast.show(message: "pdf successfully saved!", controller: self)
            } catch {
                print("Pdf could not be saved")
                Toast.show(message: "Pdf could not be saved", controller: self)
            }
        }
    }
    
    func takeScreenshot(shouldSave: Bool = true) -> UIImage? {
        var screenshotImage :UIImage?
        let keyWindows = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        let layer = keyWindows?.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions((layer?.frame.size)!, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer?.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            Toast.show(message: Message.ssSaved, controller: self)
        }
        return screenshotImage
    }
    @objc func btnDealDetails(sender:UIButton) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = "\(arrDeals[sender.tag].deal_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnEditDeal(sender:UIButton) {
        
        draftDealDetailsAPI(dealId: "\(arrDeals[sender.tag].deal_id ?? 0)")
    }
    @objc func btnDelete(sender:UIButton) {
        
        deleteMyDealAPI(deal_id:"\(arrDeals[sender.tag].deal_id ?? 0)")
    }
    // MARK: - Webservice
    
    func getMyDealsAPI(filter:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallMyAndDraftDeals(url: "\(URLs.my_deals)\(filter)&page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.last_page = response.data?.last_page ?? 0
                self.arrDeals += response.data?.data ?? []
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func myDealsDownloadAPI() {
        Helper.shared.showHUD()
        
        let params:[String:String] = [:]
        
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDownloadReports(url: URLs.my_deals_download, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.savePdf(urlString: response.data?.my_deals_pdf ?? "")
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func draftDealDetailsAPI(dealId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDraftDealDetails(url: URLs.deal_draft_details+dealId, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "UploadDealVC") as! UploadDealVC
                obj.isFromMyDealVC = true
                obj.dictDraftDetails = response.data
                self.navigationController?.pushViewController(obj, animated: true)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteMyDealAPI(deal_id:String) {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["deal_id"] = deal_id
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.delete_my_deal, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.intCountArr = 0
                self.arrDeals.removeAll()
                self.currentPage = 1
                self.getMyDealsAPI(filter: self.strType)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnNewCampaign(_ sender: Any) {
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHeaderFilter(_ sender: UIButton) {
        
        for i in 0..<arrButtons.count {
            
            if sender.tag == arrButtons[i].tag {
                arrButtons[i].setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                arrButtons[i].backgroundColor = #colorLiteral(red: 0.04705882353, green: 0.2745098039, blue: 0.6941176471, alpha: 1)
                
                intCountArr = 0
                arrDeals.removeAll()
                currentPage = 1
                strType = arrButtons[i].accessibilityLabel!
                getMyDealsAPI(filter: arrButtons[i].accessibilityLabel!)
            } else {
                
                arrButtons[i].setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                arrButtons[i].backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
       
    }
    
    @IBAction func btnDownload(_ sender: Any) {
        
        myDealsDownloadAPI()
    }
    @IBAction func btnTakeSS(_ sender: Any) {
        takeScreenshot(shouldSave: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension MyDealVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeals.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyDealCell") as! MyDealCell
        cell.lblTitle.text = arrDeals[indexPath.row].title
        cell.lblDate.text = arrDeals[indexPath.row].deal_date
        cell.lblSold.text = "\(arrDeals[indexPath.row].purchased ?? 0)"
        cell.lblRedeemed.text = "\(arrDeals[indexPath.row].redeemed ?? 0)"
        cell.lblAvail.text = "\((arrDeals[indexPath.row].number_of_deal ?? 0) - (arrDeals[indexPath.row].purchased ?? 0))"
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditDeal(sender:)), for: .touchUpInside)
        cell.btnReport.tag = indexPath.row
        cell.btnReport.addTarget(self, action: #selector(btnDealDetails(sender:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDelete(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrDeals.count {
            if (indexPath.row == (self.arrDeals.count ) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrDeals.count
                    getMyDealsAPI(filter: strType ?? "")
                }
                
            }
        }
    }
    
}
