//
//  DealLocationVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class DealLocationVC: UIViewController {
    
    // MARK: - Variable
    var arrLocationName:[String] = [""]
    var arrStreet:[String] = [""]
    var arrLocationState:[String] = ["AZ"]
    var arrLocationCity:[String] = [""]
    var arrZip:[String] = [""]
    var arrPhone:[String] = [""]
    
    var arrCityId:[String] = [""]
    var arrStateId:[String] = ["3"]
    var arrLat:[String] = [""]
    var arrLong:[String] = [""]
    var arrCountryId:[String] = ["1"]
    
    var picker:UIPickerView?
    var tappedTextfield:UITextField?
    var arrStates:[StatesData]?
    var arrCities:[GetCitiesData]?
    var intSelectedRow:Int!
    
    var imgMain:UIImage?
    var arrImages:[UIImage]?
    var params:[String:Any]?
    
    //get draft
    var strDraft_token = ""
    var isFromDraftDealVC = false
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
   
    // MARK: - IBOutlet
    @IBOutlet weak var btnSaveDraft: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnAddMore: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        self.hideKeyboardWhenTappedAround()
        getStatesAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 180
        
        if isFromDraftDealVC {
            
            setDraftDetails()
        }
        if isFromMyDealVC {
            btnSaveDraft.isHidden = true
            setDraftDetails()
        }
    }
    
    func setDraftDetails() {
        
        if dictDraftDetails?.deal_locations?.count != 0 {
            
            arrLocationName.removeAll()
            arrStreet.removeAll()
            arrLocationState.removeAll()
            arrLocationCity.removeAll()
            arrZip.removeAll()
            arrPhone.removeAll()
            arrCityId.removeAll()
            arrLat.removeAll()
            arrLong.removeAll()
            arrStateId.removeAll()
            arrCountryId.removeAll()
        }
        
        for i in 0..<(dictDraftDetails?.deal_locations?.count ?? 0) {
            
            arrLocationName.append(dictDraftDetails?.deal_locations?[i].address ?? "")
            arrStreet.append(dictDraftDetails?.deal_locations?[i].street ?? "")
            arrLocationState.append(dictDraftDetails?.deal_locations?[i].state_name ?? "")
            arrLocationCity.append(dictDraftDetails?.deal_locations?[i].city_name ?? "")
            arrZip.append(dictDraftDetails?.deal_locations?[i].zip_code ?? "")
            arrPhone.append(dictDraftDetails?.deal_locations?[i].phone ?? "")
            arrCityId.append("\(dictDraftDetails?.deal_locations?[i].city_id ?? 0)")
            arrStateId.append("\(dictDraftDetails?.deal_locations?[i].state_id ?? 0)")
            arrLat.append("\(dictDraftDetails?.deal_locations?[i].latitude ?? 0)")
            arrLong.append("\(dictDraftDetails?.deal_locations?[i].longitude ?? 0)")
            arrCountryId.append("\(dictDraftDetails?.deal_locations?[i].country_id ?? 0)")
            
        }
        table.reloadData()
    }
    @objc func btnRemoveDeal(sender:UIButton) {
        
        if arrLocationName.count == 1 {
            return
        }
        arrLocationName.remove(at: sender.tag)
        arrStreet.remove(at: sender.tag)
        arrLocationState.remove(at: sender.tag)
        arrLocationCity.remove(at: sender.tag)
        arrZip.remove(at: sender.tag)
        arrPhone.remove(at: sender.tag)
        arrCityId.remove(at: sender.tag)
        arrLat.remove(at: sender.tag)
        arrLong.remove(at: sender.tag)
        arrStateId.remove(at: sender.tag)
        arrCountryId.remove(at: sender.tag)
        table.reloadData()
        
        btnAddMore.isHidden = false
    }
    
    
    func toolBar(textField:UITextField) {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        if tappedTextfield?.accessibilityLabel == "State" {
            
            arrLocationState.remove(at: tappedTextfield!.tag)
            arrLocationState.insert(arrStates?[intSelectedRow].state_code ?? "", at: tappedTextfield!.tag)
            arrStateId.remove(at: tappedTextfield!.tag)
            arrStateId.insert("\(arrStates?[intSelectedRow].state_id ?? 0)", at: tappedTextfield!.tag)
            
            arrLocationCity.remove(at: tappedTextfield!.tag)
            arrLocationCity.insert("", at: tappedTextfield!.tag)
            arrCityId.remove(at: tappedTextfield!.tag)
            arrCityId.insert("", at: tappedTextfield!.tag)
            
            arrLat.remove(at: tappedTextfield!.tag)
            arrLat.insert("", at: tappedTextfield!.tag)
            arrLong.remove(at: tappedTextfield!.tag)
            arrLong.insert("", at: tappedTextfield!.tag)
            
            arrCountryId.remove(at: tappedTextfield!.tag)
            arrCountryId.insert("\(arrStates?[intSelectedRow].country_id ?? 0)", at: tappedTextfield!.tag)
            
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            
            arrLocationCity.remove(at: tappedTextfield!.tag)
            arrLocationCity.insert(arrCities?[intSelectedRow].city_name ?? "", at: tappedTextfield!.tag)
            arrCityId.remove(at: tappedTextfield!.tag)
            arrCityId.insert("\(arrCities?[intSelectedRow].city_id ?? 0)", at: tappedTextfield!.tag)
            arrLat.remove(at: tappedTextfield!.tag)
            arrLat.insert("\(arrCities?[intSelectedRow].latitude ?? 0)", at: tappedTextfield!.tag)
            arrLong.remove(at: tappedTextfield!.tag)
            arrLong.insert("\(arrCities?[intSelectedRow].longitude ?? 0)", at: tappedTextfield!.tag)
            
        }
        table.reloadData()
        view.endEditing(true)
        
        
    }
    @objc func cancelClick() {
        view.endEditing(true)
    }
    // MARK: - Webservice
    func getCitiesAPI(stateId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=\(stateId)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.picker?.reloadAllComponents()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func getStatesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetStates(url: "\(URLs.get_states)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrStates = response.data
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func saveDraftAPI(params:[String:Any]) {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        for i in 0..<(arrImages?.count ?? 0) {
            
            imagesData.append((arrImages?[i].jpegData(compressionQuality: 0.5)!)!)
        }
        
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain != nil {
            
            mainImageData.append((imgMain?.jpegData(compressionQuality: 0.5))!)
        }
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.save_draft, mainImageData: mainImageData, imagesData: imagesData, parameters: params as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        

        for i in 0..<arrLocationName.count {
            
            if isFromMyDealVC {
                
                if dictDraftDetails?.deal_locations?.count != 0 {
                    params?["deal_location_ids[\(i+1)]"] = "\(dictDraftDetails?.deal_locations?[i].deal_location_id ?? 0)"
                }
                
            }
            
            params?["address[\(i+1)]"] = arrLocationName[i]
            
            params?["street[\(i+1)]"] = arrStreet[i]
            params?["state_id[\(i+1)]"] = arrStateId[i]
            params?["city_id[\(i+1)]"] = arrCityId[i]
            params?["zip_code[\(i+1)]"] = arrZip[i]
            params?["phone[\(i+1)]"] = arrPhone[i]
            params?["latitude[\(i+1)]"] = arrLat[i]
            params?["longitude[\(i+1)]"] = arrLong[i]
            params?["country_id[\(i+1)]"] = arrCountryId[i]
        }
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealAgreementVC") as! DealAgreementVC
        obj.arrImages = arrImages
        obj.imgMain = imgMain
        obj.params = params
        
        obj.isFromDraftDealVC = isFromDraftDealVC
        obj.isFromMyDealVC = isFromMyDealVC
        obj.dictDraftDetails = dictDraftDetails
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnAddMore(_ sender: Any) {
        btnAddMore.isHidden = true
        
        arrLocationName.append("")
        arrStreet.append("")
        arrLocationState.append("AZ")
        arrLocationCity.append("")
        arrZip.append("")
        arrPhone.append("")
        arrCityId.append("")
        arrStateId.append("3")
        arrLat.append("")
        arrLong.append("")
        arrCountryId.append("1")
        table.reloadData()
    }
    
    @IBAction func btnSaveDraft(_ sender: Any) {
        
        params?["draft_token"] = strDraft_token//todo
        
        for i in 0..<arrLocationName.count {
            
            params?["address[\(i+1)]"] = arrLocationName[i]
            
            params?["street[\(i+1)]"] = arrStreet[i]
            params?["state_id[\(i+1)]"] = arrStateId[i]
            params?["city_id[\(i+1)]"] = arrCityId[i]
            params?["zip_code[\(i+1)]"] = arrZip[i]
            params?["phone[\(i+1)]"] = arrPhone[i]
            params?["latitude[\(i+1)]"] = arrLat[i]
            params?["longitude[\(i+1)]"] = arrLong[i]
            params?["country_id[\(i+1)]"] = arrCountryId[i]
        }
        
        saveDraftAPI(params: params ?? [:])
    }
    
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}

extension DealLocationVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLocationName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealLocationCell") as! DealLocationCell
        cell.lblLocation.text = "Location \(indexPath.row+1)"
        cell.txtLocationName.text = arrLocationName[indexPath.row]
        cell.txtStreet.text = arrStreet[indexPath.row]
        cell.txtState.text = arrLocationState[indexPath.row]
        cell.txtCity.text = arrLocationCity[indexPath.row]
        cell.txtZip.text = arrZip[indexPath.row]
        cell.txtPhone.text = arrPhone[indexPath.row]
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveDeal(sender:)), for: .touchUpInside)
        
        cell.txtLocationName.accessibilityLabel = "LocationName"
        cell.txtStreet.accessibilityLabel = "Street"
        cell.txtState.accessibilityLabel = "State"
        cell.txtCity.accessibilityLabel = "City"
        cell.txtZip.accessibilityLabel = "Zip"
        cell.txtPhone.accessibilityLabel = "Phone"
        
        cell.txtLocationName.delegate = self
        cell.txtStreet.delegate = self
        cell.txtState.delegate = self
        cell.txtCity.delegate = self
        cell.txtZip.delegate = self
        cell.txtPhone.delegate = self
        
        cell.txtLocationName.tag = indexPath.row
        cell.txtStreet.tag = indexPath.row
        cell.txtState.tag = indexPath.row
        cell.txtCity.tag = indexPath.row
        cell.txtZip.tag = indexPath.row
        cell.txtPhone.tag = indexPath.row
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
}

extension DealLocationVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.accessibilityLabel == "State" || textField.accessibilityLabel == "City" {
            
            tappedTextfield = textField
            intSelectedRow = nil
            
            if textField.accessibilityLabel == "State" {
                picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
                picker?.reloadAllComponents()
                toolBar(textField: textField)
                
            }
            if textField.accessibilityLabel == "City" {
                
                if arrLocationState[textField.tag] == "" {
                    view.endEditing(true)
                    Toast.show(message: "Please select state first", controller: self)
                    return
                } else {
                    
                    getCitiesAPI(stateId: arrStateId[textField.tag])
                    
                    self.picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
                    self.picker?.reloadAllComponents()
                    self.toolBar(textField: textField)
                }
            }
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("END EDITING CALLED")
        
        if textField.accessibilityLabel == "LocationName" {
            
            arrLocationName.remove(at: textField.tag)
            arrLocationName.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Street" {
            
            arrStreet.remove(at: textField.tag)
            arrStreet.insert(textField.text ?? "", at: textField.tag)
        }
//        if textField.accessibilityLabel == "State" {
//            
//            arrState.remove(at: textField.tag)
//            arrState.insert(textField.text ?? "", at: textField.tag)
//        }
//        if textField.accessibilityLabel == "City" {
//            
//            arrCity.remove(at: textField.tag)
//            arrCity.insert(textField.text ?? "", at: textField.tag)
//        }
        if textField.accessibilityLabel == "Zip" {
            
            arrZip.remove(at: textField.tag)
            arrZip.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Phone" {
            
            arrPhone.remove(at: textField.tag)
            arrPhone.insert(textField.text ?? "", at: textField.tag)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.accessibilityLabel == "Phone" {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXX-XXX-XXXX", phone: newString)
                return false
        }
        if textField.accessibilityLabel == "Zip" {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXXXX", phone: newString)
                return false
        }
        
        if textField.accessibilityLabel == "State" || textField.accessibilityLabel == "City" {
            
            return false
        }
        return true
    }
}
extension DealLocationVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfield?.accessibilityLabel == "State" {
            return arrStates?.count ?? 0
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            return arrCities?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfield?.accessibilityLabel == "State" {
            return arrStates?[row].state_code
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            return arrCities?[row].city_name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
    
}
