//
//  SnagpayProfileVC.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit

class SnagpayProfileVC: UIViewController {

    // MARK: - Variable

    var isSelectedHomeVC = false
    
    var arr = ["Upload a SNAGpay deal","My wishlist","Manage wishlist","My discount","Search directory","Help"]
    
    // MARK: - IBOutlet
    
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 80
        } else {
            table.rowHeight = 60
        }
        table.tableFooterView = UIView()
    }
    
    // MARK: - Webservice
    
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}
extension SnagpayProfileVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SnagpayProfileCell") as! SnagpayProfileCell
        cell.lblTitle.text = arr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "UploadDealVC") as! UploadDealVC
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 1 {
            
            let obj = UIStoryboard(name: "Wishlist", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 2 {
            
            let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "ManageWishlistVC") as! ManageWishlistVC
            obj.isFromVC = "Dashboard"
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 3 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DiscountVC") as! DiscountVC
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 4 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchDirecoryCategoryVC") as! SearchDirecoryCategoryVC
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 5 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
            navigationController?.pushViewController(obj, animated: true)
        }
        
        
    }
    
    
}
