//
//  MyProfileVC.swift
//  Snagpay
//
//  Created by Apple on 23/09/21.
//

import UIKit
import SVGKit

class MyProfileVC: UIViewController {
    
    // MARK: - Variable
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewTabbar: UIView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtWorkphone: UITextField!
    @IBOutlet weak var txtCellPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtPostcode: UITextField!
    @IBOutlet weak var imgQR: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getProfileAPI()
    }
   

    // MARK: - Function
    
    func setupUI() {
        
        setupTabbar()
    }
    func setupTabbar() {
        
        let customView = Bundle.main.loadNibNamed("CustomTabbar", owner: self, options: nil)?.first as? CustomTabbar
        
        customView?.imgProfile.image = #imageLiteral(resourceName: "profile_active")
        customView?.lblMyProfile.textColor = UIColor(named: "App Blue")
        customView?.btnDashboard.addTarget(self, action: #selector(btnDashboard), for: .touchUpInside)
        customView?.btnNotification.addTarget(self, action: #selector(btnNotification), for: .touchUpInside)
        customView?.btnMyProfile.addTarget(self, action: #selector(btnMyProfile), for: .touchUpInside)
        customView?.frame = self.viewTabbar.bounds
        self.viewTabbar.addSubview(customView!)
    }
    
    @objc func btnDashboard() {
        
        performSegue(withIdentifier: "segueDashboard", sender: self)
    }
    @objc func btnMyProfile() {
        
        
    }
    @objc func btnNotification() {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DashboardNotiVC") as! DashboardNotiVC
        navigationController?.pushViewController(obj, animated: false)
    }
    
    //MARK: - Webservice
    func getProfileAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetProfile(url: URLs.get_profile, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                
                self.txtFname.text = response.data?.user?.first_name
                self.txtLname.text = response.data?.user?.last_name
                self.txtEmail.text = response.data?.user?.email
                self.txtWorkphone.text = response.data?.user?.work_phone
                self.txtCellPhone.text = response.data?.user?.cell_phone
                self.txtAddress.text = response.data?.user?.address
                self.txtCity.text = response.data?.user?.city
                self.txtState.text = response.data?.user?.state
                self.txtPostcode.text = response.data?.user?.postcode
                
                let url = URL(string: response.data?.user?.qrcode ?? "")
                
                if let data = try? Data(contentsOf: url!)
                {
                    let anSVGImage: SVGKImage = SVGKImage(data: data)
                    self.imgQR.image = anSVGImage.uiImage
                }
                
                NotificationCenter.default.post(name: Notification.Name("SendProfileData"), object: response.data)

                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnTopHeader(_ sender: UIButton) {
        
        if sender.tag == 1 {
            containerview.isHidden = true
            (view.viewWithTag(2) as! UILabel).backgroundColor = UIColor(named: "App Blue")
            (view.viewWithTag(3) as! UILabel).textColor = .black
            (view.viewWithTag(12) as! UILabel).backgroundColor = .lightGray
            (view.viewWithTag(13) as! UILabel).textColor = .lightGray
        } else {
            containerview.isHidden = false
            view.bringSubviewToFront(containerview)
            (view.viewWithTag(2) as! UILabel).backgroundColor = .lightGray
            (view.viewWithTag(3) as! UILabel).textColor = .lightGray
            (view.viewWithTag(12) as! UILabel).backgroundColor = UIColor(named: "App Blue")
            (view.viewWithTag(13) as! UILabel).textColor = .black
        }
    }
    
}
extension MyProfileVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtPostcode {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXXXX", phone: newString)
                return false
        }
        if textField == txtWorkphone {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXX-XXX-XXXX", phone: newString)
            return false
        }
        if textField == txtCellPhone {
            
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
