//
//  BusinessDetailsVC.swift
//  Snagpay
//
//  Created by Apple on 23/09/21.
//

import UIKit

class BusinessDetailsVC: UIViewController {

    // MARK: - Variable
    var arrTitle:[String] = []
    var arrId:[String] = []
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    var strNoOfLocations:String?
    var intSelectedNoOfLocations:Int?
    
    var intSelectedTextfieldTag:Int?
    var strCatId:String?
    
    var arrNoOfLocations:[String] = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15",]
    var arrTypeBusiness:[GetCategoryData]?
    var arrCost:[String] = []
    var arrHowlong:[String] = ["1","2","3","4","5"]
    

    var dict:GetProfileData?
    var tempNoOfLocations = 0
    ////
    var arrLocationId:[String] = []
    var arrEmail:[String] = []
    var arrWorkphone:[String] = []
    var arrAddress:[String] = []
    var arrLocationCity:[String] = []
    var arrLocationState:[String] = []
    var arrPostcode:[String] = []
    var arrEIN:[String] = []
    var arrEntityName:[String] = []
    
    var arrCityId:[String] = [""]
    var arrStateId:[String] = [""]
    ////
    var tappedTextfield:UITextField?
    var arrStates:[StatesData]?
    var arrCities:[GetCitiesData]?
    var intSelectedRow:Int!
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var txtEntityName: UITextField!
    @IBOutlet weak var txtEIN: UITextField!
    @IBOutlet weak var txtBusinessName: UITextField!
    @IBOutlet weak var txtNoOfLocation: UITextField!
    @IBOutlet weak var txtDescribeBusiness: UITextField!
    @IBOutlet weak var txtTypeBusiness: UITextField!
    @IBOutlet weak var txtCost: UITextField!
    @IBOutlet weak var txtWebsite: UITextField!
    @IBOutlet weak var txtHowlong: UITextField!
    @IBOutlet weak var txtAvgSales: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        self.hideKeyboardWhenTappedAround()
        getStatesAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 800
        
        arrCost.removeAll()
        for i in 1..<101 {
            
            arrCost.append("\(i)%")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileData(notification:)), name: Notification.Name("SendProfileData"), object: nil)
        
    }
    
    @objc func getProfileData(notification: Notification) {
        
        let dict = notification.object as! GetProfileData
        self.dict = dict
        
        txtEntityName.text = dict.user?.entity_name
        txtEIN.text = dict.user?.ein
        txtBusinessName.text = dict.user?.business_name
        txtNoOfLocation.text = dict.user?.no_of_physical_locations
        txtDescribeBusiness.text = dict.user?.describe_business
        txtTypeBusiness.text = dict.user?.category_name
        strCatId = "\(dict.user?.type_of_business ?? 0)"
        txtCost.text = "\(dict.user?.cost_of_goods ?? 0)%"
        txtWebsite.text = dict.user?.website_or_page
        txtHowlong.text = dict.user?.how_long_have_you
        txtAvgSales.text = "\(dict.user?.avg_sales_per_month ?? 0)"
       
        self.arrTypeBusiness = dict.categories
            
        self.strNoOfLocations = dict.user?.no_of_physical_locations
        self.intSelectedNoOfLocations = Int(self.strNoOfLocations ?? "0")
            
        
        /////
        
        arrLocationId.removeAll()
        arrEmail.removeAll()
        arrWorkphone.removeAll()
        arrAddress.removeAll()
        arrLocationCity.removeAll()
        arrLocationState.removeAll()
        arrPostcode.removeAll()
        arrEIN.removeAll()
        arrEntityName.removeAll()
        arrStateId.removeAll()
        arrCityId.removeAll()
        for i in 0..<(dict.locations?.count ?? 0) {
            
            arrLocationId.append("\(dict.locations?[i].user_location_id ?? 0)")
            arrEmail.append(dict.locations?[i].email ?? "")
            arrWorkphone.append(dict.locations?[i].work_phone ?? "")
            arrAddress.append(dict.locations?[i].address ?? "")
            arrLocationCity.append(dict.locations?[i].city_name ?? "")
            arrLocationState.append(dict.locations?[i].state_code ?? "")
            arrPostcode.append(dict.locations?[i].postcode ?? "")
            arrEIN.append(dict.locations?[i].ein ?? "")
            arrEntityName.append(dict.locations?[i].entity_name ?? "")
            arrCityId.append("\(dict.locations?[i].city_id ?? 0)")
            arrStateId.append("\(dict.locations?[i].state_id ?? 0)")
        }
        
        table.reloadData()
        setTableHeight()
        
        ////
        
    }
    
    func setTableHeight() {
//        DispatchQueue.main.async {
//          // your code here
//            self.heightTable.constant = self.table.contentSize.height
//            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
//        }
    }
    
    
    func setupLocationArrays() {
        
        if (intSelectedNoOfLocations ?? 0) >= Int(strNoOfLocations ?? "0")! {
            
            arrLocationId.removeAll()
            arrEmail.removeAll()
            arrWorkphone.removeAll()
            arrAddress.removeAll()
            arrLocationCity.removeAll()
            arrLocationState.removeAll()
            arrPostcode.removeAll()
            arrEIN.removeAll()
            arrEntityName.removeAll()
            arrCityId.removeAll()
            arrStateId.removeAll()
            for i in 0..<(dict?.locations?.count ?? 0) {
                
                arrLocationId.append("\(dict?.locations?[i].user_location_id ?? 0)")
                arrEmail.append(dict?.locations?[i].email ?? "")
                arrWorkphone.append(dict?.locations?[i].work_phone ?? "")
                arrAddress.append(dict?.locations?[i].address ?? "")
                arrLocationCity.append(dict?.locations?[i].city_name ?? "")
                arrLocationState.append(dict?.locations?[i].state_code ?? "")
                arrPostcode.append(dict?.locations?[i].postcode ?? "")
                arrEIN.append(dict?.locations?[i].ein ?? "")
                arrEntityName.append(dict?.locations?[i].entity_name ?? "")
                arrCityId.append("\(dict?.locations?[i].city_id ?? 0)")
                arrStateId.append("\(dict?.locations?[i].state_id ?? 0)")
            }
            
            let diff = (intSelectedNoOfLocations ?? 0) - Int(strNoOfLocations ?? "0")!
            for _ in 0..<diff {
                
                arrLocationId.append("")
                arrEmail.append("")
                arrWorkphone.append("")
                arrAddress.append("")
                arrLocationCity.append("")
                arrLocationState.append("")
                arrPostcode.append("")
                arrEIN.append("")
                arrEntityName.append("")
                arrCityId.append("")
                arrStateId.append("")
            }
        } else {
            
            arrLocationId.removeAll()
            arrEmail.removeAll()
            arrWorkphone.removeAll()
            arrAddress.removeAll()
            arrLocationCity.removeAll()
            arrLocationState.removeAll()
            arrPostcode.removeAll()
            arrEIN.removeAll()
            arrEntityName.removeAll()
            arrCityId.removeAll()
            arrStateId.removeAll()
            
            for _ in 0..<(intSelectedNoOfLocations ?? 0) {
                
                arrLocationId.append("")
                arrEmail.append("")
                arrWorkphone.append("")
                arrAddress.append("")
                arrLocationCity.append("")
                arrLocationState.append("")
                arrPostcode.append("")
                arrEIN.append("")
                arrEntityName.append("")
                arrCityId.append("")
                arrStateId.append("")
            }
        }
        
        table.reloadData()
        setTableHeight()
    }
    func toolBar(textField:UITextField) {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        
        if intSelectedTextfieldTag == 101 {
            intSelectedNoOfLocations = tempNoOfLocations
            setupLocationArrays()
            view.endEditing(true)
        }
        
        if intSelectedRow == nil {
            return
        }
        
        if tappedTextfield?.accessibilityLabel == "State" {
            
            arrLocationState.remove(at: tappedTextfield!.tag)
            arrLocationState.insert(arrStates?[intSelectedRow].state_code ?? "", at: tappedTextfield!.tag)
            arrStateId.remove(at: tappedTextfield!.tag)
            arrStateId.insert("\(arrStates?[intSelectedRow].state_id ?? 0)", at: tappedTextfield!.tag)
            
            arrLocationCity.remove(at: tappedTextfield!.tag)
            arrLocationCity.insert("", at: tappedTextfield!.tag)
            arrCityId.remove(at: tappedTextfield!.tag)
            arrCityId.insert("", at: tappedTextfield!.tag)
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            
            arrLocationCity.remove(at: tappedTextfield!.tag)
            arrLocationCity.insert(arrCities?[intSelectedRow].city_name ?? "", at: tappedTextfield!.tag)
            arrCityId.remove(at: tappedTextfield!.tag)
            arrCityId.insert("\(arrCities?[intSelectedRow].city_id ?? 0)", at: tappedTextfield!.tag)
        }
        table.reloadData()
        view.endEditing(true)
    }
    @objc func cancelClick() {
        view.endEditing(true)
    }
    
    // MARK: - Webservice
    func updateProfileAPI() {
        Helper.shared.showHUD()

        let obj = self.parent as! MyProfileVC
        
        var params:[String:String] = [:]
        
        params["first_name"] = obj.txtFname.text
        params["last_name"] = obj.txtLname.text
        params["work_phone"] = obj.txtWorkphone.text
        params["cell_phone"] = obj.txtCellPhone.text
        params["address"] = obj.txtAddress.text
        params["city"] = obj.txtCity.text
        params["state"] = obj.txtState.text
        params["postcode"] = obj.txtPostcode.text
        params["business_name"] = txtBusinessName.text
        params["describe_business"] = txtDescribeBusiness.text
        params["type_of_business"] = strCatId
        params["cost_of_goods"] = (txtCost.text)?.replacingOccurrences(of: "%", with: "")
        params["website_or_page"] = txtWebsite.text
        params["no_of_physical_locations"] = txtNoOfLocation.text
        params["how_long_have_you"] = txtHowlong.text
        params["avg_sales_per_month"] = txtAvgSales.text
        params["entity_name"] = txtEntityName.text
        params["ein"] = txtEIN.text

//        for i in 0..<arrLocationId.count {
//            
//            params["arr_user_location_id[\(i)]"] = arrLocationId[i]
//        }
//        for i in 0..<arrEmail.count {
//            
//            params["arr_email[\(i)]"] = arrEmail[i]
//        }
//        for i in 0..<arrEntityName.count {
//            
//            params["arr_entity_name[\(i)]"] = arrEntityName[i]
//        }
//        for i in 0..<arrEIN.count {
//            
//            params["arr_ein[\(i)]"] = arrEIN[i]
//        }
//        for i in 0..<arrWorkphone.count {
//            
//            params["arr_work_phone[\(i)]"] = arrWorkphone[i]
//        }
//        for i in 0..<arrAddress.count {
//            
//            params["arr_address[\(i)]"] = arrAddress[i]
//        }
//        for i in 0..<arrCityId.count {
//            
//            params["arr_city_id[\(i)]"] = arrCityId[i]
//        }
//        for i in 0..<arrStateId.count {
//            
//            params["arr_state_id[\(i)]"] = arrStateId[i]
//        }
//        for i in 0..<arrPostcode.count {
//            
//            params["arr_postcode[\(i)]"] = arrPostcode[i]
//        }

        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]

        NetworkManager.shared.webserviceCallCommon(url: URLs.update_profile, parameters: params, headers:headers) { (response) in

            if response.ResponseCode == 200 {

                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func getCitiesAPI(stateId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=\(stateId)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.picker.reloadAllComponents()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func getStatesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetStates(url: "\(URLs.get_states)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrStates = response.data
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnSave(_ sender: Any) {
        
        
        let obj = self.parent as! MyProfileVC
        
        if obj.txtFname.text == "" {
            Toast.show(message: "Please enter First Name", controller: self)
            return
        }
        if obj.txtLname.text == "" {
            Toast.show(message: "Please enter Last Name", controller: self)
            return
        }
        if obj.txtWorkphone.text == "" {
            Toast.show(message: "Please enter Work Phone", controller: self)
            return
        }
        if obj.txtCellPhone.text == "" {
            Toast.show(message: "Please enter Cell Phone", controller: self)
            return
        }
        if obj.txtAddress.text == "" {
            Toast.show(message: "Please enter Address", controller: self)
            return
        }
        if obj.txtCity.text == "" {
            Toast.show(message: "Please enter City", controller: self)
            return
        }
        if obj.txtState.text == "" {
            Toast.show(message: "Please enter State", controller: self)
            return
        }
        if obj.txtPostcode.text == "" {
            Toast.show(message: "Please enter Zip code", controller: self)
            return
        }
        if txtBusinessName.text == "" {
            Toast.show(message: "Please enter Business Name", controller: self)
            return
        }
        if txtDescribeBusiness.text == "" {
            Toast.show(message: "Please enter Describe Business", controller: self)
            return
        }
        if txtTypeBusiness.text == "" {
            Toast.show(message: "Please enter Type of Business", controller: self)
            return
        }
        if txtCost.text == "" {
            Toast.show(message: "Please enter Cost of Goods", controller: self)
            return
        }
        if txtWebsite.text == "" {
            Toast.show(message: "Please enter Website", controller: self)
            return
        }
//        if txtNoOfLocation.text == "" {
//            Toast.show(message: "Please enter Number of Locations", controller: self)
//            return
//        }
        if txtHowlong.text == "" {
            Toast.show(message: "Please enter Howlong have you been in business", controller: self)
            return
        }
        if txtAvgSales.text == "" {
            Toast.show(message: "Please enter Average sales of month", controller: self)
            return
        }
        if txtEntityName.text == "" {
            Toast.show(message: "Please enter Entity Name", controller: self)
            return
        }
        if txtEIN.text == "" {
            Toast.show(message: "Please enter EIN", controller: self)
            return
        }

        
//        if let index = arrEmail.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter email in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrWorkphone.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter work phone in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrAddress.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter address in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrLocationCity.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter city in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrLocationState.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter state in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrPostcode.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter zip code in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrEIN.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter EIN (Entity number) in location \(index+1)", controller: self)
//            return
//        }
//        if let index = arrEntityName.firstIndex(of: "") {
//
//            Toast.show(message: "Please enter entity name in location \(index+1)", controller: self)
//            return
//        }
//
//        for i in 0..<arrEmail.count {
//
//            if !arrEmail[i].isValidEmail() {
//                Toast.show(message: "Please enter valid email in location \(i+1)", controller: self)
//                return
//            }
//
//        }
        updateProfileAPI()
        
    }
    
}

extension BusinessDetailsVC:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.accessibilityLabel == "State" || textField.accessibilityLabel == "City" {
            
            intSelectedTextfieldTag = nil
            tappedTextfield = textField
            intSelectedRow = nil
            
            if textField.accessibilityLabel == "State" {
                picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
                picker.reloadAllComponents()
                toolBar(textField: textField)
                
            }
            if textField.accessibilityLabel == "City" {
                
                if arrLocationState[textField.tag] == "" {
                    view.endEditing(true)
                    Toast.show(message: "Please select state first", controller: self)
                    return
                } else {
                    
                    getCitiesAPI(stateId: arrStateId[textField.tag])
                    
                    self.picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
                    self.picker.reloadAllComponents()
                    self.toolBar(textField: textField)
                }
            }
        }
        
        if textField.tag == 101 || textField.tag == 102 || textField.tag == 103 || textField.tag == 104 {
            
            tappedTextfield = textField
            intSelectedTextfieldTag = textField.tag
            arrTitle.removeAll()
            arrId.removeAll()
            
            if intSelectedTextfieldTag == 101 {
                
                for i in 0..<arrNoOfLocations.count {
                    arrTitle.append(arrNoOfLocations[i])
                    arrId.append(arrNoOfLocations[i])
                }
            }
            if intSelectedTextfieldTag == 102 {
                
                for i in 0..<(arrTypeBusiness?.count ?? 0) {
                    arrTitle.append(arrTypeBusiness?[i].category_name ?? "")
                    arrId.append("\(arrTypeBusiness?[i].category_id ?? 0)")
                }
            }
            if intSelectedTextfieldTag == 103 {
                
                for i in 0..<arrCost.count {
                    arrTitle.append(arrCost[i])
                    arrId.append(arrCost[i])
                }
            }
            if intSelectedTextfieldTag == 104 {
                
                for i in 0..<arrHowlong.count {
                    arrTitle.append(arrHowlong[i])
                    arrId.append(arrHowlong[i])
                }
            }
            
            picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
            picker.reloadAllComponents()
            toolBar(textField: textField)
        } 
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("DID END EDITING CALLED")
        if textField.tag == 101 || textField.tag == 102 || textField.tag == 103 || textField.tag == 104 {
            
            return
        }
        
        
        if textField.accessibilityLabel == "Email" {
            
            arrEmail.remove(at: textField.tag)
            arrEmail.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Workphone" {
            
            arrWorkphone.remove(at: textField.tag)
            arrWorkphone.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Address" {
            
            arrAddress.remove(at: textField.tag)
            arrAddress.insert(textField.text ?? "", at: textField.tag)
        }

        if textField.accessibilityLabel == "Zip code" {
            
            arrPostcode.remove(at: textField.tag)
            arrPostcode.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "EIN" {
            
            arrEIN.remove(at: textField.tag)
            arrEIN.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "EntityName" {
            
            arrEntityName.remove(at: textField.tag)
            arrEntityName.insert(textField.text ?? "", at: textField.tag)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtEIN || textField.accessibilityLabel == "EIN" {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XX-XXXXXXX", phone: newString)
                return true//false
        }
        if textField.accessibilityLabel == "Zip code" {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXXXX", phone: newString)
                return false
        }
        if textField.accessibilityLabel == "Workphone" {
            
            guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXX-XXX-XXXX", phone: newString)
                return false
        }
        if textField.accessibilityLabel == "State" || textField.accessibilityLabel == "City" {
            return false
        }
        return true
    }
}

extension BusinessDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return intSelectedNoOfLocations ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhysicalLocationCell") as! PhysicalLocationCell
        
        cell.txtEmail.text = arrEmail[indexPath.row]
        cell.txtWorkphone.text = arrWorkphone[indexPath.row]
        cell.txtAddress.text = arrAddress[indexPath.row]
        cell.txtCity.text = arrLocationCity[indexPath.row]
        cell.txtState.text = arrLocationState[indexPath.row]
        cell.txtPostcode.text = arrPostcode[indexPath.row]
        cell.txtEIN.text = arrEIN[indexPath.row]
        cell.txtEntityName.text = arrEntityName[indexPath.row]
        
        cell.lblEmail.text = "\(indexPath.row+1). Email"
        cell.lblWorkphone.text = "\(indexPath.row+1). Work Phone"
        cell.lblAddress.text = "\(indexPath.row+1). Address"
        cell.lblCity.text = "\(indexPath.row+1). City"
        cell.lblState.text = "\(indexPath.row+1). State"
        cell.lblPostcode.text = "\(indexPath.row+1). Zip code"
        cell.lblEIN.text = "\(indexPath.row+1). EIN (Entity Number)"
        cell.lblEntityName.text = "\(indexPath.row+1). Entity Name"
        
        cell.txtEmail.tag = indexPath.row
        cell.txtWorkphone.tag = indexPath.row
        cell.txtAddress.tag = indexPath.row
        cell.txtCity.tag = indexPath.row
        cell.txtState.tag = indexPath.row
        cell.txtPostcode.tag = indexPath.row
        cell.txtEIN.tag = indexPath.row
        cell.txtEntityName.tag = indexPath.row
        
        cell.txtEmail.accessibilityLabel = "Email"
        cell.txtWorkphone.accessibilityLabel = "Workphone"
        cell.txtAddress.accessibilityLabel = "Address"
        cell.txtCity.accessibilityLabel = "City"
        cell.txtState.accessibilityLabel = "State"
        cell.txtPostcode.accessibilityLabel = "Zip code"
        cell.txtEIN.accessibilityLabel = "EIN"
        cell.txtEntityName.accessibilityLabel = "EntityName"
        
        cell.txtEmail.delegate = self
        cell.txtWorkphone.delegate = self
        cell.txtAddress.delegate = self
        cell.txtCity.delegate = self
        cell.txtState.delegate = self
        cell.txtPostcode.delegate = self
        cell.txtEIN.delegate = self
        cell.txtEntityName.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        setTableHeight()
    }
    
    
}

extension BusinessDetailsVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfield?.accessibilityLabel == "State" {
            return arrStates?.count ?? 0
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            return arrCities?.count ?? 0
        }
        return arrTitle.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfield?.accessibilityLabel == "State" {
            return arrStates?[row].state_code
        }
        if tappedTextfield?.accessibilityLabel == "City" {
            return arrCities?[row].city_name
        }
        return arrTitle[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if tappedTextfield?.accessibilityLabel == "State" || tappedTextfield?.accessibilityLabel == "City" {
            
            intSelectedRow = row
        }
        
        if intSelectedTextfieldTag == 101 {
            txtNoOfLocation.text = arrTitle[row]
            tempNoOfLocations = Int(arrTitle[row])!
        }
        if intSelectedTextfieldTag == 102 {
            txtTypeBusiness.text = arrTitle[row]
            strCatId = arrId[row]
        }
        if intSelectedTextfieldTag == 103 {
            txtCost.text = arrTitle[row]
            
        }
        if intSelectedTextfieldTag == 104 {
            txtHowlong.text = arrTitle[row]
            
        }
    }

}


