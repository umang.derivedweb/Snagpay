//
//  ReportVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class ReportVC: UIViewController {
    
    // MARK: - Variable
    var arr = ["Monthly Statements","Summary Report","Sell Report","Buy Report","Fees Report","1099-B Tax report"]
    
    var dictFilter:[String:String]?
    
    var selectedButton:UIButton!
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    
    // MARK: - Function
    
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 80
        } else {
            table.rowHeight = 60
        }
        table.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReportFilter"), object: nil)

    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        
        dictFilter = notification.userInfo as? [String : String]
        
        callRelatedApi()
    }

    func savePdf(urlString:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(Int.getUniqueRandomNumbers(min: 1000, max: 1500, count: 10)).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
                Toast.show(message: Message.pdfSavedSuccess, controller: self)
            } catch {
                print("Pdf could not be saved")
                Toast.show(message: Message.pdfSavedFailure, controller: self)
            }
        }
    }
    
    
    
    // MARK: - Webservice
    func reportEmailAPI(strUrl:String) {
        Helper.shared.showHUD()
        
        print(dictFilter)
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: strUrl, parameters: dictFilter!, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func reportDownloadAPI(strUrl:String, isShow:Bool) {
        Helper.shared.showHUD()
        
        print(dictFilter)
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDownloadReports(url: strUrl, parameters: dictFilter!, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                if isShow {
                    let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                    obj.strUrl = response.data?.pdf ?? ""
                    self.navigationController?.pushViewController(obj, animated: true)
                } else {
                    self.savePdf(urlString: response.data?.pdf ?? "")
                }
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @objc func btnEmailPdf(sender:UIButton) {
        
        loadReportView()
        
        selectedButton = sender
    }
    
    @objc func btnDownloadPdf(sender:UIButton) {
        
        loadReportView()
        
        selectedButton = sender
    }
    
    @objc func btnShowPDF(sender:UIButton) {
        loadReportView()
        
        selectedButton = sender
    }
    
    func loadReportView() {
        
        let customView = Bundle.main.loadNibNamed("ReportFilterView", owner: self, options: nil)?.first as! ReportFilterView
        customView.tag = 2001
        customView.frame = view.frame
        view.addSubview(customView)
    }
    
    func callRelatedApi() {
        
        if selectedButton?.accessibilityLabel == "Email" {
            if selectedButton.tag == 0 {
                reportEmailAPI(strUrl: URLs.send_monthly_statement)
            }
            if selectedButton.tag == 1 {
                reportEmailAPI(strUrl: URLs.send_summary_report)
            }
            if selectedButton.tag == 2 {
                reportEmailAPI(strUrl: URLs.send_sell_report)
            }
            if selectedButton.tag == 3 {
                reportEmailAPI(strUrl: URLs.send_buy_report)
            }
        }
        if selectedButton?.accessibilityLabel == "Download" {
            
            if selectedButton.tag == 0 {
                reportDownloadAPI(strUrl: URLs.monthly_statement, isShow: false)
            }
            if selectedButton.tag == 1 {
                reportDownloadAPI(strUrl: URLs.summary_report, isShow: false)
            }
            if selectedButton.tag == 2 {
                reportDownloadAPI(strUrl: URLs.sell_report, isShow: false)
            }
            if selectedButton.tag == 3 {
                reportDownloadAPI(strUrl: URLs.buy_report, isShow: false)
            }
        }
        if selectedButton?.accessibilityLabel == "Eye" {
            
            if selectedButton.tag == 0 {
                reportDownloadAPI(strUrl: URLs.monthly_statement, isShow: true)
            }
            if selectedButton.tag == 1 {
                reportDownloadAPI(strUrl: URLs.summary_report, isShow: true)
            }
            if selectedButton.tag == 2 {
                reportDownloadAPI(strUrl: URLs.sell_report, isShow: true)
            }
            if selectedButton.tag == 3 {
                reportDownloadAPI(strUrl: URLs.buy_report, isShow: true)
            }
        }
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}
extension ReportVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell") as! ReportCell
        cell.lblTitle.text = arr[indexPath.row]
        cell.btnEmail.accessibilityLabel = "Email"
        cell.btnEmail.tag = indexPath.row
        cell.btnEmail.addTarget(self, action: #selector(btnEmailPdf(sender:)), for: .touchUpInside)
        cell.btnDownload.accessibilityLabel = "Download"
        cell.btnDownload.tag = indexPath.row
        cell.btnDownload.addTarget(self, action: #selector(btnDownloadPdf(sender:)), for: .touchUpInside)
        cell.btnEye.accessibilityLabel = "Eye"
        cell.btnEye.tag = indexPath.row
        cell.btnEye.addTarget(self, action: #selector(btnShowPDF(sender:)), for: .touchUpInside)
        return cell
    }
}
