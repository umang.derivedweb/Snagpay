//
//  DraftDealsVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class DraftDealsVC: UIViewController {
    
    // MARK: - Variable
    var arrDeals:[ArrDealsData] = []
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getDraftDealsAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 80
        } else {
            table.rowHeight = 60
        }
    }
    
    // MARK: - Webservice
    func getDraftDealsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallMyAndDraftDeals(url: "\(URLs.draft_deals)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data?.last_page ?? 0
                self.arrDeals += response.data?.data ?? []
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteDraftAPI(deal_id:String) {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["deal_id"] = deal_id
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.draft_deal_delete, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.intCountArr = 0
                self.arrDeals.removeAll()
                self.currentPage = 1
                self.getDraftDealsAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func draftDealDetailsAPI(dealId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDraftDealDetails(url: URLs.deal_draft_details+dealId, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "UploadDealVC") as! UploadDealVC
                obj.isFromDraftDealVC = true
                obj.strDraft_token = response.data?.deal_details?.draft_token ?? ""
                obj.dictDraftDetails = response.data
                self.navigationController?.pushViewController(obj, animated: true)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNewCampaign(_ sender: Any) {
    }
    @objc func btnDelete(sender:UIButton) {
        
        deleteDraftAPI(deal_id:"\(arrDeals[sender.tag].deal_id ?? 0)")
    }
    
    @objc func btnDealDetails(sender:UIButton) {
        
        draftDealDetailsAPI(dealId: "\(arrDeals[sender.tag].deal_id ?? 0)")
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension DraftDealsVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DraftDealsCell") as! DraftDealsCell
        cell.lblTitle.text = arrDeals[indexPath.row].title
        cell.lblDate.text = arrDeals[indexPath.row].deal_date
        cell.lblStatus.text = arrDeals[indexPath.row].status
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDelete(sender:)), for: .touchUpInside)
        cell.btnReport1.tag = indexPath.row
        cell.btnReport1.addTarget(self, action: #selector(btnDealDetails(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrDeals.count {
            if (indexPath.row == (self.arrDeals.count ) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrDeals.count
                    getDraftDealsAPI()
                }
                
            }
        }
    }
    
}
