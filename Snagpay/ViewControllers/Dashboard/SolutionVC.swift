//
//  SolutionVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit
import AVKit

class SolutionVC: UIViewController {

    // MARK: - Variable
    
    let arr = ["Save Cash Flow","Marketing Tools","Advertise Your Business","Gift It"]
    let arrColour:[UIColor] = [UIColor(named: "App Red")!,UIColor(named: "App Blue")!,UIColor(named: "App Green")!,UIColor(named: "App SkyBlue")!]
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 170
        } else {
            table.rowHeight = 130
        }
    }
    
    func playVideo() {
        
        let videoURL = URL(string: "https://snagpay.com/images/splash/cell_video.mp4")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    // MARK: - Webservice
    
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}
extension SolutionVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SolutionCell") as! SolutionCell
        cell.lblTitle.text = arr[indexPath.row]
        cell.viewBg.backgroundColor = arrColour[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            
            playVideo()
        }
        
    }

}
