//
//  DealAgreementVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class DealAgreementVC: UIViewController {
    
    // MARK: - Variable
    let datePicker = UIDatePicker()
    
    var imgMain:UIImage?
    var arrImages:[UIImage]?
    var params:[String:Any]?
    
    var isCheckbox = false
    
    //get draft
    var isFromDraftDealVC = false
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtFullName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        showDatePicker()
    }
    
    
    // MARK: - Function
    func setupUI() {
        txtview.layer.borderWidth = 1
        txtview.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        if isFromDraftDealVC {
            
            setDraftDetails()
        }
        
        txtview.attributedText = (txtview.text).convertToAttributedString()
    }
    
    func setDraftDetails() {
        
        txtDate.text = dictDraftDetails?.deal_details?.deal_date
    }
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDate.inputAccessoryView = toolbar
        txtDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
        txtDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        if txtDate.text == "" {
            
            Toast.show(message: "Please select date", controller: self)
            return
        }
        
        let date = Helper.shared.changeDateFormat(fromFormat:"MM-dd-yyyy",toFormat:"yyyy-MM-dd", strDate:txtDate.text!)
        
        params?["deal_date"] = date
        params?["full_name"] = txtFullName.text ?? ""
        
        params?.removeValue(forKey: "draft_token")

        
        if !isCheckbox {
            
            Toast.show(message: "Please tick the checkbox", controller: self)
            return
        }
        
        //print(params)
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreviewDealVC") as! PreviewDealVC
        obj.params = params
        obj.arrImages = arrImages ?? []
        obj.imgMain = imgMain
        obj.isFromMyDealVC = isFromMyDealVC
        obj.dictDraftDetails = dictDraftDetails
        navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnCheckbox(_ sender: UIButton) {
        
        if sender.tag == 0 {
            isCheckbox = true
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        } else {
            isCheckbox = false
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    @IBAction func btnPreview(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "PreviewDealVC") as! PreviewDealVC
        obj.params = params
        obj.arrImages = arrImages ?? []
        obj.isFromMyDealVC = isFromMyDealVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}
