//
//  AlphabetsVC.swift
//  Snagpay
//
//  Created by Apple on 15/12/21.
//

import UIKit

class AlphabetsVC: UIViewController {
    
    // MARK: - Variable
    var strCatName:String?
    var strCategoryId:String?
    
    var arrMerchant:[MerchantsAlphabetsData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblHeader: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        merchantsAlphabetsAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        lblHeader.text = strCatName
        table.rowHeight = 84
    }
    
    //MARK: - Webservice
    func merchantsAlphabetsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallMerchantsAlphabets(url: "\(URLs.get_merchant_alphabets)\(strCategoryId!)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrMerchant = response.data
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension AlphabetsVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMerchant?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlphabetsCell") as! AlphabetsCell
        cell.lbLetter.text = arrMerchant?[indexPath.row].alphabet
        cell.lblPurchases.text = "\(arrMerchant?[indexPath.row].cnt ?? 0) merchants"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SearchDirectoryVC") as! SearchDirectoryVC
        obj.strCategoryId = strCategoryId
        obj.strCatName = strCatName
        obj.strSearchBy = arrMerchant?[indexPath.row].alphabet
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
