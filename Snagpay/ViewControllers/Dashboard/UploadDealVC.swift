//
//  UploadDealVC.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit
import BSImagePicker
import Photos

class UploadDealVC: UIViewController {

    // MARK: - Variable
    var arrDeal = [""]
    var arrName = [""]
    var arrPrice = [""]
    var arrNoGifts = [""]
    var arrPostDeal = ["1"]
    var arrMaxGiftCards = [""]
    var arrMaxDays = [""]
    
    var arrImages:[UIImage] = []
    
    
    var arrPopularCategories:[GetCategoryData]?
    var arrAllCategories:[AllCategoriesData]?
    
    var arrSubCategories:[SubCategoriesData]?
    var arrSubSubCategories:[SubCategoriesData]?
    
    var strCatId:String?
    var strSubCatId:String?
    var strSubSubCatId:String?
    
    var tappedTextfield:UITextField!
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var imagePicker = UIImagePickerController()
    
    var picker2  = UIPickerView()
    var intSelectedRow:Int!
    var arrNumbers:[String] = []
    
    //get draft
    var strDraft_token = ""//todo
    var isFromDraftDealVC = false
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnSaveDraft: UIButton!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var heightCollviewPhoto: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var collviewPhotos: UICollectionView!
    @IBOutlet weak var collviewCategory: UICollectionView!
    @IBOutlet weak var txtSubCategory: UITextField!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var heightCollviewCategory: NSLayoutConstraint!
    @IBOutlet weak var lblPhotoCount: UILabel!
    @IBOutlet weak var txtSubSubCategory: UITextField!
    @IBOutlet weak var viewSubSubCategory: UIView!
    @IBOutlet weak var imgMain: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        showPicker()
        getCategoryAPI()
        self.hideKeyboardWhenTappedAround()
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.heightTable?.constant = self.table.contentSize.height
        }
        
    }

    // MARK: - Function
    
    func setupUI() {
        txtCompanyName.text = Helper.shared.strBusinessName
        if !isFromDraftDealVC {
            strDraft_token = randomString(length: 12)
        } else {
            
            setDraftDetails()
        }
        
        if isFromMyDealVC {
            btnSaveDraft.isHidden = true
            setDraftDetails()
        }
        
        collviewPhotos.layer.cornerRadius = 5
        collviewPhotos.layer.borderWidth = 1
        collviewPhotos.layer.borderColor = UIColor.lightGray.cgColor
        imgMain.layer.cornerRadius = 5
        imgMain.layer.borderWidth = 1
        imgMain.layer.borderColor = UIColor.lightGray.cgColor
        
        arrNumbers.removeAll()
        for i in 1...60 {
            arrNumbers.append("\(i)")
        }
        
    }
    
    func setDraftDetails() {
        
        if let _ = dictDraftDetails?.category {
            strCatId = "\(dictDraftDetails?.category?.category_id ?? 0)"
        }
        if let _ = dictDraftDetails?.sub_category {
            strSubCatId = "\(dictDraftDetails?.sub_category?.category_id ?? 0)"
            txtSubCategory.text = "\(dictDraftDetails?.sub_category?.category_name ?? "")"
        }
        if let _ = dictDraftDetails?.sub_sub_category {
            
            viewSubSubCategory.isHidden = false
            strSubSubCatId = "\(dictDraftDetails?.sub_sub_category?.category_id ?? 0)"
            txtSubSubCategory.text = "\(dictDraftDetails?.sub_sub_category?.category_name ?? "")"
        }
        
        if let _ = dictDraftDetails?.deal_details?.company_name {
            
            txtCompanyName.text = dictDraftDetails?.deal_details?.company_name
        }
        
        for i in 0..<(dictDraftDetails?.deal_options?.count ?? 0) {
            
            if i == 0 {
                
                arrDeal.remove(at: i)
                arrDeal.insert("", at: i)
                arrName.remove(at: i)
                arrName.insert(dictDraftDetails?.deal_options?[i].deal_option_name ?? "", at: i)
                arrPrice.remove(at: i)
                arrPrice.insert("\(dictDraftDetails?.deal_options?[i].sell_price ?? 0)", at: i)
                arrNoGifts.remove(at: i)
                arrNoGifts.insert("\(dictDraftDetails?.deal_options?[i].no_of_egifts_available ?? 0)", at: i)
                arrPostDeal.remove(at: i)
                arrPostDeal.insert("\(dictDraftDetails?.deal_options?[i].post_deal_for_a ?? 1)", at: i)
                arrMaxGiftCards.remove(at: i)
                arrMaxGiftCards.insert("\(dictDraftDetails?.deal_options?[i].maximum_e_gift_cards_purchased ?? 0)", at: i)
                arrMaxDays.remove(at: i)
                arrMaxDays.insert("\(dictDraftDetails?.deal_options?[i].maximum_days_to_run_deal ?? 0)", at: i)
                
            } else {
                arrDeal.append("")
                arrName.append(dictDraftDetails?.deal_options?[i].deal_option_name ?? "")
                arrPrice.append("\(dictDraftDetails?.deal_options?[i].sell_price ?? 0)")
                arrNoGifts.append("\(dictDraftDetails?.deal_options?[i].no_of_egifts_available ?? 0)")
                arrPostDeal.append("\(dictDraftDetails?.deal_options?[i].post_deal_for_a ?? 1)")
                arrMaxGiftCards.append("\(dictDraftDetails?.deal_options?[i].maximum_e_gift_cards_purchased ?? 0)")
                arrMaxDays.append("\(dictDraftDetails?.deal_options?[i].maximum_days_to_run_deal ?? 0)")
            }
            
        }
        
        table.reloadData()
        
        DispatchQueue.main.async {
            
            self.lblPhotoCount.text = "Photos : \((self.dictDraftDetails?.deal_images?.count ?? 0))/6"
            
            for i in 0..<(self.dictDraftDetails?.deal_images?.count ?? 0){
                
                let url = URL(string:self.dictDraftDetails?.deal_images?[i].image_name ?? "")
                   if let data = try? Data(contentsOf: url!)
                   {
                    let image: UIImage = UIImage(data: data)!
                    self.arrImages.append(image)
                   }
                
            }
            self.collviewPhotos.reloadData()
            
            if let _ = self.dictDraftDetails?.deal_details?.deal_image {
                
                let url = URL(string:self.dictDraftDetails?.deal_details?.deal_image ?? "")
                if let data = try? Data(contentsOf: url!)
                {
                    let image: UIImage = UIImage(data: data)!
                    self.imgMain.image = image
                    
                }
            }
        }
    }
    
    func showPicker() {
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 200, width: UIScreen.main.bounds.size.width, height: 200)
        
        txtSubCategory.inputView = picker
        txtSubSubCategory.inputView = picker
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        txtSubCategory.inputAccessoryView = toolBar
        txtSubSubCategory.inputAccessoryView = toolBar
    }
    
    
    @objc func onDoneButtonTapped() {
        view.endEditing(true)
        
    }
    @objc func onCancelButtonTapped() {
        view.endEditing(true)
    }
    @objc func btnDeletePhoto(sender:UIButton) {
        
        arrImages.remove(at: sender.tag)
        collviewPhotos.reloadData()
    }
    
    @objc func btnRemoveDeal(sender:UIButton) {
        
        if arrDeal.count == 1 {
            return
        }
        arrDeal.remove(at: sender.tag)
        arrName.remove(at: sender.tag)
        arrPrice.remove(at: sender.tag)
        arrNoGifts.remove(at: sender.tag)
        arrPostDeal.remove(at: sender.tag)
        arrMaxGiftCards.remove(at: sender.tag)
        arrMaxDays.remove(at: sender.tag)
        table.reloadData()
        
        btnAddMore.isHidden = false
    }
    
    @objc func btnMaxDays(sender:UIButton) {
        arrMaxDays.remove(at: sender.tag)
        arrMaxDays.insert("", at: sender.tag)
        arrMaxGiftCards.remove(at: sender.tag)
        arrMaxGiftCards.insert("", at: sender.tag)
        arrPostDeal.remove(at: sender.tag)
        arrPostDeal.insert("1", at: sender.tag)
        table.reloadData()
    }
    @objc func btngiftsPurchased(sender:UIButton) {
        arrMaxDays.remove(at: sender.tag)
        arrMaxDays.insert("", at: sender.tag)
        arrMaxGiftCards.remove(at: sender.tag)
        arrMaxGiftCards.insert("", at: sender.tag)
        arrPostDeal.remove(at: sender.tag)
        arrPostDeal.insert("2", at: sender.tag)
        table.reloadData()
    }
    
    func toolBar(textField:UITextField) {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        if tappedTextfield?.accessibilityLabel == "MaxGifts" {
            
            if arrPostDeal[tappedTextfield!.tag] == "1" {
                arrMaxDays.remove(at: tappedTextfield!.tag)
                arrMaxDays.insert(arrNumbers[intSelectedRow], at: tappedTextfield!.tag)
            } else if arrPostDeal[tappedTextfield!.tag] == "2" {
                arrMaxGiftCards.remove(at: tappedTextfield!.tag)
                arrMaxGiftCards.insert(arrNumbers[intSelectedRow], at: tappedTextfield!.tag)
            }
            
            
        }
        
        table.reloadData()
        view.endEditing(true)
        
    }
    @objc func cancelClick() {
        view.endEditing(true)
    }
    
    
    
    func randomString(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }
    
    func setCollectionHeight() {
        
        DispatchQueue.main.async {
            let height = self.collviewCategory.collectionViewLayout.collectionViewContentSize.height
            self.heightCollviewCategory.constant = height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
        }
        
    }
    
    func openImagePicker() {
        
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 6
        imagePicker.settings.theme.selectionStyle = .numbered
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.settings.selection.unselectOnReachingMax = false
        
        
        
        let start = Date()
        self.presentImagePicker(imagePicker, select: { (asset) in
            print("Selected: \(asset)")
        }, deselect: { (asset) in
            print("Deselected: \(asset)")
        }, cancel: { (assets) in
            print("Canceled with selections: \(assets)")
        }, finish: { (assets) in
            print("Finished with selections: \(assets)")
            self.arrImages.removeAll()
            for i in 0..<assets.count {
                
                let option = PHImageRequestOptions()
                option.deliveryMode = .highQualityFormat
                option.resizeMode = .exact
                
                PHImageManager.default().requestImage(for: assets[i], targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option) { (image, info) in
                    // Do something with image
                    self.arrImages.append(image!)
                    self.collviewPhotos.reloadData()
                }
                
            }
            
        }, completion: {
            let finish = Date()
            print(finish.timeIntervalSince(start))
        })
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Webservice
    func getCategoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetOnlyCategory(url: "\(URLs.categories)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrPopularCategories = response.data?.popular_categories
                self.arrAllCategories = response.data?.all_categories
                self.collviewCategory.reloadData()
                self.setCollectionHeight()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func saveDraftAPI(params:[String:Any]) {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        for i in 0..<arrImages.count {
            
            imagesData.append((arrImages[i].jpegData(compressionQuality: 0.5)!))
        }
        
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain.image != nil {
            
            mainImageData.append((imgMain.image?.jpegData(compressionQuality: 0.5))!)
        }
             
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.save_draft, mainImageData: mainImageData, imagesData: imagesData, parameters: params as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    // MARK: - IBAction
    @IBAction func btnAddMore(_ sender: Any) {
        
        if arrDeal.count < 3 {
            arrDeal.append("")
            arrName.append("")
            arrPrice.append("")
            arrNoGifts.append("")
            arrPostDeal.append("1")
            arrMaxGiftCards.append("")
            arrMaxDays.append("")
            table.reloadData()
        }
        
        if arrDeal.count == 3 {
            btnAddMore.isHidden = true
        }
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if imgMain.image == nil {
            
            Toast.show(message: "Please select Main Photo", controller: self)
            return
        }
//        if arrImages.count == 0 {
//            Toast.show(message: "Please select Photos", controller: self)
//            return
//        }
        if txtSubCategory.text == "" {
            
            Toast.show(message: "Please select Sub Category", controller: self)
            return
        }
//        if txtSubSubCategory.text == "" {
//
//            Toast.show(message: "Please select Sub Category", controller: self)
//            return
//        }
//        if let index = arrName.firstIndex(of: "") {
//            
//            Toast.show(message: "Please enter Name of Deal in Deal \(index+1)", controller: self)
//            return
//        }
        if txtCompanyName.text == "" {
            
            Toast.show(message: "Please enter company name", controller: self)
            return
        }
        if let index = arrPrice.firstIndex(of: "") {
            
            Toast.show(message: "Please enter Price in Deal \(index+1)", controller: self)
            return
        }
        if let index = arrNoGifts.firstIndex(of: "") {
            
            Toast.show(message: "Please enter Number of eGifts available in Deal \(index+1)", controller: self)
            return
        }
        
        for i in 0..<arrPostDeal.count {
            
            if arrPostDeal[i] == "1" {
                
                if arrMaxDays[i] == "" {
                    Toast.show(message: "Please enter Maximum days to run in Deal \(i+1)", controller: self)
                    return
                }
            }
            if arrPostDeal[i] == "2" {
                
                if arrMaxGiftCards[i] == "" {
                    Toast.show(message: "Please enter Maximum e-gift cards purchased in Deal \(i+1)", controller: self)
                    return
                }
            }
        }
        
        
        //Params
        var params:[String:Any] = [:]
        
        params["main_category_id"] = strCatId
        params["sub_category_id"] = strSubCatId
        params["sub_sub_category_id"] = strSubSubCatId ?? ""
        params["company_name"] = txtCompanyName.text
        
        for i in 0..<arrName.count {
            
            if isFromMyDealVC {
                params["deal_option_id[\(i+1)]"] = "\(dictDraftDetails?.deal_options?[i].deal_option_id ?? 0)"
            }
            
            params["deal_option_name[\(i+1)]"] = arrName[i]
            
            params["regular_price[\(i+1)]"] = arrPrice[i]
            params["no_of_egifts_available[\(i+1)]"] = arrNoGifts[i]
            params["post_deal_for_a[\(i+1)]"] = arrPostDeal[i]
            params["maximum_e_gift_cards_purchased[\(i+1)]"] = arrMaxGiftCards[i]
            params["maximum_days_to_run_deal[\(i+1)]"] = arrMaxDays[i]
        }
        
        //This array is to show preview deal name on PreviewDealVCdeal_option_name
        Helper.shared.arrNamePreview = arrName
        Helper.shared.arrPricePreview = arrPrice
        //print(params)
        let obj = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "DealHighlightVC") as! DealHighlightVC
        obj.arrDeal = arrDeal
        obj.arrImages = arrImages
        obj.imgMain = imgMain.image
        obj.params = params
        obj.strDraft_token = strDraft_token//todo
        obj.isFromDraftDealVC = isFromDraftDealVC
        obj.isFromMyDealVC = isFromMyDealVC
        obj.dictDraftDetails = dictDraftDetails
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSaveDrafts(_ sender: Any) {
        
        //Params
        var params:[String:Any] = [:]
        
        params["draft_token"] = strDraft_token//todo
        
        params["main_category_id"] = strCatId
        params["sub_category_id"] = strSubCatId
        params["sub_sub_category_id"] = strSubSubCatId ?? ""
        params["company_name"] = txtCompanyName.text
        
        for i in 0..<arrName.count {
            
            params["deal_option_name[\(i+1)]"] = arrName[i]
            params["regular_price[\(i+1)]"] = arrPrice[i]
            params["no_of_egifts_available[\(i+1)]"] = arrNoGifts[i]
            params["post_deal_for_a[\(i+1)]"] = arrPostDeal[i]
            params["maximum_e_gift_cards_purchased[\(i+1)]"] = arrMaxGiftCards[i]
            params["maximum_days_to_run_deal[\(i+1)]"] = arrMaxDays[i]
        }
        
        saveDraftAPI(params:params)
    }
    @IBAction func btnAddPhotos(_ sender: Any) {
        
        openImagePicker()
    }
    
    @IBAction func btnAddMainPhoto(_ sender: Any) {
        
        openGallary()
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    
}
extension UploadDealVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UploadDealCell") as! UploadDealCell
        cell.lblDeal.text = "Deal \(indexPath.row+1)"
        cell.txtDealName.text = arrName[indexPath.row]
        cell.txtPrice.text = arrPrice[indexPath.row]
        cell.txtEgiftsAvailable.text = arrNoGifts[indexPath.row]
        
        
        
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveDeal(sender:)), for: .touchUpInside)
        
        if arrPostDeal[indexPath.row] == "1" {
            cell.txtMaxGifts.text = arrMaxDays[indexPath.row]
            cell.btnMaxDays.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            cell.btnEgiftsPurchased.setImage(#imageLiteral(resourceName: "empty_radio"), for: .normal)
            cell.txtMaxGifts.placeholder = "Maximum days to run that deal"
            //cell.contentView.viewWithTag(1111)?.isHidden = true
        } else {
            cell.txtMaxGifts.text = arrMaxGiftCards[indexPath.row]
            cell.btnEgiftsPurchased.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            cell.btnMaxDays.setImage(#imageLiteral(resourceName: "empty_radio"), for: .normal)
            cell.txtMaxGifts.placeholder = "How many e-Gift cards"
            //cell.contentView.viewWithTag(1111)?.isHidden = false
        }
        
        cell.txtDealName.tag = indexPath.row
        cell.txtPrice.tag = indexPath.row
        cell.txtEgiftsAvailable.tag = indexPath.row
        cell.txtMaxGifts.tag = indexPath.row
        
        cell.txtDealName.accessibilityLabel = "Name"
        cell.txtPrice.accessibilityLabel = "Price"
        cell.txtEgiftsAvailable.accessibilityLabel = "Gifts"
        cell.txtMaxGifts.accessibilityLabel = "MaxGifts"
        
        cell.txtDealName.delegate = self
        cell.txtPrice.delegate = self
        cell.txtEgiftsAvailable.delegate = self
        cell.txtMaxGifts.delegate = self
        
        cell.btnMaxDays.tag = indexPath.row
        cell.btnEgiftsPurchased.tag = indexPath.row
        
        cell.btnMaxDays.accessibilityLabel = "PostDeal1"
        cell.btnEgiftsPurchased.accessibilityLabel = "PostDeal2"
        
        cell.btnMaxDays.addTarget(self, action: #selector(btnMaxDays(sender:)), for: .touchUpInside)
        cell.btnEgiftsPurchased.addTarget(self, action: #selector(btngiftsPurchased(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    
}
extension UploadDealVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collviewPhotos {
            
            lblPhotoCount.text = "Photos : \(arrImages.count)/6"
            if arrImages.count != 6 {
                return arrImages.count+1
            }
            return arrImages.count
        } else {
            return arrPopularCategories?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collviewPhotos {
            
            if arrImages.count != 6 {
                if indexPath.row == arrImages.count {
                    
                    let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCell", for: indexPath) as! EmptyCell
                    return cell
                } else {
                    let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
                    cell.img.image = arrImages[indexPath.row]
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDelete.addTarget(self, action: #selector(btnDeletePhoto(sender:)), for: .touchUpInside)
                    return cell
                }
            } else {
                
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
                cell.img.image = arrImages[indexPath.row]
                cell.btnDelete.tag = indexPath.row
                cell.btnDelete.addTarget(self, action: #selector(btnDeletePhoto(sender:)), for: .touchUpInside)
                return cell
            }

        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessCategoryCell", for: indexPath) as! BusinessCategoryCell
            cell.lblTitle.text = arrPopularCategories?[indexPath.row].category_name
            cell.viewBg.backgroundColor = Helper.shared.hexStringToUIColor(hex: arrPopularCategories?[indexPath.row].backround_color ?? "")
            
            if "\(arrPopularCategories?[indexPath.row].category_id ?? 0)" == strCatId {
                
                cell.viewBg.layer.borderColor = UIColor.black.cgColor
                cell.viewBg.layer.borderWidth = 3
                arrSubCategories = arrAllCategories?[indexPath.row].sub_categories
            } else {
                
                cell.viewBg.layer.borderColor = UIColor.clear.cgColor
                cell.viewBg.layer.borderWidth = 3
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collviewPhotos {
            
            return CGSize(width: (Int(collectionView.frame.width)/3), height: Int(collectionView.frame.height)/2)
        } else {
            
            return CGSize(width: (collectionView.frame.width/2)-5, height: (collectionView.frame.height/3)-5)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collviewPhotos {
            if indexPath.row == arrImages.count {
                openImagePicker()
            }
            return
        }
        
        strCatId = "\(arrPopularCategories?[indexPath.row].category_id ?? 0)"
        
        txtSubCategory.text = ""
        txtSubSubCategory.text = ""
        strSubCatId = ""
        strSubSubCatId = ""
        viewSubSubCategory.isHidden = true
        arrSubCategories = arrAllCategories?[indexPath.row].sub_categories
        
        if let cell = collviewCategory.cellForItem(at: indexPath) as? BusinessCategoryCell {
            cell.viewBg.layer.borderColor = UIColor.black.cgColor
            cell.viewBg.layer.borderWidth = 3
        }
        collviewCategory.reloadData()
        //setCollectionHeight()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell = collviewCategory.cellForItem(at: indexPath) as? BusinessCategoryCell {
            cell.viewBg.layer.borderColor = UIColor.clear.cgColor
            cell.viewBg.layer.borderWidth = 3
        }
    }
    
}
extension UploadDealVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfield.tag == 101 {
            return arrSubCategories?.count ?? 0
        }
        if tappedTextfield.tag == 102 {
            return arrSubSubCategories?.count ?? 0
        }
        if tappedTextfield.accessibilityLabel == "MaxGifts" {
            return arrNumbers.count
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfield.tag == 101 {
            return arrSubCategories?[row].category_name
        }
        if tappedTextfield.tag == 102 {
            return arrSubSubCategories?[row].category_name
        }
        if tappedTextfield.accessibilityLabel == "MaxGifts" {
            return arrNumbers[row]
        }
        return ""
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if tappedTextfield.tag == 101 {
            txtSubCategory.text = arrSubCategories?[row].category_name
            strSubCatId = "\(arrSubCategories?[row].category_id ?? 0)"
            
            if arrSubCategories?[row].sub_sub_categories?.count != 0 {
                
                viewSubSubCategory.isHidden = false
                txtSubSubCategory.text = ""
                arrSubSubCategories = arrSubCategories?[row].sub_sub_categories
            } else {
                viewSubSubCategory.isHidden = true
                arrSubSubCategories?.removeAll()
            }
        }
        if tappedTextfield.tag == 102 {
            txtSubSubCategory.text = arrSubSubCategories?[row].category_name
            strSubSubCatId = "\(arrSubSubCategories?[row].category_id ?? 0)"
        }
        if tappedTextfield.accessibilityLabel == "MaxGifts" {
            
            intSelectedRow = row
        }
        
    }

}

extension UploadDealVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 101 || textField.tag == 102 {
            
            tappedTextfield = textField
            picker.reloadAllComponents()
        }
        if textField.accessibilityLabel == "MaxGifts" {
            tappedTextfield = textField
            intSelectedRow = nil
            
            picker2 = Helper.shared.myPickerView(vc: self, txtfield: textField)
            picker2.reloadAllComponents()
            toolBar(textField: textField)
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("END EDITING CALLED")
        if textField.tag == 101 || textField.tag == 102 {
            
            return
        }
        
        if textField.accessibilityLabel == "Name" {
            
            arrName.remove(at: textField.tag)
            arrName.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Price" {
            
            arrPrice.remove(at: textField.tag)
            arrPrice.insert(textField.text ?? "", at: textField.tag)
        }
        if textField.accessibilityLabel == "Gifts" {
            
            arrNoGifts.remove(at: textField.tag)
            arrNoGifts.insert(textField.text ?? "", at: textField.tag)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 101 || textField.tag == 102 || textField.accessibilityLabel == "MaxGifts" {
            
            return false
            
        }
        return true
    }
}
extension UploadDealVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            
            imgMain.image = pickedImage//pickedImage.jpegData(compressionQuality: 0.8)!
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
