//
//  DashboardNotiVC.swift
//  Snagpay
//
//  Created by Apple on 23/09/21.
//

import UIKit

class DashboardNotiVC: UIViewController {
    
    // MARK: - Variable
    var arrNotifications:[NotificationData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewTabbar: UIView!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getNotificationAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        setupTabbar()
    }
    func setupTabbar() {
        
        let customView = Bundle.main.loadNibNamed("CustomTabbar", owner: self, options: nil)?.first as? CustomTabbar
        
        customView?.imgNotification.image = #imageLiteral(resourceName: "notification_active")
        customView?.lblNotification.textColor = UIColor(named: "App Blue")
        customView?.btnDashboard.addTarget(self, action: #selector(btnDashboard), for: .touchUpInside)
        customView?.btnNotification.addTarget(self, action: #selector(btnNotification), for: .touchUpInside)
        customView?.btnMyProfile.addTarget(self, action: #selector(btnMyProfile), for: .touchUpInside)
        customView?.frame = self.viewTabbar.bounds
        self.viewTabbar.addSubview(customView!)
    }
    
    @objc func btnDashboard() {
        
        performSegue(withIdentifier: "segueDashboard", sender: self)
    }
    @objc func btnMyProfile() {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        navigationController?.pushViewController(obj, animated: false)
    }
    @objc func btnNotification() {
        
        
    }
    // MARK: - Webservice
    func getNotificationAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetGlobalNotifications(url: URLs.get_global_notifications, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrNotifications = response.data?.data
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

   
}
extension DashboardNotiVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.lblTitle.text  = arrNotifications?[indexPath.row].title
        cell.lblTime.text  = arrNotifications?[indexPath.row].date
        cell.lblDesc.text  = arrNotifications?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrNotifications?[indexPath.row].image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        return cell
    }
}
