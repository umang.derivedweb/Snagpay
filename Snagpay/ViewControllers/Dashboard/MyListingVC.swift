//
//  MyListingVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class MyListingVC: UIViewController {
    
    // MARK: - Variable
    let arr = ["My Deals","Draft Deals"]
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 260
        } else {
            table.rowHeight = 220
        }
    }
    
    // MARK: - Webservice
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewListing(_ sender: Any) {
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension MyListingVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyListingCell") as! MyListingCell
        cell.lblTitle.text = arr[indexPath.row]
        
        if indexPath.row == 0 {
            cell.imgBg.image = #imageLiteral(resourceName: "green_background")
        } else {
            cell.imgBg.image = #imageLiteral(resourceName: "dark_green_background")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyDealVC") as! MyDealVC
            navigationController?.pushViewController(obj, animated: true)
        }
        if indexPath.row == 1 {
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DraftDealsVC") as! DraftDealsVC
            navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    
}

