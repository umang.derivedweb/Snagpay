//
//  DealHighlightVC.swift
//  Snagpay
//
//  Created by Apple on 20/09/21.
//

import UIKit

class DealHighlightVC: UIViewController {
    
    // MARK: - Variable
    var arrDealDetails:[String] = []
    var arrDeal:[String]!
    var arrCharCount:[Int] = []
    
    var imgMain:UIImage?
    var arrImages:[UIImage]?
    var params:[String:Any]?
    
    //get draft
    var strDraft_token = ""
    var isFromDraftDealVC = false
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtWebsite: UITextField!
    @IBOutlet weak var btnSaveDraft: UIButton!
    @IBOutlet weak var lblDescribeDeal: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txtDealTitle: UITextField!
    @IBOutlet weak var lblCharCount: UILabel!
    @IBOutlet weak var lblCharCount2: UILabel!
    @IBOutlet weak var lblCharCount3: UILabel!
    @IBOutlet weak var txtHighlight: UITextField!
    @IBOutlet weak var txtHighlight2: UITextField!
    @IBOutlet weak var txtHighlight3: UITextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        self.hideKeyboardWhenTappedAround()
    }
    

    // MARK: - Function
    func setupUI() {
//        table.rowHeight = UITableView.automaticDimension
//        table.estimatedRowHeight = 100
        table.rowHeight = 0
        
        txtviewDescription.layer.borderWidth = 1
        txtviewDescription.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        for _ in 0..<arrDeal.count {
            
            arrCharCount.append(600)
            arrDealDetails.append("")
        }
        table.reloadData()
        
        if isFromDraftDealVC {
            
            setDraftDetails()
        }
        
        if isFromMyDealVC {
            btnSaveDraft.isHidden = true
            setDraftDetails()
        }
    }
    func setDraftDetails() {
        
        txtDealTitle.text = dictDraftDetails?.deal_details?.title
        txtHighlight.text = dictDraftDetails?.deal_details?.description
        txtWebsite.text = dictDraftDetails?.deal_details?.website
        txtHighlight2.text = dictDraftDetails?.deal_details?.description_2
        txtHighlight3.text = dictDraftDetails?.deal_details?.description_3
        txtviewDescription.text = dictDraftDetails?.deal_details?.deal_description
        
        lblCharCount.text = "\(140-(dictDraftDetails?.deal_details?.description ?? "").count) characters remaining"
        lblCharCount2.text = "\(140-(dictDraftDetails?.deal_details?.description_2 ?? "").count) characters remaining"
        lblCharCount3.text = "\(140-(dictDraftDetails?.deal_details?.description_3 ?? "").count) characters remaining"
        
        for i in 0..<(dictDraftDetails?.deal_options?.count ?? 0) {
            
            arrCharCount.remove(at: i)
            arrCharCount.insert(600-(dictDraftDetails?.deal_options?[i].deal_option_description ?? "").count, at: i)
            arrDealDetails.remove(at: i)
            arrDealDetails.insert(dictDraftDetails?.deal_options?[i].deal_option_description ?? "", at: i)
            
        }
        table.reloadData()
    }
    
    // MARK: - Webservice
    func saveDraftAPI(params:[String:Any]) {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        for i in 0..<(arrImages?.count ?? 0) {
            
            imagesData.append((arrImages?[i].jpegData(compressionQuality: 0.5)!)!)
        }
        
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain != nil {
            
            mainImageData.append((imgMain?.jpegData(compressionQuality: 0.5))!)
        }
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.save_draft, mainImageData: mainImageData, imagesData: imagesData, parameters: params as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveDraft(_ sender: Any) {
        
        //Params
        
        params?["draft_token"] = strDraft_token//todo
        params?["title"] = txtDealTitle.text
        params?["description"] = txtHighlight.text
        params?["website"] = txtWebsite.text
        params?["description_2"] = txtHighlight2.text
        params?["description_3"] = txtHighlight3.text
        params?["deal_description"] = txtviewDescription.text
        
        for i in 0..<arrDealDetails.count {
            
            params?["deal_option_description[\(i+1)]"] = arrDealDetails[i]
            
        }
        
        saveDraftAPI(params: params ?? [:])
    }
    @IBAction func btnNext(_ sender: Any) {
        
//        if let index = arrDealDetails.firstIndex(of: "") {
//            
//            Toast.show(message: "Please enter Details in Deal \(index+1)", controller: self)
//            return
//        }
        if txtDealTitle.text == "" {
            
            Toast.show(message: "Please enter Deal Title", controller: self)
            return
        }
        if txtHighlight.text == "" {
            
            Toast.show(message: "Please enter Deal Highlights", controller: self)
            return
        }
        if txtviewDescription.text == "" {
            
            Toast.show(message: "Please enter Deal Description", controller: self)
            return
        }
        if txtWebsite.text == "" {
            
            Toast.show(message: "Please enter website", controller: self)
            return
        }
        
        //Params
        params?["title"] = txtDealTitle.text
        params?["description"] = txtHighlight.text
        params?["website"] = txtWebsite.text
        params?["description_2"] = txtHighlight2.text
        params?["description_3"] = txtHighlight3.text
        params?["deal_description"] = txtviewDescription.text
        for i in 0..<arrDealDetails.count {
            
            params?["deal_option_description[\(i+1)]"] = arrDealDetails[i]
            
        }
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TermsLimitsDealVC") as! TermsLimitsDealVC
        obj.arrImages = arrImages
        obj.imgMain = imgMain
        obj.params = params
        obj.strDraft_token = strDraft_token//todo
        obj.isFromDraftDealVC = isFromDraftDealVC
        obj.isFromMyDealVC = isFromMyDealVC
        obj.dictDraftDetails = dictDraftDetails
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    
}
extension DealHighlightVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealHighlightCell") as! DealHighlightCell
        
        cell.lblDealName.text = "Description - Deal: \(indexPath.row+1)"
        cell.txtview.tag = indexPath.row
        cell.txtview.delegate = self
        cell.txtview.text = arrDealDetails[indexPath.row]
        cell.lblCharCount.text = "\(arrCharCount[indexPath.row]) characters remaining"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
}

extension DealHighlightVC:UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        print("END EDITING CALLED")
        if textView == txtviewDescription {
            
            return
        }
        
            
        arrDealDetails.remove(at: textView.tag)
        arrDealDetails.insert(textView.text ?? "", at: textView.tag)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        let newLength = textView.text.utf16.count + text.utf16.count - range.length

        if 600-newLength < 0 {
            return false
        }
        arrCharCount.remove(at: textView.tag)
        arrCharCount.insert(600-newLength, at: textView.tag)
        
        let indexPath = IndexPath(row: textView.tag, section: 0)
         if let cell = table.cellForRow(at: indexPath) as? DealHighlightCell {
            cell.lblCharCount.text = "\(arrCharCount[indexPath.row]) characters remaining"
        }
        return true
    }
}
extension DealHighlightVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtDealTitle {
            
            return false
        }
        
        let newLength = (textField.text?.utf16.count ?? 0) + string.utf16.count - range.length

        if 140-newLength < 0 {
            return false
        }
        
        if textField == txtHighlight {
            
            lblCharCount.text = "\(140-newLength) characters remaining"
        }
        if textField == txtHighlight2 {
            
            lblCharCount2.text = "\(140-newLength) characters remaining"
        }
        if textField == txtHighlight3 {
            
            lblCharCount3.text = "\(140-newLength) characters remaining"
        }
        return true
    }
}
