//
//  TermsLimitsDealVC.swift
//  Snagpay
//
//  Created by Apple on 20/09/21.
//

import UIKit

class TermsLimitsDealVC: UIViewController {
    
    // MARK: - Variable
    let arrNumber1 = ["1","2","3","4","5","6","7","8","9","10"]
    let arrNumber2 = ["0","1","2","3","4","5","6","7","8","9","10"]
    let arrNumber3 = ["1","2","3","4","5","6","7","8","9","10"]
    let arrNumber4 = ["6","7","8","9","10","11","12"]
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var intBtnTag:Int?
    
    var imgMain:UIImage?
    var arrImages:[UIImage]?
    var params:[String:Any]?
    
    //get draft
    var strDraft_token = ""
    var isFromDraftDealVC = false
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnSaveDraft: UIButton!
    @IBOutlet weak var btnFirst: UIButton!
    @IBOutlet weak var btnSecond: UIButton!
    @IBOutlet weak var btnThird: UIButton!
    @IBOutlet weak var btnFourth: UIButton!
    @IBOutlet weak var btnFirstCheckbox: UIButton!
    @IBOutlet weak var btnSecondCheckbox: UIButton!
    @IBOutlet weak var btnThirdCheckbox: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
    }
    

    // MARK: - Function
    func setupUI() {
        if isFromDraftDealVC {
            
            setDraftDetails()
        }
        if isFromMyDealVC {
            btnSaveDraft.isHidden = true
            setDraftDetails()
        }
    }
    
    func setDraftDetails() {
        
        btnFirst.setTitle("\(dictDraftDetails?.deal_details?.limit_per_person ?? 1)", for: .normal)
        btnSecond.setTitle("\(dictDraftDetails?.deal_details?.buy_additional_as_gift ?? 1)", for: .normal)
        btnThird.setTitle("\(dictDraftDetails?.deal_details?.repurchased_every_days ?? 1)", for: .normal)
        btnFourth.setTitle("\(dictDraftDetails?.deal_details?.expires_months_from_date_offer_begins ?? 1)", for: .normal)
    }
    func showPicker() {
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 200, width: UIScreen.main.bounds.size.width, height: 200)
        
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        
        let doneButton = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onCancelButtonTapped))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        self.view.addSubview(toolBar)
    }
    
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
    }
    @objc func onCancelButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    // MARK: - Webservice
    func saveDraftAPI(params:[String:Any]) {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        for i in 0..<(arrImages?.count ?? 0) {
            
            imagesData.append((arrImages?[i].jpegData(compressionQuality: 0.5)!)!)
        }
        
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain != nil {
            
            mainImageData.append((imgMain?.jpegData(compressionQuality: 0.5))!)
        }
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.save_draft, mainImageData: mainImageData, imagesData: imagesData, parameters: params as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if btnFirstCheckbox.tag == 0 || btnSecondCheckbox.tag == 0 || btnThirdCheckbox.tag == 0 {
            
            Toast.show(message: "Please select checkbox", controller: self)
            return
        }
        
        params?["limit_per_person"] = btnFirst.titleLabel?.text
        params?["buy_additional_as_gift"] = btnSecond.titleLabel?.text
        params?["repurchased_every_days"] = btnThird.titleLabel?.text
        params?["expires_months_from_date_offer_begins"] = btnFourth.titleLabel?.text
        
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealLocationVC") as! DealLocationVC
        obj.arrImages = arrImages
        obj.imgMain = imgMain
        obj.params = params
        obj.strDraft_token = strDraft_token//todo
        obj.isFromDraftDealVC = isFromDraftDealVC
        obj.isFromMyDealVC = isFromMyDealVC
        obj.dictDraftDetails = dictDraftDetails
        navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnDropdown(_ sender: UIButton) {
        intBtnTag = sender.tag
        showPicker()
    }
    
    @IBAction func btnSaveDrafts(_ sender: Any) {
        
        params?["draft_token"] = strDraft_token//todo
        params?["limit_per_person"] = btnFirst.titleLabel?.text
        params?["buy_additional_as_gift"] = btnSecond.titleLabel?.text
        params?["repurchased_every_days"] = btnThird.titleLabel?.text
        params?["expires_months_from_date_offer_begins"] = btnFourth.titleLabel?.text
        saveDraftAPI(params: params ?? [:])
    }
    
    @IBAction func btnCheckbox(_ sender: UIButton) {
        
        if sender == btnFirstCheckbox {
            
            if sender.tag == 0 {
                sender.tag = 101
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            } else {
                sender.tag = 0
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
        if sender == btnSecondCheckbox {
            
            if sender.tag == 0 {
                sender.tag = 201
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            } else {
                sender.tag = 0
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
        if sender == btnThirdCheckbox {
            
            if sender.tag == 0 {
                sender.tag = 301
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            } else {
                sender.tag = 0
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            }
        }
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}

extension TermsLimitsDealVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if intBtnTag == 1 {
            return arrNumber1.count
        }
        if intBtnTag == 2 {
            return arrNumber2.count
        }
        if intBtnTag == 3 {
            return arrNumber3.count
        }
        if intBtnTag == 4 {
            return arrNumber4.count
        }
        return 0
        
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if intBtnTag == 1 {
            return arrNumber1[row]
        }
        if intBtnTag == 2 {
            return arrNumber2[row]
        }
        if intBtnTag == 3 {
            return arrNumber3[row]
        }
        if intBtnTag == 4 {
            return arrNumber4[row]
        }
        return ""
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if intBtnTag == 1 {
            btnFirst.setTitle(arrNumber1[row], for: .normal)
        }
        if intBtnTag == 2 {
            btnSecond.setTitle(arrNumber2[row], for: .normal)
        }
        if intBtnTag == 3 {
            btnThird.setTitle(arrNumber3[row], for: .normal)
        }
        if intBtnTag == 4 {
            btnFourth.setTitle(arrNumber4[row], for: .normal)
        }
    }

}
