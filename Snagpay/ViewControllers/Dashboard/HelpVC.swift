//
//  HelpVC.swift
//  Snagpay
//
//  Created by Apple on 21/09/21.
//

import UIKit

class HelpVC: UIViewController {
    
    // MARK: - Variable
    
    // MARK: - IBOutlet
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
    }
    
    // MARK: - Webservice
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}
