//
//  DiscountVC.swift
//  Snagpay
//
//  Created by Apple on 16/09/21.
//

import UIKit
import AVKit

class DiscountVC: UIViewController {
    
    // MARK: - Variable
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblEquation1: UILabel!
    @IBOutlet weak var lblEquation2: UILabel!
    @IBOutlet weak var lblEquation3: UILabel!
    @IBOutlet weak var lblEquation4: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        creditLineHistoryAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        
    }
    
    func playVideo(url:String) {
        
        guard let url = URL(string: url) else { return }
        let player = AVPlayer(url: url)
        player.rate = 1 //auto play
        let playerFrame = CGRect(x: 0, y: 0, width: self.viewVideo.frame.width, height: self.viewVideo.frame.height)
        let playerViewController = AVPlayerViewController()
        playerViewController.videoGravity = .resizeAspectFill
        playerViewController.player = player
        playerViewController.view.frame = playerFrame
        
        self.addChild(playerViewController)
        self.viewVideo.addSubview(playerViewController.view)
        playerViewController.didMove(toParent: self)
    }
    
    // MARK: - Webservice
    func creditLineHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallCreditLineHistory(url: "\(URLs.credit_line_history)", parameters: [:], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblEquation1.text = "\(response.data?.discount_txt?.cost_of_goods ?? 0)%"
                self.lblEquation2.text = "\(response.data?.discount_txt?.fee_to_trade ?? 0)%"
                self.lblEquation3.text = "\(response.data?.discount_txt?.total_cost_of_trade ?? 0)%"
                self.lblEquation4.text = "\(response.data?.discount_txt?.saving_on_snagpay ?? 0)%"
                self.lblBalance.text = response.data?.available_credit_line
                self.playVideo(url: response.data?.video_url ?? "")
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNew(_ sender: Any) {
        
        let customView = Bundle.main.loadNibNamed("AddDiscountView", owner: self, options: nil)?.first as? AddDiscountView
        
        customView?.frame = self.view.frame
        self.view.addSubview(customView!)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

