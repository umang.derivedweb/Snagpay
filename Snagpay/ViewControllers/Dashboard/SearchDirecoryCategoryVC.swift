//
//  SearchDirecoryCategoryVC.swift
//  Snagpay
//
//  Created by Apple on 11/12/21.
//

import UIKit

class SearchDirecoryCategoryVC: UIViewController {

    // MARK: - Variables
    
    var arrCategories:[GetCategoryData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var collview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategoryAPI()
    }
    
    // MARK: - Function
    
    
    //MARK: - Webservice
    func getCategoryAPI() {
        //Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCategory(url: URLs.get_categories, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCategories = response.data?.categories
                self.collview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }

    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
}
extension SearchDirecoryCategoryVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategories?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.title.text = arrCategories?[indexPath.row].category_name
        
        cell.img.sd_setImage(with: URL(string: arrCategories?[indexPath.row].category_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.viewBg.backgroundColor = Helper.shared.hexStringToUIColor(hex: arrCategories?[indexPath.row].backround_color ?? "")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AlphabetsVC") as! AlphabetsVC
        obj.strCategoryId = "\(arrCategories?[indexPath.row].category_id ?? 0)"
        obj.strCatName = arrCategories?[indexPath.row].category_name
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2)-5, height: (collectionView.frame.height/3)-5)
    }
}
