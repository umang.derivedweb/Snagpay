//
//  SearchDirectoryVC.swift
//  Snagpay
//
//  Created by Apple on 17/09/21.
//

import UIKit

class SearchDirectoryVC: UIViewController {

    // MARK: - Variable
    var selectedIndexPath: IndexPath? = nil
    var arrMerchants:[UserData] = []
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    var searchText = ""
    var strCategoryId:String?
    var strCatName:String?
    var strSearchBy:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getMerchantsAPI(search: "")
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = 80
        table.tableFooterView = UIView()
        lblHeader.text = strCatName
    }
    
    // MARK: - Webservice
    
    func getMerchantsAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetMerchants(url: "\(URLs.get_merchants)\(search)&category_id=\(strCategoryId!)&start_with=\(strSearchBy!)&page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data?.last_page ?? 0
                self.arrMerchants += response.data?.data ?? []
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnHome(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension SearchDirectoryVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMerchants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchDirectoryCell") as! SearchDirectoryCell
        cell.lblTitle.text = "\(arrMerchants[indexPath.row].first_name ?? "") \(arrMerchants[indexPath.row].last_name ?? "")"
        cell.lblSubTitle.text = arrMerchants[indexPath.row].business_name
        cell.lblEmail.text = arrMerchants[indexPath.row].email
        cell.lblWorkphone.text = arrMerchants[indexPath.row].work_phone
        cell.lblCellphone.text = arrMerchants[indexPath.row].cell_phone
        cell.lblType.text = arrMerchants[indexPath.row].type
        cell.lblAddress.text = arrMerchants[indexPath.row].address
        cell.lblCity.text = arrMerchants[indexPath.row].city
        cell.lblState.text = arrMerchants[indexPath.row].state
        cell.lblZip.text = arrMerchants[indexPath.row].postcode
        cell.lblPhone.text = arrMerchants[indexPath.row].phone_no
        cell.lblWebsite.text = arrMerchants[indexPath.row].website_or_page
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchDirectoryCell", for: indexPath) as! SearchDirectoryCell

        if selectedIndexPath == nil {
            
            selectedIndexPath = indexPath
            cell.imgArrow.image = #imageLiteral(resourceName: "arrow-down")
        } else {
            if selectedIndexPath! == indexPath {
                selectedIndexPath = nil
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow_right")
            } else {
                selectedIndexPath = indexPath
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow-down")
            }
        }
        
        
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let smallHeight: CGFloat = 80.0
        let expandedHeight: CGFloat = 244.0
        let ip = indexPath
        if selectedIndexPath != nil {
            if ip as IndexPath == selectedIndexPath! {
                return expandedHeight
            } else {
                return smallHeight
            }
        } else {
            return smallHeight
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrMerchants.count {
            if (indexPath.row == (self.arrMerchants.count) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrMerchants.count
                    getMerchantsAPI(search: searchText)
                }
                
            }
        }
    }
}
extension SearchDirectoryVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 0.5)
        return true
    }

    @objc func getHintsFromTextField(textField: UITextField) {
        print("Hints for textField: \(textField)")
        searchText = textField.text ?? ""
        intCountArr = 0
        self.arrMerchants.removeAll()
        currentPage = 1
        getMerchantsAPI(search: textField.text ?? "")
    }
}
