//
//  PreviewDealVC.swift
//  Snagpay
//
//  Created by Apple on 30/10/21.
//

import UIKit

class PreviewDealVC: UIViewController {

    
    // MARK: - Variable
    
    var params:[String:Any]?
    var imgMain:UIImage?
    var arrImages:[UIImage] = []
    var arrCheckbox:[Int] = []
    var isFromMyDealVC = false
    var dictDraftDetails:DealDraftDetailsData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var table: ContentSizedTableView!
    @IBOutlet weak var lblHighlight: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var lblHighlight1: UILabel!
    @IBOutlet weak var lblHighlight1Desc: UILabel!
    @IBOutlet weak var lblHighlight2: UILabel!
    @IBOutlet weak var lblHighlight2Desc: UILabel!
    @IBOutlet weak var collview: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblNoDeals: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        lblTitle.text = "\(params!["company_name"] ?? "")\n\(params!["title"] ?? "")"
        //lblDropdown.text = Helper.shared.arrNamePreview[0]
        lblPrice.text = "$ \(Helper.shared.arrPricePreview[0])"
        lblAbout.text = params!["deal_description"] as? String
        lblHighlight.text = params!["description"] as? String
        lblNoDeals.text = params!["no_of_egifts_available[1]"] as? String
        
        
        if params!["description_2"] as! String == "" {
            //lblHighlight1.isHidden = true
            //lblHighlight1Desc.isHidden = true
            lblHighlight1Desc.text = "No details available"
        } else {
            lblHighlight1Desc.text = params!["description_2"] as? String
        }
        if params!["description_3"] as! String == "" {
            //lblHighlight2.isHidden = true
            //lblHighlight2Desc.isHidden = true
            lblHighlight2Desc.text = "No details available"
        } else {
            lblHighlight2Desc.text = params!["description_3"] as? String
        }
        
        for i in 0..<Helper.shared.arrNamePreview.count {
            
            if i == 0 {
                arrCheckbox.append(1)
            } else {
                arrCheckbox.append(0)
            }
            
        }
        table.reloadData()
        
        img.image = imgMain//arrImages?[0]
        self.heightTable?.constant = 0.0
        
    }
    func setTableHeight() {
        DispatchQueue.main.async {
          // your code here
            self.heightTable.constant = self.table.contentSize.height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Webservice
    
    func createDealAPI() {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        for i in 0..<arrImages.count {
            
            imagesData.append((arrImages[i].jpegData(compressionQuality: 0.5)!))
        }
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain != nil {
            
            mainImageData.append((imgMain?.jpegData(compressionQuality: 0.5))!)
        }
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.create_deal, mainImageData: mainImageData, imagesData: imagesData, parameters: params! as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tabBarController?.selectedIndex = 4
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func updateDealAPI() {
        
        Helper.shared.showHUD()
        
        var imagesData:[Data] = []
        
        arrImages.remove(at: 0)
        for i in 0..<arrImages.count {
            
            imagesData.append((arrImages[i].jpegData(compressionQuality: 0.5)!))
        }
        //Its a single image
        var mainImageData:[Data] = []
        if imgMain != nil {
            
            mainImageData.append((imgMain?.jpegData(compressionQuality: 0.5))!)
        }
        
        params?["deal_id"] = "\(dictDraftDetails?.deal_details?.deal_id ?? 0)"
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCreateDealorSaveDraft(url: URLs.update_deal, mainImageData: mainImageData, imagesData: imagesData, parameters: params! as [String : Any], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tabBarController?.selectedIndex = 4
                    self.navigationController?.popToRootViewController(animated: false)
                }
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnDealDropdown(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            setTableHeight()
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "arrow-up"), for: .normal)
        } else {
            self.heightTable?.constant = 0.0
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "arrow-down"), for: .normal)
        }
    }
    
    @IBAction func btnAboutDropdown(_ sender: UIButton) {
        
        if sender.tag == 0 {
            
            sender.tag = 1
            lblAbout.numberOfLines = 0
            sender.setImage(#imageLiteral(resourceName: "arrow-up"), for: .normal)
        } else {
            
            sender.tag = 0
            lblAbout.numberOfLines = 2
            sender.setImage(#imageLiteral(resourceName: "arrow-down"), for: .normal)
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        if isFromMyDealVC {
            updateDealAPI()
        } else {
            createDealAPI()
        }
    }
    
    @objc func btnCheckbox(sender:UIButton) {
        
        let index = arrCheckbox.firstIndex(of: 1)!
        arrCheckbox.remove(at: index)
        arrCheckbox.insert(0, at: index)
        
        arrCheckbox.remove(at: sender.tag)
        arrCheckbox.insert(1, at: sender.tag)
        table.reloadData()
        
        lblDropdown.text = Helper.shared.arrNamePreview[sender.tag]
        lblPrice.text = "$ \(Helper.shared.arrPricePreview[sender.tag])"
    }
    
}
extension PreviewDealVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Helper.shared.arrNamePreview.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        cell.lblTitle.text = Helper.shared.arrNamePreview[indexPath.row]
        
        if arrCheckbox[indexPath.row] == 1 {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        } else {
            cell.btnCheckbox.setImage(#imageLiteral(resourceName: "empty_radio"), for: .normal)
        }
        
        cell.btnCheckbox.tag = indexPath.row
        cell.btnCheckbox.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        setTableHeight()
    }
}
extension PreviewDealVC : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        arrImages.insert(imgMain!, at: 0)
        pageControl.numberOfPages = arrImages.count
        pageControl.isHidden = !((arrImages.count) > 1)
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.img.image = arrImages[indexPath.row]
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if scrollView == collview {
            pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        
    }
    
}
