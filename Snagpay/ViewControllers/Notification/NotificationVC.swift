//
//  NotificationVC.swift
//  Snagpay
//
//  Created by Apple on 10/02/21.
//

import UIKit

class NotificationVC: UIViewController {

    // MARK: - Variable
    var arrNotifications:[NotificationData]?
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblCity: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getNotificationAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        lblCity.text = Helper.shared.strCity
    }
    // MARK: - Webservice
    func getNotificationAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetGlobalNotifications(url: URLs.get_global_notifications, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrNotifications = response.data?.data
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

}
extension NotificationVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.lblTitle.text  = arrNotifications?[indexPath.row].title
        cell.lblTime.text  = arrNotifications?[indexPath.row].date
        cell.lblDesc.text  = arrNotifications?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrNotifications?[indexPath.row].image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        return cell
    }
}
