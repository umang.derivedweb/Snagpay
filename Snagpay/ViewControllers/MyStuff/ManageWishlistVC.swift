//
//  ManageWishlistVC.swift
//  Snagpay
//
//  Created by Apple on 12/02/21.
//

import UIKit

class ManageWishlistVC: UIViewController {

    // MARK: - Variables
    var arrCitiesId:[Int] = []
    var arrCategoriesId:[Int] = []
    var arrCategory:[WishlistCategoriesData]?
    var arrCity:[WishlistCitiesData]?
    
    var arrSelectedCities:[String] = []
    var arrSelectedCategories:[String] = []
    
    var isFromVC:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getMyWishlistAPI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ManageWishlist"), object: nil)

    }
    

    // MARK: - Function
    
    func setupUI() {
        
        if isFromVC != "Dashboard" {
            table.tableFooterView = UIView()
        }
        table.rowHeight = 50
        
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        table.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        let nib2 = UINib(nibName: "TableSectionFooter", bundle: nil)
        table.register(nib2, forHeaderFooterViewReuseIdentifier: "TableSectionFooter")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ManageWishlist"), object: nil)
    }
    

    @objc func methodOfReceivedNotification(notification: Notification) {
        
        print(Helper.shared.arrCities)
        print(Helper.shared.arrCategories)
        manageWishlistAPI()
    }
    
    @objc func btnAddCity(sender:UIButton) {
        if sender.tag == 1 {
            let customView = Bundle.main.loadNibNamed("AddNewCity", owner: self, options: nil)?.first as! AddNewCity
            customView.frame = self.view.frame
            customView.arrLocalCities = arrCitiesId
            self.view.addSubview(customView)
        }
        
    }
    
    @objc func btnAddCategory(sender:UIButton) {
        if sender.tag == 2 {
            let customView = Bundle.main.loadNibNamed("AddCategoryView", owner: self, options: nil)?.first as! AddCategoryView
            customView.frame = self.view.frame
            Helper.shared.arrCategories = arrCategoriesId
            self.view.addSubview(customView)
        }
        
    }
    
    @objc func btnCheckbox(sender:UIButton) {
        if let index = Helper.shared.arrCities.firstIndex(of: arrCitiesId[sender.tag]) {
            
            Helper.shared.arrCities.remove(at: index)
            manageWishlistAPI()
        }
        
    }
    @objc func btnDelete(sender:UIButton) {
        
        if let index = Helper.shared.arrCategories.firstIndex(of: arrCategoriesId[sender.tag]) {
            
            Helper.shared.arrCategories.remove(at: index)
            manageWishlistAPI()
        }
    }
    // MARK: - Webservice

    func manageWishlistAPI() {
        Helper.shared.showHUD()
        
        let params = ["city_ids":(Helper.shared.arrCities.map{String($0)}).joined(separator: ","),
                      "category_ids":(Helper.shared.arrCategories.map{String($0)}).joined(separator: ",")] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.manage_my_wishlist, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.getMyWishlistAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func getMyWishlistAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetMyWishlist(url: "\(URLs.get_my_wishlist)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCity = response.city
                self.arrCategory = response.category
                self.arrCitiesId.removeAll()
                self.arrCategoriesId.removeAll()
                for i in 0..<(self.arrCity?.count ?? 0) {
                    
                    self.arrCitiesId.append(self.arrCity?[i].city_id ?? 0)
                }
                Helper.shared.arrCities = self.arrCitiesId
                for i in 0..<(self.arrCategory?.count ?? 0) {
                    
                    self.arrCategoriesId.append(self.arrCategory?[i].category_id ?? 0)
                }
                Helper.shared.arrCategories = self.arrCategoriesId
                self.table.reloadData()
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
extension ManageWishlistVC:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrCity?.count ?? 0
        }
        return arrCategory?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MAnageWishlistCell") as! MAnageWishlistCell
        if indexPath.section == 0 {
            cell.lblTitle.text = arrCity?[indexPath.row].city_name
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(btnCheckbox(sender:)), for: .touchUpInside)
            cell.btnCheck.isHidden = false
            cell.btnDelete.isHidden = true
        } else {
            cell.lblTitle.text = arrCategory?[indexPath.row].category_name
            cell.btnCheck.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDelete(sender:)), for: .touchUpInside)
            cell.btnCheck.isHidden = true
            cell.btnDelete.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.table.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionFooter") as! TableSectionFooter
        
        if section == 0 {
            cell.lblTitle.text = "Deals near you"
        } else {
            cell.lblTitle.text = "Saved categories"
        }

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let cell = self.table.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
        cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if section == 0 {
            cell.btnAdd.addTarget(self, action: #selector(btnAddCity(sender:)), for: .touchUpInside)
            cell.btnAdd.tag = 1
            cell.btnAdd.setTitle("Add another city", for: .normal)
        } else {
            cell.btnAdd.addTarget(self, action: #selector(btnAddCategory(sender:)), for: .touchUpInside)
            cell.btnAdd.tag = 2
            cell.btnAdd.setTitle("Add new category", for: .normal)
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
}
