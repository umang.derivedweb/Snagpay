//
//  MyMerchantVC.swift
//  Snagpay
//
//  Created by Apple on 01/12/21.
//

import UIKit

class MyMerchantVC: UIViewController {
    
    // MARK: - Variable
    var arrMerchants:[UserData] = []
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getMerchantsAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = 50
        table.tableFooterView = UIView()
    }
    
    // MARK: - Webservice
    
    func getMerchantsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetMerchants(url: "\(URLs.get_my_merchants)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.last_page = response.data?.last_page ?? 0
                self.arrMerchants += response.data?.data ?? []
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension MyMerchantVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMerchants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as! GeneralCell
        
        if arrMerchants[indexPath.row].business_name == "" {
            cell.lblTitle.text = "No Business Name"
        } else {
            cell.lblTitle.text = "\(arrMerchants[indexPath.row].business_name ?? "")"
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
        obj.merchant_id = "\(self.arrMerchants[indexPath.row].user_id ?? 0)"
        obj.strType = "merchant"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrMerchants.count {
            if (indexPath.row == (self.arrMerchants.count) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrMerchants.count
                    getMerchantsAPI()
                }
                
            }
        }
    }
}

