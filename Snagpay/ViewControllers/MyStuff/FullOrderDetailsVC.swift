//
//  FullOrderDetailsVC.swift
//  Snagpay
//
//  Created by Apple on 10/09/21.
//

import UIKit

class FullOrderDetailsVC: UIViewController {

    // MARK: - Variable
    var intOrderId:Int!
    var arrItems:[ItemsData]?
    var tableHeight:CGFloat = 0.0
    
    // MARK: - IBOutlet
    @IBOutlet weak var heightContinueShopping: NSLayoutConstraint!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var table: ContentSizedTableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddressLine1: UILabel!
    @IBOutlet weak var lblAddressLine2: UILabel!
    @IBOutlet weak var lblAddressLine3: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSnagpayBucks: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSubAmount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var btnContShopping: UIButton!
    @IBOutlet weak var lblOrderCode: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getOrderDetailsAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        tableHeight = CGFloat(table.frame.height)
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnContShopping.layer.cornerRadius = 25
        }
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
           // Code you want to be delayed
            self.heightTable?.constant = self.table.contentSize.height
        }
    }
    
    //MARK: - Webservice
    func getOrderDetailsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetFullOrderDetails(url: "\(URLs.full_order_details)\(intOrderId ?? 0)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblOrderCode.text = "\(response.data?.order_code ?? "")"
                self.lblOrderTime.text = "\(response.data?.order_datetime ?? "")"
                self.lblOrderStatus.text = "\(response.data?.order_status ?? "")"
                self.lblName.text = "\(response.data?.name ?? "")"
                self.lblAddressLine1.text = "\(response.data?.address ?? "")"
                self.lblAddressLine2.text = "\(response.data?.street ?? "")"
                self.lblAddressLine3.text = "\(response.data?.zip_code ?? "")"
                self.lblDiscount.text = "$ \(response.data?.discount ?? 0)"
                self.lblSubAmount.text = "$ \(response.data?.sub_total_amount ?? 0)"
                self.lblTotalAmount.text = "$ \(response.data?.total_amount ?? 0)"
                self.arrItems = response.data?.items
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueShopping(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
        navigationController?.pushViewController(obj, animated: false)
        
    }
}
extension FullOrderDetailsVC:UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems?.count ?? 0

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell") as! WishlistCell
        cell.lblTitle.text = "\(arrItems?[indexPath.row].company_name ?? "")\n\(arrItems?[indexPath.row].title ?? "")"
        cell.lblDesc.text = "\(arrItems?[indexPath.row].bought ?? "")+ Bought"
        cell.lblPrice.text = "$ \(arrItems?[indexPath.row].sell_price ?? 0)"
        cell.img.sd_setImage(with: URL(string: arrItems?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
}
