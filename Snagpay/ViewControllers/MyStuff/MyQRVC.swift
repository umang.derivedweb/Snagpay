//
//  MyQRVC.swift
//  Snagpay
//
//  Created by Apple on 23/11/21.
//

import UIKit
import WebKit
import SVGKit

class MyQRVC: UIViewController {

    // MARK: - Variable
    
    
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgQR: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getProfileAPI()
        viewBg.shadowToView()
    }
    

    // MARK: - Function
    
    // MARK: - Webservice
    func getProfileAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetProfile(url: URLs.get_profile, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblName.text = "\(response.data?.user?.first_name ?? "") \(response.data?.user?.last_name ?? "")"
                
                let url = URL(string: response.data?.user?.qrcode ?? "")
                
                if let data = try? Data(contentsOf: url!)
                {
                    let anSVGImage: SVGKImage = SVGKImage(data: data)
                    self.imgQR.image = anSVGImage.uiImage
                }
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
