//
//  MerchantLocationVC.swift
//  Snagpay
//
//  Created by Apple on 09/02/22.
//

import UIKit
import SDWebImage
import SVGKit

class MerchantLocationVC: UIViewController {

    // MARK: - Variable
    var arrLocations:[LocationData]?
    var dictFilter:[String:String]?
    var strUserLocId:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        getLocationsAPI()
    }

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReportFilter"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        
        dictFilter = notification.userInfo as? [String : String]
        
        reportShowPDFAPI()
    }
    // MARK: - Webservice
    func getLocationsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallUserLocations(url: URLs.get_user_locations, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrLocations = response.data
                self.table.reloadData()
                
                if self.arrLocations?.count == 0 {
                    self.table.isHidden = true
                }
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func reportShowPDFAPI() {
        Helper.shared.showHUD()
        
        print(dictFilter)
        dictFilter?["user_location_id"] = strUserLocId
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallDownloadReports(url: URLs.qr_sales_report, parameters: dictFilter!, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = response.data?.pdf ?? ""
                self.navigationController?.pushViewController(obj, animated: true)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteAddressAPI(locationId:String) {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["user_location_id"] = locationId
        
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.delete_user_location, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.getLocationsAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddLocation(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddMerchantLocationVC") as! AddMerchantLocationVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnEditAddress(sender:UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddMerchantLocationVC") as! AddMerchantLocationVC
        obj.location_id = "\(arrLocations?[sender.tag].user_location_id ?? 0)"
        obj.isEdit = true
        obj.dictEditAddress = arrLocations?[sender.tag]
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnDeleteAddress(sender:UIButton) {
        
        deleteAddressAPI(locationId: "\(arrLocations?[sender.tag].user_location_id ?? 0)")
    }
    @objc func btnReport(sender:UIButton) {
        
        loadReportView()
        strUserLocId = "\(arrLocations?[sender.tag].user_location_id ?? 0)"
    }
    
    func loadReportView() {
        
        let customView = Bundle.main.loadNibNamed("ReportFilterView", owner: self, options: nil)?.first as! ReportFilterView
        customView.tag = 2001
        customView.frame = view.frame
        view.addSubview(customView)
    }
}
extension MerchantLocationVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrLocations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MerchantLocationCell") as! MerchantLocationCell
        
        cell.btnEdit.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        cell.btnReport.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditAddress(sender:)), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteAddress(sender:)), for: .touchUpInside)
        cell.btnReport.addTarget(self, action: #selector(btnReport(sender:)), for: .touchUpInside)
        
        cell.lblEIN.text = "EIN: \(arrLocations?[indexPath.row].ein ?? "")"
        cell.lblEntityName.text = "Entity Name: \(arrLocations?[indexPath.row].entity_name ?? "")"
        cell.lblEmail.text = "Email: \(arrLocations?[indexPath.row].email ?? "")"
        cell.lblPhone.text = "Work Phone: \(arrLocations?[indexPath.row].work_phone ?? "")"
        cell.lblAddress.text = "Address: \(arrLocations?[indexPath.row].address ?? "")"
        cell.lblState.text = "State: \(arrLocations?[indexPath.row].state_name ?? "")"
        cell.lblCity.text = "City: \(arrLocations?[indexPath.row].city_name ?? "")"
        cell.lblZip.text = "Zip code: \(arrLocations?[indexPath.row].postcode ?? "")"
        
        let url = URL(string: arrLocations?[indexPath.row].qrcode ?? "")
        
        if let data = try? Data(contentsOf: url!)
        {
            let anSVGImage: SVGKImage = SVGKImage(data: data)
            cell.imgQR.image = anSVGImage.uiImage
        }
        return cell
    }
}
