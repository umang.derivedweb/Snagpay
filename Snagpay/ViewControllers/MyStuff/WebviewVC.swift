//
//  WebviewVC.swift
//  Snagpay
//
//  Created by Apple on 24/08/21.
//

import UIKit
import WebKit
import SVProgressHUD

class WebviewVC: UIViewController, WKNavigationDelegate, WKUIDelegate {

    var strUrl:String!
    
    @IBOutlet weak var webview: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        webview.navigationDelegate = self
        webview.uiDelegate = self
        
        self.view.addSubview(webview)
        let url = URL(string: strUrl)
        webview.load(URLRequest(url: url!))
    }
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Helper.shared.hideHUD()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Helper.shared.showHUD()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Helper.shared.hideHUD()
    }
}
