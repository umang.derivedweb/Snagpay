//
//  AddMoneyVC.swift
//  Snagpay
//
//  Created by Apple on 19/02/21.
//

import UIKit

class AddMoneyVC: UIViewController {

    // MARK: - Variables
    
    // MARK: - IBOutlet
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Function
    
    // MARK: - IBAction
    @IBAction func btnProceed(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SelectCardVC") as! SelectCardVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
