//
//  CreditLineVC.swift
//  Snagpay
//
//  Created by Apple on 11/12/21.
//

import UIKit

class CreditLineVC: UIViewController {

    // MARK: - Variable
    var dictFilter:[String:String] = [:]
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    var arrHistory:[HistoryData] = []
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblCreditLineBalance: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblTotalCredits: UILabel!
    @IBOutlet weak var lblAvailCredits: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        creditLineHistoryAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ReportFilter"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        intCountArr = 0
        arrHistory.removeAll()
        currentPage = 1
        
        dictFilter = (notification.userInfo as? [String : String]) ?? [:]
        creditLineHistoryAPI()

    }
    
    func loadReportView() {
        
        let customView = Bundle.main.loadNibNamed("ReportFilterView", owner: self, options: nil)?.first as! ReportFilterView
        customView.tag = 2001
        customView.frame = view.frame
        view.addSubview(customView)
    }
    
    @objc func btnDealDetails(sender:UIButton) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = "\(arrHistory[sender.tag].deal_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
        
    }
    
    // MARK: - Webservice
    
    func creditLineHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallCreditLineHistory(url: "\(URLs.credit_line_history)?page=\(currentPage)", parameters: dictFilter, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblCreditLineBalance.text = response.data?.available_credit_line
                self.lblTotalCredits.text = response.data?.credit_line
                self.lblAvailCredits.text = response.data?.available_credit_line
                self.last_page = response.data?.history?.last_page ?? 0
                self.arrHistory += response.data?.history?.data ?? []
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        
        loadReportView()
    }
}
extension CreditLineVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreditLineCell") as! CreditLineCell
        cell.lblTitle.text = arrHistory[indexPath.row].transaction_title
        cell.lblCredit.text = "$ \(arrHistory[indexPath.row].credit ?? 0)"
        cell.lblDate.text = arrHistory[indexPath.row].datetime
        
        cell.btnDealName.tag = indexPath.row
        cell.btnDealName.addTarget(self, action: #selector(btnDealDetails(sender:)), for: .touchUpInside)
        
        if let _ = arrHistory[indexPath.row].title {
            
            cell.btnDealName.setTitle(arrHistory[indexPath.row].title, for: .normal)
        } else {
            cell.btnDealName.setTitle("", for: .normal)
            let label = cell.contentView.viewWithTag(101) as! UILabel
            label.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if intCountArr != self.arrHistory.count {
            if (indexPath.row == (self.arrHistory.count) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrHistory.count
                    creditLineHistoryAPI()
                }
                
            }
        }
    }
    
}
