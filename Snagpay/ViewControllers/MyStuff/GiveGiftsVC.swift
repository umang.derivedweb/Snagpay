//
//  GiveGiftsVC.swift
//  Snagpay
//
//  Created by Apple on 30/11/21.
//

import UIKit
import SVGKit

class GiveGiftsVC: UIViewController {
    
    // MARK: - Variable
    var arrOrder:[OrdersListData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getAllOrdersAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        self.hideKeyboardWhenTappedAround()
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = UITableView.automaticDimension
            table.estimatedRowHeight = 150
        } else {
            table.rowHeight = UITableView.automaticDimension
            table.estimatedRowHeight = 120
        }
        table.tableFooterView = UIView()
    }
    
    // MARK: - Webservice
    
    func getAllOrdersAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGiveGifts(url: URLs.give_gifts, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                
                self.arrOrder = response.data
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
extension GiveGiftsVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrder?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiveGiftCell") as! GiveGiftCell
        cell.lblTitle.text = "\(arrOrder?[indexPath.row].title ?? "")"
        cell.lblQty.text = "Quantity: \(arrOrder?[indexPath.row].qty ?? 0)"
        cell.lblAmount.text = "Amount: \(arrOrder?[indexPath.row].amount ?? 0)"
        cell.lblRedeemedAmount.text = "Redeemed Amount: \(arrOrder?[indexPath.row].redeemed_amount ?? 0)"
        
        let url = URL(string: arrOrder?[indexPath.row].qrcode ?? "")
        
        if let data = try? Data(contentsOf: url!)
        {
            let anSVGImage: SVGKImage = SVGKImage(data: data)
            cell.img.image = anSVGImage.uiImage
        }
        
        
        return cell
    }
    
    
}


