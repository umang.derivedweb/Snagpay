//
//  WalletVC.swift
//  Snagpay
//
//  Created by Apple on 19/02/21.
//

import UIKit

class WalletVC: UIViewController {

    //MARK: Variables
    
    
    var arrPaymentList:[PaymentListData]?
    
    var arrImages:[String] = []


    var selectedIndex = -1

    // i.e initially no row is selected

    var isExpanded = false
    
    //MARK: IBOutlets
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getSnagpayWalletAPI()
    }
    
    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        table.estimatedRowHeight = 100
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
        
        heightConstraint.constant = Helper.shared.screenHeight - (440+getStatusBarHeight())
    }
    func getStatusBarHeight() -> CGFloat {
        var statusBarHeight: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height
        }
        return statusBarHeight
    }

    //MARK: - Webservice
    func getSnagpayWalletAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetSnagpayWallet(url: URLs.get_snagpay_wallet, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblCredits.text = "\(response.data?.trade_credit ?? "")"
                self.arrPaymentList = response.data?.recent_transactions
                self.arrImages.removeAll()
                for _ in 0..<(self.arrPaymentList?.count ?? 0) {
                    
                    self.arrImages.append("dropdown")
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    //MARK: IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStetemt(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "StatementVC") as! StatementVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnAddMoney(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyVC") as! AddMoneyVC
        navigationController?.pushViewController(obj, animated: true)
    }
    

}

extension WalletVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrPaymentList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        
        //cell.lblQue.text = arrQue[indexPath.row]
        //cell.lblAns.text = arrAns[indexPath.row]
        cell.imgArrow.image = UIImage(named: arrImages[indexPath.row])
        
        if cell.imgArrow.image == UIImage(named: "dropdown") {
            
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        }
        cell.lblTransactionTitle.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblTransactionType.text = arrPaymentList?[indexPath.row].business_name
        cell.lblDate.text = arrPaymentList?[indexPath.row].datetime
        //cell.lblPaidUsing.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblOrderId.text = arrPaymentList?[indexPath.row].e_wallet_tran_code
        cell.lblDealName.text = "Deal Name: \(arrPaymentList?[indexPath.row].title ?? "")"
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (self.selectedIndex == indexPath.row && isExpanded == true) {
            
            return UITableView.automaticDimension
        } else {
            
            return 100
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrImages.count {
            
            arrImages.remove(at: i)
            arrImages.insert("dropdown", at: i)
        }
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndex == indexPath.row {
            
            if self.isExpanded == false {
                
                self.isExpanded = true
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("top", at: indexPath.row)
            } else {
                
                self.isExpanded = false
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("dropdown", at: indexPath.row)
            }
        } else {
            
            self.isExpanded = true
            
            self.selectedIndex = indexPath.row
            arrImages.remove(at: indexPath.row)
            arrImages.insert("top", at: indexPath.row)
        }
        
        //tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
    }
    
}



