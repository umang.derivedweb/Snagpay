//
//  MySnagpayDeals.swift
//  Snagpay
//
//  Created by Apple on 22/03/21.
//

import UIKit

class SnagpayDealsVC: UIViewController, FiterData {
    
    // MARK: - Variables
    var arrDeals:[DealsListData] = []
    
    var strSort = ""
    var strCatId = ""
    var strStartPrice = ""
    var strEndPrice = ""
    var strDistance = ""
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var lblNumCart: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getSnagpayDealsAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
    }

    // MARK: - Function
    func setupUI() {
        lblCredits.text = Helper.shared.strAvailableCredits
    }
    func sendData(sort: String, catId: String, StartPrice: String, endPrice: String, distance:String) {
        
        strSort = sort
        strCatId = catId
        strStartPrice = StartPrice
        strEndPrice = endPrice
        strDistance = distance
    }
    //MARK: - Webservice
    func getSnagpayDealsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetCategoryDeals(url: "\(URLs.get_my_snagpay_deals)?page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.arrDeals += (response.data?.deals?.data)!
                self.collectionview.reloadData()
                self.last_page = response.data?.deals?.last_page ?? 0
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
//    @IBAction func btnFilter(_ sender: Any) {
//
//        let obj = UIStoryboard(name: "Category", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
//        obj.arrSubCatData = arrSubCatData
//        obj.delegate = self
//        navigationController?.pushViewController(obj, animated: true)
//    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnGotoCart(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        navigationController?.pushViewController(obj, animated: true)
    }
}

extension SnagpayDealsVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDeals.count 
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
        cell.lblTitle.text = arrDeals[indexPath.row].title
        cell.lblDistance.text = arrDeals[indexPath.row].city_name
        cell.lblPrice.text = "$ \(arrDeals[indexPath.row].sell_price ?? 0)"
        cell.lblBought.text = "\(arrDeals[indexPath.row].bought ?? "")+ bought"
        cell.lblRating.text = "(\(arrDeals[indexPath.row].total_rating ?? 0) rating)"
        cell.viewRating.rating = Double(arrDeals[indexPath.row].avg_rating ?? 0.0)
        cell.img.sd_setImage(with: URL(string: arrDeals[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = "\(arrDeals[indexPath.row].deal_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.collectionview.frame.width/2)-5, height: 450)
        }
        return CGSize(width: (self.collectionview.frame.width/2)-5, height: 346)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if intCountArr != self.arrDeals.count {
            if (indexPath.row == (self.arrDeals.count ) - 1 ) { //it's your last cell
                if last_page != currentPage {
                    
                    //Load more data & reload your collection view
                    print("Last row")
                    currentPage = currentPage + 1
                    intCountArr = self.arrDeals.count
                    getSnagpayDealsAPI()
                }
                
            }
        }
    }
}

