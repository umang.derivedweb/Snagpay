//
//  GiftCardDetailsVC.swift
//  Snagpay
//
//  Created by Apple on 24/08/21.
//

import UIKit

class GiftCardDetailsVC: UIViewController {

    // MARK: - Variable
    var intGiftId:Int!
    var arrTransaction:[GiftCardsTransactionsData]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        giftCardsTransactionAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.estimatedRowHeight = 50
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
    }
    
    
    // MARK: - Webservice
    func giftCardsTransactionAPI() {
        Helper.shared.showHUD()
        
        let params = ["e_gift_card_id":"\(intGiftId ?? 0)"]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetGiftCardsTransactions(url: URLs.e_gift_card_transactions, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrTransaction = response.data
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension GiftCardDetailsVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTransaction?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GiftcardDetailsCell") as! GiftcardDetailsCell
        cell.lblCode.text = arrTransaction?[indexPath.row].e_gift_card_tran_code
        cell.lblAmount.text = "\(arrTransaction?[indexPath.row].amount ?? 0)"
        
        return cell
    }
    
    
}
