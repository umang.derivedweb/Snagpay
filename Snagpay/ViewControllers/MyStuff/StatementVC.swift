//
//  StatementVC.swift
//  Snagpay
//
//  Created by Apple on 19/02/21.
//

import UIKit

class StatementVC: UIViewController {
    
    
    // MARK: - Variable
    var arrPaymentList:[PaymentListData]?
    
    var arrImages:[String] = []


    var selectedIndex = -1

    // i.e initially no row is selected

    var isExpanded = false
    
    var strMonth:String?
    var strYear:String?
    var arrMonths:[[String:String]] = [["month":"January","number":"1"],
                                       ["month":"February","number":"2"],
                                       ["month":"March","number":"3"],
                                       ["month":"April","number":"4"],
                                       ["month":"May","number":"5"],
                                       ["month":"June","number":"6"],
                                       ["month":"July","number":"7"],
                                       ["month":"August","number":"8"],
                                       ["month":"September","number":"9"],
                                       ["month":"October","number":"10"],
                                       ["month":"November","number":"11"],
                                       ["month":"December","number":"12"]]
    var arrYear = ["2021","2020","2019","2018"]
    var selectedButtonTag = 1
    var picker:UIPickerView?
    var intSelectedRow:Int!
    var tappedTextfieldTag:Int?
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnPdf: UIButton!
    @IBOutlet weak var txtMonth: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        table.estimatedRowHeight = 100
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
        toolBar()
    }
    func toolBar() {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtYear.inputAccessoryView = toolBar
        txtMonth.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        if tappedTextfieldTag == 1 {
            strYear = arrYear[intSelectedRow]
            txtYear.text = arrYear[intSelectedRow]
            txtYear.resignFirstResponder()
        }
        if tappedTextfieldTag == 2 {
            strMonth = arrMonths[intSelectedRow]["number"]
            txtMonth.text = arrMonths[intSelectedRow]["month"]
            txtMonth.resignFirstResponder()
        }
        
    }
    @objc func cancelClick() {
        self.view.endEditing(true)
    }
    //MARK: - Webservice
    func getPaymentHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetRecentPaymentHistory(url: "\(URLs.monthly_payment_history)month=\(strMonth!)&year=\(strYear!)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrPaymentList = response.data?.data
                self.arrImages.removeAll()
                for _ in 0..<(self.arrPaymentList?.count ?? 0) {
                    
                    self.arrImages.append("dropdown")
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func paymentDownloadAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetMonthlyPaymentDownload(url: "\(URLs.monthly_payment_download)month=\(strMonth!)&year=\(strYear!)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                DispatchQueue.main.async {
                    let url = URL(string: response.data?.pdf_url ?? "")
                    let pdfData = try? Data.init(contentsOf: url!)
                    let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                    let pdfNameFromUrl = "snagpay-\(String.random()).pdf"
                    let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
                    do {
                        try pdfData?.write(to: actualPath, options: .atomic)
                        print("pdf successfully saved!")
                        Toast.show(message: "PDF File downloaded", controller: self)
                    } catch {
                        print("Pdf could not be saved")
                        Toast.show(message: "Pdf could not be saved", controller: self)
                    }
                }
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmit(_ sender: Any) {
        
        if strMonth != nil || strYear != nil {
            
            if selectedButtonTag == 1 {
                
                getPaymentHistoryAPI()
                
            } else {
                paymentDownloadAPI()
            }
        }
        
    }
    @IBAction func btnCheckbox(_ sender: UIButton) {
        
        selectedButtonTag = sender.tag
        if sender.tag == 1 {
            
            sender.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            btnPdf.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        } else {
            
            sender.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            btnView.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        }
    }
}
extension StatementVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrPaymentList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        
        //cell.lblQue.text = arrQue[indexPath.row]
        //cell.lblAns.text = arrAns[indexPath.row]
        cell.imgArrow.image = UIImage(named: arrImages[indexPath.row])
        
        if cell.imgArrow.image == UIImage(named: "dropdown") {
            
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        }
        cell.lblTransactionTitle.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblTransactionType.text = arrPaymentList?[indexPath.row].transaction_type
        cell.lblDate.text = arrPaymentList?[indexPath.row].datetime
        //cell.lblPaidUsing.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblOrderId.text = arrPaymentList?[indexPath.row].e_wallet_tran_code
        cell.lblDealName.text = "Deal Name: \(arrPaymentList?[indexPath.row].title ?? "")"
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (self.selectedIndex == indexPath.row && isExpanded == true) {
            
            return UITableView.automaticDimension
        } else {
            
            return 100
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrImages.count {
            
            arrImages.remove(at: i)
            arrImages.insert("dropdown", at: i)
        }
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndex == indexPath.row {
            
            if self.isExpanded == false {
                
                self.isExpanded = true
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("top", at: indexPath.row)
            } else {
                
                self.isExpanded = false
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("dropdown", at: indexPath.row)
            }
        } else {
            
            self.isExpanded = true
            
            self.selectedIndex = indexPath.row
            arrImages.remove(at: indexPath.row)
            arrImages.insert("top", at: indexPath.row)
        }
        
        //tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
    }
    
}
extension StatementVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfieldTag == 1 {
            return arrYear.count
        }
        if tappedTextfieldTag == 2 {
            return arrMonths.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfieldTag == 1 {
            return arrYear[row]
        }
        if tappedTextfieldTag == 2 {
            return arrMonths[row]["month"]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
    
}

extension StatementVC:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        tappedTextfieldTag = textField.tag
        picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
        picker?.reloadAllComponents()
    }
}
