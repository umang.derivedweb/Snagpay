//
//  MyPurchasesVC.swift
//  Snagpay
//
//  Created by Apple on 12/02/21.
//

import UIKit

class MyPurchasesVC: UIViewController {

    
    // MARK: - Variable
    var arrOrdersCurrent:[OrdersListData]?
    var arrOrdersCompleted:[OrdersListData]?
    var arrOrdersCancelled:[OrdersListData]?
    var strOrderType:String = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnCurrent: UIButton!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var btnCancelled: UIButton!
    @IBOutlet weak var btnCompleted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        btnCurrent.sendActions(for: .touchUpInside)
    }

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        
    }
    
    //MARK: - Webservice
    func getAllOrdersAPI(url:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetAllOrders(url: url, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                if self.strOrderType == OrderType.current.rawValue {
                    self.arrOrdersCurrent = response.data?.data
                }
                if self.strOrderType == OrderType.completed.rawValue {
                    self.arrOrdersCompleted = response.data?.data
                }
                if self.strOrderType == OrderType.cancelled.rawValue {
                    self.arrOrdersCancelled = response.data?.data
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnStackview(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            containerview.isHidden = true
            self.view.viewWithTag(101)?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.view.viewWithTag(102)?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        } else {
            
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
            self.view.viewWithTag(102)?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.view.viewWithTag(101)?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    @IBAction func btnCurrentCompletedCancelled(_ sender: UIButton) {
        
        if sender.tag == 1 {
            strOrderType = OrderType.current.rawValue
            sender.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnCompleted.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCompleted.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            btnCancelled.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCancelled.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            getAllOrdersAPI(url: URLs.current_order_history)
        }
        if sender.tag == 2 {
            strOrderType = OrderType.completed.rawValue
            sender.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnCurrent.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCurrent.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            btnCancelled.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCancelled.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            getAllOrdersAPI(url: URLs.completed_order_history)
        }
        if sender.tag == 3 {
            strOrderType = OrderType.cancelled.rawValue
            sender.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnCompleted.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCompleted.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            btnCurrent.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            btnCurrent.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            getAllOrdersAPI(url: URLs.canceled_order_history)
        }
    }
    
    @objc func btnProductDetails(sender:UIButton) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        if self.strOrderType == OrderType.current.rawValue {
            
            obj.strDealId = "\(arrOrdersCurrent?[sender.tag].deal_id ?? 0)"
        }
        if self.strOrderType == OrderType.completed.rawValue {
            
            obj.strDealId = "\(arrOrdersCompleted?[sender.tag].deal_id ?? 0)"
        }
        if self.strOrderType == OrderType.cancelled.rawValue {
            
            obj.strDealId = "\(arrOrdersCancelled?[sender.tag].deal_id ?? 0)"
        }
        navigationController?.pushViewController(obj, animated: true)
    }
    
}
extension MyPurchasesVC:UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.strOrderType == OrderType.current.rawValue {
            return arrOrdersCurrent?.count ?? 0
        }
        if self.strOrderType == OrderType.completed.rawValue {
            return arrOrdersCompleted?.count ?? 0
        }
        if self.strOrderType == OrderType.cancelled.rawValue {
            return arrOrdersCancelled?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell") as! WishlistCell
        
        cell.btnSelectProduct.tag = indexPath.row
        cell.btnSelectProduct.addTarget(self, action: #selector(btnProductDetails(sender:)), for: .touchUpInside)
        if self.strOrderType == OrderType.current.rawValue {
            cell.lblTitle.text = "\(arrOrdersCurrent?[indexPath.row].company_name ?? "")\n\(arrOrdersCurrent?[indexPath.row].title ?? "")"
            cell.lblDesc.text = "\(arrOrdersCurrent?[indexPath.row].bought ?? "")+ Bought"
            cell.lblPrice.text = "$ \(arrOrdersCurrent?[indexPath.row].sell_price ?? 0)"
            cell.img.sd_setImage(with: URL(string: arrOrdersCurrent?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            
        }
        if self.strOrderType == OrderType.completed.rawValue {
            cell.lblTitle.text = "\(arrOrdersCompleted?[indexPath.row].company_name ?? "")\n\(arrOrdersCompleted?[indexPath.row].title ?? "")"
            cell.lblDesc.text = "\(arrOrdersCompleted?[indexPath.row].bought ?? "")+ Bought"
            cell.lblPrice.text = "$ \(arrOrdersCompleted?[indexPath.row].sell_price ?? 0)"
            cell.img.sd_setImage(with: URL(string: arrOrdersCompleted?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if self.strOrderType == OrderType.cancelled.rawValue {
            cell.lblTitle.text = "\(arrOrdersCancelled?[indexPath.row].company_name ?? "")\n\(arrOrdersCancelled?[indexPath.row].title ?? "")"
            cell.lblDesc.text = "\(arrOrdersCancelled?[indexPath.row].bought ?? "")+ Bought"
            cell.lblPrice.text = "$ \(arrOrdersCancelled?[indexPath.row].sell_price ?? 0)"
            cell.img.sd_setImage(with: URL(string: arrOrdersCancelled?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
        
        if self.strOrderType == OrderType.current.rawValue {
            obj.intId = arrOrdersCurrent?[indexPath.row].e_gift_card_id
            obj.strOrderType = OrderType.current.rawValue
        }
        if self.strOrderType == OrderType.completed.rawValue {
            obj.intId = arrOrdersCompleted?[indexPath.row].e_gift_card_id
            obj.strOrderType = OrderType.completed.rawValue
        }
        if self.strOrderType == OrderType.cancelled.rawValue {
            obj.intId = arrOrdersCancelled?[indexPath.row].e_gift_card_id
            obj.strOrderType = OrderType.cancelled.rawValue
        }
         
        navigationController?.pushViewController(obj, animated: true)
    }
    
}
enum OrderType:String {
    case current
    case completed
    case cancelled
}
