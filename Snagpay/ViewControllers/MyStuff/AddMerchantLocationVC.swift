//
//  AddMerchantLocationVC.swift
//  Snagpay
//
//  Created by Apple on 09/02/22.
//

import UIKit
import TextFieldEffects

class AddMerchantLocationVC: UIViewController {
    
    // MARK: - Variables
    var arrStates:[StatesData]?
    var arrCities:[GetCitiesData]?
    var picker:UIPickerView?
    var strCityId:String?
    var strStateId:String?
    var intSelectedRow:Int!
    var tappedTextfieldTag:Int?
    var isEdit:Bool = false
    var dictEditAddress:LocationData?
    var isDefault:Int?
    var location_id:String?
    var strHeader:String?
    var latitude:String?
    var longitude:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtZip: HoshiTextField!
    @IBOutlet weak var txtCity: HoshiTextField!
    @IBOutlet weak var txtState: HoshiTextField!
    @IBOutlet weak var txtAddress: HoshiTextField!
    @IBOutlet weak var txtEntityName: HoshiTextField!
    @IBOutlet weak var txtEIN: HoshiTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getStatesAPI()
        
        toolBar()
    }
    

    // MARK: - Function
    func setupUI() {
        
        lblHeader.text = "Add Location"
        if isEdit == true {
            lblHeader.text = "Edit Location"
            txtEntityName.text = dictEditAddress?.entity_name
            txtEIN.text = dictEditAddress?.ein
            txtAddress.text = dictEditAddress?.address
            txtEmail.text = dictEditAddress?.email
            txtState.text = dictEditAddress?.state_name
            strStateId = "\(dictEditAddress?.state_id ?? 0)"
            txtCity.text = dictEditAddress?.city_name
            strCityId = "\(dictEditAddress?.city_id ?? 0)"
            txtPhone.text = dictEditAddress?.work_phone
            txtZip.text = dictEditAddress?.postcode
            latitude = "\(dictEditAddress?.latitude ?? 0.0)"
            longitude = "\(dictEditAddress?.longitude ?? 0.0)"
            location_id = "\(dictEditAddress?.user_location_id ?? 0)"
            getCitiesAPI(stateId:strStateId!)
        }
    }
    func toolBar() {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtState.inputAccessoryView = toolBar
        txtCity.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        if tappedTextfieldTag == 1 {
            
            strStateId = "\(arrStates?[intSelectedRow].state_id ?? 0)"
            txtState.text = arrStates?[intSelectedRow].state_code
            txtState.resignFirstResponder()
            getCitiesAPI(stateId: strStateId!)
        }
        if tappedTextfieldTag == 2 {
            strCityId = "\(arrCities?[intSelectedRow].city_id ?? 0)"
            txtCity.text = arrCities?[intSelectedRow].city_name
            latitude = "\(arrCities?[intSelectedRow].latitude ?? 0)"
            longitude = "\(arrCities?[intSelectedRow].longitude ?? 0)"
            txtCity.resignFirstResponder()
        }
        
    }
    @objc func cancelClick() {
        txtCity.resignFirstResponder()
    }
    
    // MARK: - Webservice
    
    func addAddressAPI() {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["entity_name"] = txtEmail.text!
        params["ein"] = txtEIN.text
        params["email"] = txtEmail.text
        params["country_id"] = "1"
        params["state_id"] = strStateId
        params["city_id"] = strCityId
        params["postcode"] = txtZip.text
        params["work_phone"] = txtPhone.text
        params["address"] = txtAddress.text
        params["latitude"] = latitude
        params["longitude"] = longitude
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.add_user_location, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.navigationController?.popViewController(animated: true)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func editAddressAPI() {
        Helper.shared.showHUD()
        
        var params:[String:String] = [:]
        
        params["user_location_id"] = location_id ?? ""
        params["entity_name"] = txtEmail.text!
        params["ein"] = txtEIN.text
        params["email"] = txtEmail.text
        params["country_id"] = "1"
        params["state_id"] = strStateId
        params["city_id"] = strCityId
        params["postcode"] = txtZip.text
        params["work_phone"] = txtPhone.text
        params["address"] = txtAddress.text
        params["latitude"] = latitude
        params["longitude"] = longitude
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.edit_user_location, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.navigationController?.popViewController(animated: true)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func getCitiesAPI(stateId:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=\(stateId)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.picker?.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func getStatesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetStates(url: "\(URLs.get_states)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrStates = response.data
                self.picker?.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        
        if txtEntityName.text == "" || txtEIN.text == "" || txtCity.text == "" || txtAddress.text == "" || txtState.text == "" || txtZip.text == "" || txtPhone.text == "" || txtEmail.text == "" {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if txtEmail.text?.isValidEmail() == false {
            
            Toast.show(message: Message.enterValidEmail, controller: self)
            return
        }
        if isEdit == true {
            editAddressAPI()
        } else {
            addAddressAPI()
        }
        
    }
    
}
extension AddMerchantLocationVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tappedTextfieldTag == 1 {
            return arrStates?.count ?? 0
        }
        if tappedTextfieldTag == 2 {
            return arrCities?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if tappedTextfieldTag == 1 {
            return arrStates?[row].state_code
        }
        if tappedTextfieldTag == 2 {
            return arrCities?[row].city_name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
    
}

extension AddMerchantLocationVC:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtState || textField == txtCity {
            return false
        }
        if textField == txtZip {
            
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXXXX", phone: newString)
            return false
        }
        if textField == txtPhone {
            
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XXX-XXX-XXXX", phone: newString)
            return false
        }
        
        if textField == txtEIN {
            
            guard let text = textField.text else { return false }
            let newString = (text as NSString).replacingCharacters(in: range, with: string)
            textField.text = Helper.shared.format(with: "XX-XXXXXXX", phone: newString)
            return false
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtState || textField == txtCity {
            
            tappedTextfieldTag = textField.tag
            picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
            picker?.reloadAllComponents()
            
        }
    }
}
