//
//  PaymentHistoryVC.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit

class PaymentHistoryVC: UIViewController {

    //MARK: Variables
    var arrPaymentList:[PaymentListData]?
    
    var arrImages:[String] = []


    var selectedIndex = -1

    // i.e initially no row is selected

    var isExpanded = false
    
    //MARK: IBOutlets
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var containerview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getRecentPaymentHistoryAPI()
    }
    
    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        table.estimatedRowHeight = 100
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
    }
    //MARK: - Webservice
    func getRecentPaymentHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetRecentPaymentHistory(url: URLs.recent_payment_history, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrPaymentList = response.data?.data
                self.arrImages.removeAll()
                for _ in 0..<(self.arrPaymentList?.count ?? 0) {
                    
                    self.arrImages.append("dropdown")
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }

    //MARK: IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStackview(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            sender.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            self.view.viewWithTag(2)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            (self.view.viewWithTag(2) as! UIButton).setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            containerview.isHidden = true
        } else {
            
            sender.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            self.view.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            (self.view.viewWithTag(1) as! UIButton).setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        }
    }
    

}

extension PaymentHistoryVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrPaymentList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        
        //cell.lblQue.text = arrQue[indexPath.row]
        //cell.lblAns.text = arrAns[indexPath.row]
        cell.imgArrow.image = UIImage(named: arrImages[indexPath.row])
        
        if cell.imgArrow.image == UIImage(named: "dropdown") {
            
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        }
        cell.lblTransactionTitle.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblTransactionType.text = arrPaymentList?[indexPath.row].business_name
        cell.lblDate.text = arrPaymentList?[indexPath.row].datetime
        //cell.lblPaidUsing.text = arrPaymentList?[indexPath.row].transaction_title
        cell.lblOrderId.text = arrPaymentList?[indexPath.row].e_wallet_tran_code
        cell.lblDealName.text = "Deal Name: \(arrPaymentList?[indexPath.row].title ?? "")"
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (self.selectedIndex == indexPath.row && isExpanded == true) {
            
            return UITableView.automaticDimension
        } else {
            
            return 100
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrImages.count {
            
            arrImages.remove(at: i)
            arrImages.insert("dropdown", at: i)
        }
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndex == indexPath.row {
            
            if self.isExpanded == false {
                
                self.isExpanded = true
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("top", at: indexPath.row)
            } else {
                
                self.isExpanded = false
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("dropdown", at: indexPath.row)
            }
        } else {
            
            self.isExpanded = true
            
            self.selectedIndex = indexPath.row
            arrImages.remove(at: indexPath.row)
            arrImages.insert("top", at: indexPath.row)
        }
        
        //tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
    }
    
}



