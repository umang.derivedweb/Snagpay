//
//  NotiSettingsVC.swift
//  Snagpay
//
//  Created by Apple on 19/02/21.
//

import UIKit

class NotiSettingsVC: UIViewController {

    
    //MARK: - Variable
    var strImportat_message_alert:String?
    var strShipments_notifications:String?
    var strPersonalised_notifications:String?
    var strWishlist_notifications:String?
    
    //MARK: - IBOutlet
    @IBOutlet weak var btnShipment: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnRecom: UIButton!
    @IBOutlet weak var btnWishlist: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getNotiSettingsAPI()
    }
    

    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Webservice
    func getNotiSettingsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetNotiSettings(url: "\(URLs.get_notification_settings)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.strImportat_message_alert = "\(response.data?.importat_message_alert ?? 0)"
                self.strShipments_notifications = "\(response.data?.shipments_notifications ?? 0)"
                self.strPersonalised_notifications = "\(response.data?.personalised_notifications ?? 0)"
                self.strWishlist_notifications = "\(response.data?.wishlist_notifications ?? 0)"
                
                self.btnAccount.accessibilityLabel = self.strImportat_message_alert
                self.btnShipment.accessibilityLabel = self.strShipments_notifications
                self.btnRecom.accessibilityLabel = self.strPersonalised_notifications
                self.btnWishlist.accessibilityLabel = self.strWishlist_notifications
                
                if self.strImportat_message_alert == "1" {
                    self.btnAccount.setImage(#imageLiteral(resourceName: "on"), for: .normal)
                    
                }
                if self.strShipments_notifications == "1" {
                    self.btnShipment.setImage(#imageLiteral(resourceName: "on"), for: .normal)
                }
                if self.strPersonalised_notifications == "1" {
                    self.btnRecom.setImage(#imageLiteral(resourceName: "on"), for: .normal)
                }
                if self.strWishlist_notifications == "1" {
                    self.btnWishlist.setImage(#imageLiteral(resourceName: "on"), for: .normal)
                }
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }

    func changeNotiSettingsAPI() {
        //Helper.shared.showHUD()
        
        let params = ["importat_message_alert":strImportat_message_alert!,
                      "shipments_notifications":strShipments_notifications!,
                      "personalised_notifications":strPersonalised_notifications!,
                      "wishlist_notifications":strWishlist_notifications!] as [String : Any]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.change_notification_settings, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func btnSwitch(_ sender: UIButton) {
        
        if sender.tag == 1 {
             
            if sender.accessibilityLabel == "0" {
                sender.accessibilityLabel = "1"
                sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            } else {
                sender.accessibilityLabel = "0"
                sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            }
            strImportat_message_alert = sender.accessibilityLabel
        }
        if sender.tag == 2 {
             
            if sender.accessibilityLabel == "0" {
                sender.accessibilityLabel = "1"
                sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            } else {
                sender.accessibilityLabel = "0"
                sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            }
            strShipments_notifications = sender.accessibilityLabel
        }
        if sender.tag == 3 {
             
            if sender.accessibilityLabel == "0" {
                sender.accessibilityLabel = "1"
                sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            } else {
                sender.accessibilityLabel = "0"
                sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            }
            strPersonalised_notifications = sender.accessibilityLabel
        }
        if sender.tag == 4 {
             
            if sender.accessibilityLabel == "0" {
                sender.accessibilityLabel = "1"
                sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            } else {
                sender.accessibilityLabel = "0"
                sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            }
            strWishlist_notifications = sender.accessibilityLabel
        }
        changeNotiSettingsAPI()
    }
    
    
}
