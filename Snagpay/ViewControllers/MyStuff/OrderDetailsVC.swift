//
//  OrderDetailsVC.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit

class OrderDetailsVC: UIViewController {

    // MARK: - Variables
    var intId:Int!
    var strDealId:String?
    var strOrderType:String!
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress3: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getOrderDetailsAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        if self.strOrderType == OrderType.completed.rawValue || self.strOrderType == OrderType.cancelled.rawValue {
            btnCancelOrder.setTitle("Re-order item", for: .normal)
        }
        
    }
    
    //MARK: - Webservice
    func getOrderDetailsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetOrderDetails(url: "\(URLs.order_details)\(intId ?? 0)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.strDealId = "\(response.data?.deal_id ?? 0)"
                self.lblName.text = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
                self.lblAddress1.text = "\(response.data?.address ?? "")"
                self.lblAddress2.text = "\(response.data?.city ?? ""), \(response.data?.state ?? "")"
                self.lblAddress3.text = "\(response.data?.country ?? ""), \(response.data?.postcode ?? "")"
                self.imgProduct.sd_setImage(with: URL(string: response.data?.deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.lblTitle.text = "\(response.data?.company_name ?? "")\n\(response.data?.title ?? "")"
                self.lblPrice.text = "$ \(response.data?.sell_price ?? 0)"
                self.lblAmount.text = "$ \(response.data?.amount ?? 0)"
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func cancelOrderAPI() {
        Helper.shared.showHUD()
        
        let params = ["e_gift_card_id":intId!] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.order_cancel, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.navigationController?.popViewController(animated: true)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    //MARK:- IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelOrder(_ sender: Any) {
        
        if self.strOrderType == OrderType.completed.rawValue || self.strOrderType == OrderType.cancelled.rawValue {
            
            let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
            obj.strDealId = strDealId
            navigationController?.pushViewController(obj, animated: true)
        } else {
            cancelOrderAPI()
        }
        
    }
    
    @IBAction func btnProductDetails(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = strDealId
        navigationController?.pushViewController(obj, animated: true)
    }
}
