//
//  DealsVC.swift
//  Snagpay
//
//  Created by Apple on 24/08/21.
//

import UIKit

class DealsVC: UIViewController {

    // MARK: - Variable
    var arrGiftCards:[GiftCardsData]?
    var arrAmount:[Int] = []
    var merchant_id = "1"
    
    var isFromScan = true
    var strType = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
        if isFromScan {
            
            if strType == "merchant" {
                giftCardsAPI()
            }
            if strType == "gift" {
                scanEGiftAPI()
            }
        } else {
            goodsServiceGiftCardsAPI()
        }
        
    }
    

    // MARK: - Function
    func setupUI() {
        self.hideKeyboardWhenTappedAround()
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = UITableView.automaticDimension
            table.estimatedRowHeight = 150
        } else {
            table.rowHeight = UITableView.automaticDimension
            table.estimatedRowHeight = 120
        }
        table.tableFooterView = UIView()
    }
    
    @objc func btnProceed(sender:UIButton) {
        
        guard let cell = sender.superview?.superview as? DealCell else {
            return // or fatalError() or whatever
        }

        if cell.txtAmountRemaining.text == "" {
            Toast.show(message: "Please enter amount", controller: self)
            return
        }
        
        var params:[String:Any] = [:]
        
        params["e_gift_card_ids[\(arrGiftCards?[sender.tag].e_gift_card_id ?? 0)]"] = arrAmount[sender.tag]
        
        if isFromScan {
            params["merchant_id"] = merchant_id
        } else {
            params["merchant_id"] = "\(arrGiftCards?[sender.tag].merchant_id ?? 0)"
        }
        
        
        print(params)
        redeemGiftcardsAPI(params: params)
    }
    
    // MARK: - Webservice
    func giftCardsAPI() {
        Helper.shared.showHUD()
        
        let params = ["merchant_id":merchant_id] as [String : String]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGetGiftCards(url: URLs.e_gift_cards, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrGiftCards = response.data
                self.arrAmount.removeAll()
                for i in 0..<(self.arrGiftCards?.count ?? 0) {
                    
                    self.arrAmount.append((self.arrGiftCards?[i].amount ?? 0)-(self.arrGiftCards?[i].redeemed_amount ?? 0))
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func scanEGiftAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGoodsServiceGiftCards(url: "\(URLs.scan_e_gift)?e_gift_card_id=\(merchant_id)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrGiftCards = response.data
                self.arrAmount.removeAll()
                for i in 0..<(self.arrGiftCards?.count ?? 0) {
                    
                    self.arrAmount.append((self.arrGiftCards?[i].amount ?? 0)-(self.arrGiftCards?[i].redeemed_amount ?? 0))
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func goodsServiceGiftCardsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallGoodsServiceGiftCards(url: URLs.goods_service_e_gift_cards, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrGiftCards = response.data
                self.arrAmount.removeAll()
                for i in 0..<(self.arrGiftCards?.count ?? 0) {
                    
                    self.arrAmount.append((self.arrGiftCards?[i].amount ?? 0)-(self.arrGiftCards?[i].redeemed_amount ?? 0))
                }
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func redeemGiftcardsAPI(params:[String:Any]) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.redeem_e_gift_card, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                if self.isFromScan {
                    
                    if self.strType == "merchant" {
                        
                        self.giftCardsAPI()
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
extension DealsVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGiftCards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealCell") as! DealCell
        cell.txtAmountRemaining.delegate = self
        cell.txtAmountRemaining.tag = indexPath.row
        cell.lblTitle.text = arrGiftCards?[indexPath.row].title
        cell.lblQty.text = "Quantity: \(arrGiftCards?[indexPath.row].qty ?? 0)"
        cell.lblRedeemedAmount.text = "Redeemed Amount: \(arrGiftCards?[indexPath.row].redeemed_amount ?? 0)"
        cell.lblAmount.text = "Amount: \(self.arrGiftCards?[indexPath.row].amount ?? 0)"
        cell.lblDate.text = "Date: \(self.arrGiftCards?[indexPath.row].created_at ?? "")"
        cell.lblBusinessName.text = "Business Name: \(self.arrGiftCards?[indexPath.row].business_name ?? "")"
        cell.txtAmountRemaining.text = "\(arrAmount[indexPath.row])"
        cell.btnProceed.tag = indexPath.row
        cell.btnProceed.addTarget(self, action: #selector(btnProceed(sender:)), for: .touchUpInside)
        cell.img.sd_setImage(with: URL(string: arrGiftCards?[indexPath.row].deal_image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GiftCardDetailsVC") as! GiftCardDetailsVC
        obj.intGiftId = arrGiftCards?[indexPath.row].e_gift_card_id
        navigationController?.pushViewController(obj, animated: true)
    }
    
}

extension DealsVC:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("DID END EDITING CALLED")
        if textField.text != "" {
            
            let str = textField.text!
            if Int(str)! < arrAmount[textField.tag] {
                
                arrAmount.remove(at: textField.tag)
                arrAmount.insert(Int(textField.text ?? "0")! , at: textField.tag)
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 0.5)
        return true
    }
    
    @objc func getHintsFromTextField(textField: UITextField) {
        print("Hints for textField: \(textField)")
        
        if textField.text != "" {
            
            let str = textField.text!
            if Int(str)! > arrAmount[textField.tag] {
                textField.resignFirstResponder()
                table.reloadData()
                Toast.show(message: "Entered amount is more than remaining redeem amount", controller: self)
            }
        }
        
    }
}
