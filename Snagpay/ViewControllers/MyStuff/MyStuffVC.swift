//
//  MyStuffVC.swift
//  Snagpay
//
//  Created by Apple on 12/02/21.
//

import UIKit
import AVFoundation

class MyStuffVC: UIViewController {

    // MARK: - Variable
    
    var arrSection1 = ["Shipping Addresses","Change Password","Notification Settings","Merchant Locations","Credit Card"]
    var arrSectionImg1 = ["shipping_address","change_pwd","notification_setting","pin","credit-card"]
    var arrSection2 = ["Company","SNAGpay Guide","Work with SNAGpay","FAQ","Terms and Conditions","Return Policies","e-Gifting","Report Infringement","Privacy Policies","About App"]
    var arrSectionImg2 = ["company","snagpay_guide","work_with_snagpay","faq","term_and_condition","return_policy","gifting","report_infringement","privacy","about_app"]
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblTotalCredits: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblNumCart: UILabel!
    @IBOutlet weak var lblAvailCredits: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        creditLineHistoryAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            table.rowHeight = 70
        } else {
            table.rowHeight = 60
        }
        
        table.tableFooterView = UIView()
        
        lblCredits.text = Helper.shared.strAvailableCredits
    }
    func closeScanner() {
        
        self.captureSession.stopRunning()
        self.previewLayer.removeFromSuperlayer()
        self.previewLayer = nil
        self.captureSession = nil
    }
    func openScanner() {
        
        //view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.viewWithTag(102)!.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.viewWithTag(102)!.layer.addSublayer(previewLayer)

        captureSession.startRunning()
        
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    func found(code: String) {
        view.viewWithTag(101)!.isHidden = true
        print(code)
        
        let myStringArr = code.components(separatedBy: "=")
        
        if myStringArr[0] == "merchant_location" {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealsLocationVC") as! DealsLocationVC
            obj.locationId = myStringArr[1]
            navigationController?.pushViewController(obj, animated: true)
        } else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
            obj.merchant_id = myStringArr[1]
            obj.strType = myStringArr[0]
            navigationController?.pushViewController(obj, animated: true)
        }
        
        
        
    }

    override var prefersStatusBarHidden: Bool {
        return false//true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: - Webservice
    
    func creditLineHistoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallCreditLineHistory(url: "\(URLs.credit_line_history)", parameters: [:], headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.lblTotalCredits.text = response.data?.credit_line
                self.lblAvailCredits.text = response.data?.available_credit_line
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnCart(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSihnOut(_ sender: Any) {
        
        Helper.shared.logoutFromApp()
    }
    
    @IBAction func btnChat(_ sender: Any) {
        
        let nav = UIStoryboard(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "navMessage") as! UINavigationController
        
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
        
    }
    @IBAction func btnDashboard(_ sender: Any) {
        let obj = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnBuyAgain(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyPurchasesVC") as! MyPurchasesVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnMyWallet(_ sender: Any) {
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
//        navigationController?.pushViewController(obj, animated: true)
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyMerchantVC") as! MyMerchantVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @IBAction func btnMyWishlist(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ManageWishlistVC") as! ManageWishlistVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnRedeem(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
        obj.isFromScan = false
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnScanQR(_ sender: Any) {
        view.viewWithTag(101)!.isHidden = false
        view.bringSubviewToFront(view.viewWithTag(101)!)
        openScanner()
    }
    
    @IBAction func btnMyQR(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyQRVC") as! MyQRVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnGiveGifts(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GiveGiftsVC") as! GiveGiftsVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnCloseScanner(_ sender: Any) {
        
        view.viewWithTag(101)!.isHidden = true
        closeScanner()
    }
    
    @IBAction func btnCreditLine(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CreditLineVC") as! CreditLineVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
    
    @IBAction func unwindMyStuffVC(segue: UIStoryboardSegue) {
    }
}
extension MyStuffVC:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSection1.count
        }
        return arrSection2.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyStuffCell") as! MyStuffCell
        if indexPath.section == 0 {
            cell.img.image = UIImage(named: arrSectionImg1[indexPath.row])
            cell.lblTitle.text = arrSection1[indexPath.row]
        } else {
            cell.img.image = UIImage(named: arrSectionImg2[indexPath.row])
            cell.lblTitle.text = arrSection2[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-100, height: headerView.frame.height-10)
        if section == 0 {
            label.text = "My Settings"
        } else if section == 1 {
            label.text = "About SNAGpay"
        }
        
        label.font = UIFont(name:"Roboto-Bold", size: 17.0) // my custom font
        label.textColor = UIColor.black // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0 {
//           return 0
//        }
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
                obj.isFromVC = "MyStuff"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 1 {
                
                let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                obj.isFrom = "MyStuff"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 2 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "NotiSettingsVC") as! NotiSettingsVC
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 3 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "MerchantLocationVC") as! MerchantLocationVC
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 4 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodVC") as! PaymentMethodVC
                navigationController?.pushViewController(obj, animated: true)
            }
        }
        
        if indexPath.section == 1 {
            
            if indexPath.row == 0 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "GeneralVC") as! GeneralVC
                obj.isFrom = fromVC.company.rawValue
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 1 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "GeneralVC") as! GeneralVC
                obj.isFrom = fromVC.guide.rawValue
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 2 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "GeneralVC") as! GeneralVC
                obj.isFrom = fromVC.work.rawValue
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 3 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/faq"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 4 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/terms-condition"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 5 {
                
                Toast.show(message: "Return policies are set by sellers. Buyers are encouraged to check return policies before making a purchase.", controller: self)
            }
            if indexPath.row == 6 {
                
                Toast.show(message: "SNAGpay is a great destination for gifts! When you buy gifts on SNAGpay, you save cash while attracting new customers to your business. Products and services purchased on SNAGpay may be gifted or transferred to another party unless otherwise specified in advance by the seller.", controller: self)
            }
            if indexPath.row == 7 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/report-infringement"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 8 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/policy"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 9 {
                
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
                navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
}

extension MyStuffVC:AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
    }
    
}

enum fromVC:String {
    case company = "Company"
    case guide = "SNAGpay Guide"
    case work = "Work with SNAGpay"
    case faq = "FAQ"
    case term = "Terms and Conditions"
    case returns = "Return and Policies"
    case gift = "Gifting"
    case report = "Report Infringement"
    case privacy = "Privacy Policies"
    case about = "About App"
}
