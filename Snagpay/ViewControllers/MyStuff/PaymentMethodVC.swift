//
//  PaymentMethodVC.swift
//  Snagpay
//
//  Created by Apple on 18/02/21.
//

import UIKit
import Stripe

class PaymentMethodVC: UIViewController {
    
    // MARK: - Variables
    var arrCards:[CardData]?
    var default_card_id:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        cardsAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 120
    }
    
    // MARK: - Webservice
    func cardsAPI() {
        
        Helper.shared.showHUD()
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetCards(url: URLs.stripe_get_credit_cards, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.default_card_id = response.data?.default_card_id
                self.arrCards = response.data?.data
                self.table.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteCardAPI(card_id:String) {
        
        Helper.shared.showHUD()
        
        let params:[String:String] = ["card_id": card_id]
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.stripe_delete_card, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.cardsAPI()
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func addCardAPI(token:String) {
        
        Helper.shared.showHUD()
        
        let params:[String:String] = ["stripe_token": token]
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.stripe_add_card, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.view.viewWithTag(1001)?.removeFromSuperview()
                self.cardsAPI()
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func makeDefaultCardAPI(card_id:String) {
        
        Helper.shared.showHUD()
        
        let params:[String:String] = ["card_id": card_id]
        
        let headers = ["Authorization": "Bearer \(Helper.shared.api_token)",
                       "Accept":"application/json"]
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.stripe_make_default_card, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.cardsAPI()
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnAddCard(_ sender: Any) {
        
        let customView = Bundle.main.loadNibNamed("CardInfoView", owner: self, options: nil)?.first as! CardInfoView
        customView.btnSubmit.addTarget(self, action: #selector(btnSubmitPayment), for: .touchUpInside)
        customView.txtExpiry.delegate = self
        customView.txtCardNumber.delegate = self
        customView.tag = 1001
        customView.frame = self.view.frame
        self.view.addSubview(customView)
    }
    @objc func btnSubmitPayment() {
        
        let customView = self.view.viewWithTag(1001) as! CardInfoView
        
        //card parameters
        let stripeCardParams = STPCardParams()
        stripeCardParams.number = (customView.txtCardNumber.text ?? "").replacingOccurrences(of: " ", with: "")
        let expiryParameters = customView.txtExpiry.text?.components(separatedBy: "/")
        stripeCardParams.expMonth = UInt(expiryParameters?.first ?? "0") ?? 0
        stripeCardParams.expYear = UInt(expiryParameters?.last ?? "0") ?? 0
        stripeCardParams.cvc = customView.txtCVV.text
        
        //converting into token
        let config = STPPaymentConfiguration.shared
        let stpApiClient = STPAPIClient.init(configuration: config)
        stpApiClient.createToken(withCard: stripeCardParams) { (token, error) in
            
            if error == nil {
                
                //Success
                DispatchQueue.main.async {
                    print(token!.tokenId)
                    self.addCardAPI(token: "\(token!.tokenId)")
                }
                
            } else {
                
                //failed
                print("Failed")
                Toast.show(message: Message.invalidCard, controller: self)
            }
        }
    }
    
    @objc func deleteCard(sender:UIButton) {
        
        deleteCardAPI(card_id:(arrCards?[sender.tag].id)!)
    }
    @objc func defaultCard(sender:UIButton) {
        
        makeDefaultCardAPI(card_id:(arrCards?[sender.tag].id)!)
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension PaymentMethodVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCards?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodCell") as! PaymentMethodCell
        cell.lblBrand.text = arrCards?[indexPath.row].brand
        cell.lblCardNumber.text = "************\(arrCards?[indexPath.row].last4 ?? "")"
        cell.lblExpiry.text = "\(arrCards?[indexPath.row].exp_month ?? 0)/\(arrCards?[indexPath.row].exp_year ?? 0)"
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDefault.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(deleteCard(sender:)), for: .touchUpInside)
        cell.btnDefault.addTarget(self, action: #selector(defaultCard(sender:)), for: .touchUpInside)
        
        if arrCards?[indexPath.row].id == default_card_id {
            cell.btnDefault.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            cell.btnDelete.setImage(nil, for: .normal)
        } else {
            cell.btnDefault.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            cell.btnDelete.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
}
extension PaymentMethodVC:UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == (self.view.viewWithTag(1001) as! CardInfoView).txtCardNumber
        {
            guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
            
            if currentText.count<20 {
                textField.text = currentText.grouping(every: 4, with: " ")
                return false
            } else {
                return false
            }
            
        }
        
        //////////////////
        
        if textField == (self.view.viewWithTag(1001) as! CardInfoView).txtExpiry {
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            
            if string == "" {
                if updatedText.count == 2 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
            } else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 {
                if updatedText <= "12" { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                }
                return false
            } else if updatedText.count == 5 {
                self.expDateValidation(dateStr: updatedText)
            } else if updatedText.count > 5 {
                return false
            }
            
            return true
        }
        return true
    }
    
    func expDateValidation(dateStr:String) {
        
        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)
        
        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user
        
        if enteredYear > currentYear {
            if (1 ... 12).contains(enteredMonth) {
                print("Entered Date Is Right")
            } else {
                print("Entered Date Is Wrong")
            }
        } else if currentYear == enteredYear {
            if enteredMonth >= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                    print("Entered Date Is Right")
                } else {
                    print("Entered Date Is Wrong")
                }
            } else {
                print("Entered Date Is Wrong")
            }
        } else {
            print("Entered Date Is Wrong")
        }
        
    }
}

