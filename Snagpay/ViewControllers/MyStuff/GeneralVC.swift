//
//  GeneralVC.swift
//  Snagpay
//
//  Created by Apple on 16/04/21.
//

import UIKit

class GeneralVC: UIViewController {

    // MARK: - Variable
    let arrCompany = ["About Snagpay","Press","Careers"]
    let arrSnagpayGuide = ["Food & Drink","Travel & Leisure","Event Tickets","Products & Services","Advertising & Marketing"]
    let arrWorkSnagpay = ["Join  the Snagpay Marketplace","How to Guide","Member Code of Conduct"]
    
    
    var isFrom:String!
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
 
    // MARK: - Function
    func setupUI() {
        
        lblHeader.text = isFrom
        table.rowHeight = 65
        table.tableFooterView = UIView()
        
        if isFrom != fromVC.work.rawValue {
            table.tableHeaderView = UIView()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    

}
extension GeneralVC:UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFrom == fromVC.company.rawValue {
            return arrCompany.count
        }
        if isFrom == fromVC.guide.rawValue {
            return arrSnagpayGuide.count
        }
        if isFrom == fromVC.work.rawValue {
            return arrWorkSnagpay.count
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralCell") as! GeneralCell
        
        if isFrom == fromVC.company.rawValue {
            cell.lblTitle.text = arrCompany[indexPath.row]
        }
        if isFrom == fromVC.guide.rawValue {
            cell.lblTitle.text = arrSnagpayGuide[indexPath.row]
        }
        if isFrom == fromVC.work.rawValue {
            cell.lblTitle.text = arrWorkSnagpay[indexPath.row]
        }
        
        
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFrom == fromVC.company.rawValue {
            
            if indexPath.row == 0 {
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/about"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 1 {
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/press"
                navigationController?.pushViewController(obj, animated: true)
            }
            if indexPath.row == 2 {
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/careers"
                navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        if isFrom == fromVC.work.rawValue {
            
            if indexPath.row == 0 {
                
            }
            if indexPath.row == 1 {
                Toast.show(message: "Video coming soon, join today for $25 credit.", controller: self)
            }
            if indexPath.row == 2 {
                let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                obj.strUrl = "https://snagpay.com/member-code-of-conduct"
                navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        
        
       
    }
}

