//
//  SelectCardVC.swift
//  Snagpay
//
//  Created by Apple on 19/02/21.
//

import UIKit

class SelectCardVC: UIViewController {

    
    //MARK: IBOutlets
    
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        table.estimatedRowHeight = 95
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
    }
    
    //MARK: IBAction
    
    @IBAction func btnPay(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MoneyAddSuccessVC") as! MoneyAddSuccessVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
}
extension SelectCardVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCardCell") as! SelectCardCell
        
        
        
        
        
        return cell
        
    }
    
    
    
}
