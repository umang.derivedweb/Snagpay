//
//  FilterVC.swift
//  Snagpay
//
//  Created by Apple on 10/02/21.
//

import UIKit

protocol FiterData {
    func sendData(sort:String,catId:String,StartPrice:String,endPrice:String, distance:String)
}

class FilterVC: UIViewController {
    
    // MARK: - Variables
    var sections = [ItemList]()
    
    var itemsSort: [ItemList] = [
        ItemList(name: "Sort", subName: "Price: Low to High", items: ["Price: Low to High", "Price: High to Low", "Rating: Low to High","Rating: High to Low"], itemsImg: [1,0,0,0], minus: true)
    ]
    var itemsFilter: [ItemList] = [
        ItemList(name: "Price", subName: "$ 0.0 - $ 50.0", items: ["$ 0.0 - $ 50.0", "$ 50.0 - $ 100.0","$ 100.0 - $ 150.0","$ 150.0 - $ 200.0"], itemsImg: [1,0,0,0]), ItemList(name: "Distance", subName: "Within 5 miles", items: ["Within 5 miles", "Within 10 miles","Within 15 miles","Within 20 miles","Within 25 miles"], itemsImg: [1,0,0,0,0])
    ]
    
    var arrSubCatData:[GetCategoryData]?
    
    var strSort = "price_low_to_high"
    var strCategoryId = ""
    var strPrice = "$ 0.0 - $ 50.0"
    var strDistance = "Within 5 miles"
    
    var arrTemp:[String] = []
    var arrTempImg:[Int] = []
    
    var delegate:FiterData?
    
    var isFilter:Bool = false
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    
    // MARK: - Function
    func setupUI() {
        
        
        table.tableFooterView = UIView()
        
        if isFilter {
            for i in 0..<(arrSubCatData?.count ?? 0) {
                
                arrTemp.append(arrSubCatData?[i].category_name ?? "")
                if i == 0 {
                    arrTempImg.append(1)
                    strCategoryId = "\(arrSubCatData?[i].category_id ?? 0)"
                } else {
                    arrTempImg.append(0)
                }
                
            }
            
            
            
            itemsFilter.insert(ItemList(name: "Category", subName: arrTemp[0], items: arrTemp, itemsImg: arrTempImg), at: 0)
            
            table.reloadData()
        } else {
            
            lblHeader.text = "Sort By"
        }
        
    }
    
    //MARK :- Structure
    struct ItemList {
        var name: String
        var subName: String
        var items: [String]
        var itemsImg: [Int]
        var minus: Bool
        
        init(name: String, subName:String, items: [String], itemsImg:[Int], minus: Bool = false) {
            self.name = name
            self.subName = subName
            self.items = items
            self.itemsImg = itemsImg
            self.minus = minus
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSave(_ sender: Any) {
        let arr = strPrice.components(separatedBy: " ")
        
        if strSort == "Price: Low to High" {
            
            strSort = "price_low_to_high"
        }
        if strSort == "Price: High to Low" {
            
            strSort = "price_high_to_low"
        }
        if strSort == "Rating: Low to High" {
            
            strSort = "prating_low_to_high"
        }
        if strSort == "Rating: High to Low" {
            
            strSort = "rating_high_to_low"
        }
        
        let dis = strDistance.filter("0123456789.".contains)

        delegate?.sendData(sort: strSort, catId: strCategoryId, StartPrice: arr[1], endPrice: arr[4], distance: dis)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReset(_ sender: Any) {
    }
    
}
extension FilterVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerHeading = UILabel(frame: CGRect(x: 5, y: 5, width: self.view.frame.width, height: 25))
        let subHeading = UILabel(frame: CGRect(x: 5, y: 30, width: self.view.frame.width, height: 25))
        headerHeading.font = UIFont(name:"Roboto-Regular",size:17)
        subHeading.font = UIFont(name:"Roboto-Regular",size:15)
        let bottomLine = UILabel(frame: CGRect(x: 10, y: 59, width: self.view.frame.width-20, height: 1))
        bottomLine.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        let imageView = UIImageView(frame: CGRect(x: self.view.frame.width - 30, y: 5, width: 20, height: 20))
        
        if isFilter {
            if itemsFilter[section].minus{
                imageView.image = UIImage(named: "arrow-up")
            }else{
                imageView.image = UIImage(named: "arrow-down")
            }
        } else {
            if itemsSort[section].minus{
                imageView.image = UIImage(named: "arrow-up")
            }else{
                imageView.image = UIImage(named: "arrow-down")
            }
        }
        
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(headerViewTapped))
        tapGuesture.numberOfTapsRequired = 1
        headerView.addGestureRecognizer(tapGuesture)
        headerView.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        headerView.tag = section
        
        if isFilter {
            headerHeading.text = itemsFilter[section].name
            subHeading.text = itemsFilter[section].subName
        } else {
            headerHeading.text = itemsSort[section].name
            subHeading.text = itemsSort[section].subName
        }
        
        
        headerHeading.textColor = .black
        subHeading.textColor = .lightGray
        headerView.addSubview(headerHeading)
        headerView.addSubview(subHeading)
        headerView.addSubview(bottomLine)
        headerView.addSubview(imageView)
        
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if isFilter {
            return itemsFilter.count
        } else {
            return itemsSort.count
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFilter {
            let itms = itemsFilter[section]
            return !itms.minus ? 0 : itms.items.count
        } else {
            let itms = itemsSort[section]
            return !itms.minus ? 0 : itms.items.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        
        if isFilter {
            cell.lblTitle.text = itemsFilter[indexPath.section].items[indexPath.row]
            
            if itemsFilter[indexPath.section].itemsImg[indexPath.row] == 1 {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            } else {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            }
        } else {
            cell.lblTitle.text = itemsSort[indexPath.section].items[indexPath.row]
            
            if itemsSort[indexPath.section].itemsImg[indexPath.row] == 1 {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            } else {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFilter {
            
            if indexPath.section == 0 {
                
                strCategoryId = "\(arrSubCatData?[indexPath.row].category_id ?? 0)"
                
                var dict = itemsFilter[indexPath.section]
                dict.subName = "\(arrSubCatData?[indexPath.row].category_name ?? "")"
                
                let index = (dict.itemsImg).firstIndex{$0 == 1}
                dict.itemsImg.remove(at: index!)
                dict.itemsImg.insert(0, at: index!)
                dict.itemsImg.remove(at: indexPath.row)
                dict.itemsImg.insert(1, at: indexPath.row)
                
                itemsFilter.remove(at: indexPath.section)
                itemsFilter.insert(dict, at: indexPath.section)
                table.reloadData()
            }
            if indexPath.section == 1 {
                
                strPrice = itemsFilter[indexPath.section].items[indexPath.row]
                
                var dict = itemsFilter[indexPath.section]
                dict.subName = strPrice
                
                let index = (dict.itemsImg).firstIndex{$0 == 1}
                dict.itemsImg.remove(at: index!)
                dict.itemsImg.insert(0, at: index!)
                dict.itemsImg.remove(at: indexPath.row)
                dict.itemsImg.insert(1, at: indexPath.row)
                
                itemsFilter.remove(at: indexPath.section)
                itemsFilter.insert(dict, at: indexPath.section)
                table.reloadData()
            }
            if indexPath.section == 2 {
                
                strDistance = itemsFilter[indexPath.section].items[indexPath.row]
                
                var dict = itemsFilter[indexPath.section]
                dict.subName = strDistance
                
                let index = (dict.itemsImg).firstIndex{$0 == 1}
                dict.itemsImg.remove(at: index!)
                dict.itemsImg.insert(0, at: index!)
                dict.itemsImg.remove(at: indexPath.row)
                dict.itemsImg.insert(1, at: indexPath.row)
                
                itemsFilter.remove(at: indexPath.section)
                itemsFilter.insert(dict, at: indexPath.section)
                table.reloadData()
            }
        } else {
            
            if indexPath.section == 0 {
                
                strSort = itemsSort[indexPath.section].items[indexPath.row]
                
                var dict = itemsSort[indexPath.section]
                dict.subName = strSort
                
                let index = (dict.itemsImg).firstIndex{$0 == 1}
                dict.itemsImg.remove(at: index!)
                dict.itemsImg.insert(0, at: index!)
                dict.itemsImg.remove(at: indexPath.row)
                dict.itemsImg.insert(1, at: indexPath.row)
                
                
                itemsSort.remove(at: indexPath.section)
                itemsSort.insert(dict, at: indexPath.section)
                
                table.reloadData()
            }
        }
        
        
        
    }
    @objc func headerViewTapped(tapped:UITapGestureRecognizer){
        print(tapped.view?.tag)
        
        if isFilter {
            
            if itemsFilter[tapped.view!.tag].minus == true{
                itemsFilter[tapped.view!.tag].minus = false
            }else{
                itemsFilter[tapped.view!.tag].minus = true
            }
            if let imView = tapped.view?.subviews[1] as? UIImageView{
                if imView.isKind(of: UIImageView.self){
                    if itemsFilter[tapped.view!.tag].minus{
                        imView.image = UIImage(named: "arrow-up")
                    }else{
                        imView.image = UIImage(named: "arrow-down")
                    }
                }
            }
        } else {
            
            if itemsSort[tapped.view!.tag].minus == true{
                itemsSort[tapped.view!.tag].minus = false
            }else{
                itemsSort[tapped.view!.tag].minus = true
            }
            if let imView = tapped.view?.subviews[1] as? UIImageView{
                if imView.isKind(of: UIImageView.self){
                    if itemsSort[tapped.view!.tag].minus{
                        imView.image = UIImage(named: "arrow-up")
                    }else{
                        imView.image = UIImage(named: "arrow-down")
                    }
                }
            }
        }
        
        table.reloadData()
    }
    
}
