//
//  CategoryVC.swift
//  Snagpay
//
//  Created by Apple on 04/02/21.
//

import UIKit

class CategoryVC: UIViewController {

    // MARK: - Variables
    var arrPopularCategories:[GetCategoryData]?
    var arrAllCategories:[AllCategoriesData]?
    var dictSelectedCat:AllCategoriesData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var collectionview: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategoryAPI()
        setTableHeight()
    }
    

    // MARK: - Function
    func setTableHeight() {
        DispatchQueue.main.async {
          // your code here
            self.heightTable.constant = self.table.contentSize.height
            self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
        }
        
    }
    //MARK: - Webservice
    func getCategoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetOnlyCategory(url: "\(URLs.categories)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrPopularCategories = response.data?.popular_categories
                self.arrAllCategories = response.data?.all_categories
                self.table.reloadData()
                self.collectionview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

}
extension CategoryVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPopularCategories?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.title.text = arrPopularCategories?[indexPath.row].category_name
        
        cell.img.sd_setImage(with: URL(string: arrPopularCategories?[indexPath.row].category_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.viewBg.backgroundColor = Helper.shared.hexStringToUIColor(hex: arrPopularCategories?[indexPath.row].backround_color ?? "")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let index = arrAllCategories?.firstIndex{$0.category_id == arrPopularCategories?[indexPath.row].category_id}
        
        if index != nil {
            
            dictSelectedCat = arrAllCategories?[index!]
            table.reloadData()
            setTableHeight()
        }

        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionview.frame.width/2)-5, height: (self.collectionview.frame.height/3)-5)
    }
}
extension CategoryVC:UITableViewDataSource, UITableViewDelegate {
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return arrAllCategories?.count ?? 0
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return arrAllCategories?[section].sub_categories?.count ?? 0
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
//
//        cell.lblTitle.text = arrAllCategories?[indexPath.section].sub_categories?[indexPath.row].category_name
//        return cell
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dictSelectedCat?.sub_categories?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        
        cell.lblTitle.text = dictSelectedCat?.sub_categories?[indexPath.row].category_name
        return cell
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
//        obj.strCatId = "\(arrAllCategories?[indexPath.section].sub_categories?[indexPath.row].category_id ?? 0)"
//        obj.strCategoryId = "\(arrAllCategories?[indexPath.section].category_id ?? 0)"
//        self.navigationController?.pushViewController(obj, animated: true)
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
        obj.strCatId = "\(dictSelectedCat?.sub_categories?[indexPath.row].category_id ?? 0)"
        obj.strCategoryId = "\(dictSelectedCat?.category_id ?? 0)"
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = dictSelectedCat?.category_name//arrAllCategories?[section].category_name
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            label.font = UIFont(name:"Roboto-Bold", size: 19.0) // my custom font
        } else {
            label.font = UIFont(name:"Roboto-Bold", size: 16.0) // my custom font
        }
        
        label.textColor = UIColor.black // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        setTableHeight()
    }
}

