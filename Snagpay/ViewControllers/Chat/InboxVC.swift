//
//  InboxVC.swift
//  Snagpay
//
//  Created by Apple on 11/03/22.
//

import UIKit
import Firebase

class InboxVC: UIViewController {
    
    // MARK: - Variables
    var arrUsers:[[String:Any]] = []
    var handleUsers:DatabaseHandle!
    var handleInboxChanged:DatabaseHandle!
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        tabBarController?.tabBar.isHidden = true
        getUsers()
        updateUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        if handleUsers != nil {
            Constants.refs.databaseUsers.removeObserver(withHandle: handleUsers)
            Constants.refs.databaseInbox.removeObserver(withHandle: handleInboxChanged)
        }
        
    }
    // MARK: - Function
    
    func setupUI() {
        table.rowHeight = 75
        table.tableFooterView = UIView()
        
    }
    
    func getUsers() {
        //Helper.shared.showHUD()
        arrUsers.removeAll()
        handleUsers = Constants.refs.databaseUsers.observe(.childAdded) { (snapshot) in
            
            print(snapshot.value)
            let dict = snapshot.value as! [String:Any]
            
            self.mergeUsersWithInbox(dict: dict)
            
        }
    }
    
    func mergeUsersWithInbox(dict:[String:Any]) {
        
        if (dict["id"] as! String) != (Auth.auth().currentUser?.uid ?? "") {
            
            var strQuery = ""
            if Int(dict["user_id"] as! String)! > Int(Helper.shared.userId)! {
                
                strQuery = "\(Helper.shared.userId)-\((dict["user_id"] as! String))"
            } else {
                
                strQuery = "\((dict["user_id"] as! String))-\(Helper.shared.userId)"
            }
            
            Constants.refs.databaseInbox.child(strQuery)
                .observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists(){
                        print(snapshot.value)
                        let dict2 = (snapshot.value as! [String:Any]).merging(dict ?? [:]){(current, _) in current}
                        self.arrUsers.append(dict2)
                        self.table.reloadData()
                    } else {
                        self.arrUsers.append(dict)
                        self.table.reloadData()
                    }
                    
                    //SVProgressHUD.dismiss()
                })
        }
    }
    
    func updateUI() {
        
        handleInboxChanged = Constants.refs.databaseInbox.observe(.childChanged) { (snapshot) in
            
            print(snapshot.value)
            
            let dict = snapshot.value as? [String:Any]
            self.getUsers()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}

extension InboxVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrUsers.count == 0 {
            table.isHidden = true
        } else {
            table.isHidden = false
        }
        return arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrUsers.count == 0 {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell") as! InboxCell
        cell.viewOnline.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.lblUsername.text = arrUsers[indexPath.row]["name"] as? String
        cell.lblMessage.text = arrUsers[indexPath.row]["last_message"] as? String
        
        if let time = (arrUsers[indexPath.row]["last_message_timestamp"] as? String) {
            cell.lblTime.text = Helper.shared.convertDateFormat(date: time)
        } else {
            cell.lblTime.text = ""
        }
        
        
        if arrUsers[indexPath.row]["online_status"] as? String == "offline" {
            cell.viewOnline.backgroundColor = .lightGray
        } else {
            cell.viewOnline.backgroundColor = UIColor(named: "App Green")
        }
        
        if arrUsers[indexPath.row]["unread_count_\(Helper.shared.userId)"] as? String == "0" {
            cell.lblMessageCount.text = ""
        } else {
            cell.lblMessageCount.text = arrUsers[indexPath.row]["unread_count_\(Helper.shared.userId)"] as? String
        }
        
        
        //        if (arrInbox[indexPath.row]["imageURL"] ?? "") as! String == "default" {
        //            cell.imgProfile.image = #imageLiteral(resourceName: "profile_gray")
        //        } else {
        //            cell.imgProfile.sd_setImage(with: URL(string: (arrInbox[indexPath.row]["imageURL"] ?? "") as! String), placeholderImage: UIImage(named: "placeholder.png"))
        //        }
        
       
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = arrUsers[indexPath.row]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        obj.strReceiverId = dict["user_id"] as! String
        obj.strReceiverUdId = dict["id"] as! String
        obj.strHeader = dict["name"] as! String
        navigationController?.pushViewController(obj, animated: true)
    }
}
