//
//  ChatVC.swift
//  Snagpay
//
//  Created by Apple on 11/03/22.
//

import UIKit
import Firebase

class ChatVC: UIViewController {

    // MARK: - Variables
    var arrMessages:[[String:Any]] = []
    var handleChat:DatabaseHandle!
    var handleInbox:DatabaseHandle!
    var strReceiverId = ""
    var strReceiverUdId = ""
    var strQuery = ""
    var strHeader = ""
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        getChatHistory()
        getTypingStatus()
        setUnreadCount()
    }
    override func viewWillAppear(_ animated: Bool) {
        //lblTitle.text = Helper.shared.user_name
        //navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1450980392, green: 0.1647058824, blue: 0.2156862745, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.isHidden = false
        tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        Constants.refs.databaseChat.child(strQuery).removeObserver(withHandle: handleChat)
        Constants.refs.databaseInbox.child(strQuery).child("is_typing").removeObserver(withHandle: handleChat)
    }
    
    // MARK: - Function
    
    func setupUI() {
        if Int(strReceiverId)! > Int(Helper.shared.userId)! {
            
             strQuery = "\(Helper.shared.userId)-\(strReceiverId)"
        } else {
            
            strQuery = "\(strReceiverId)-\(Helper.shared.userId)"
        }
        lblTitle.text = strHeader
    }
    
    
    func getChatHistory() {
        //Helper.shared.showHUD()
        self.arrMessages.removeAll()
        table.reloadData()
        
        
        handleChat = Constants.refs.databaseChat.child(strQuery).observe(.childAdded) { (snapshot) in
            
            //print(snapshot.value)
            var dict = snapshot.value as? [String:Any]
            
            
            if Helper.shared.userId != (dict?["sender_id"] as! String) {
                
                //dict?["is_read"] = 1
                
                Constants.refs.databaseChat.child(self.strQuery).child(snapshot.key).child("is_read").setValue(true)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.setUnreadCount()
                }
                
            }
            
            self.arrMessages.append(dict!)
            self.table.reloadData()
            self.scrollToBottom()
            //SVProgressHUD.dismiss()
        }
    }
    
    func getTypingStatus() {
        handleInbox = Constants.refs.databaseInbox.child(strQuery).child("is_typing").observe(.value) { (snapshot) in

            if snapshot.exists(){

                //print(snapshot.value)
                let str = snapshot.value as? String
                
                if str != "" {
                    let arr = str!.components(separatedBy: "-")
                    
                    if arr[0] != Helper.shared.userId {
                        self.lblSubTitle.text = "\(arr[1])"
                    } else {
                        self.lblSubTitle.text = ""
                    }
                    
                } else {
                    self.lblSubTitle.text = ""
                }

                

            }else{
                print("doesn't exist")
            }

        }
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
            self.table.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    
    //ScrollToBottom
    override func viewWillLayoutSubviews() {
        
        DispatchQueue.main.async {
            if self.arrMessages.count > 0 {
                let indexPath = IndexPath(row: self.arrMessages.count-1, section: 0)
                self.table.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
            
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnSend(_ sender: Any) {
        if self.txtMessage.text != "" &&  self.txtMessage.text != "Type a Message" {
            
            let refSender = Constants.refs.databaseChat.child(strQuery).childByAutoId()
            
            let message = ["sender_id": Helper.shared.userId,
                           "message_type":"text",
                           "text": txtMessage.text ?? "",
                           "image":"",
                           "timestamp": Helper.shared.getCurrentTime(date: Date()),
                           "is_read": false] as [String : Any]
            
            refSender.setValue(message)
            
            putEntryInInbox()
            
            
        }
    }
    
    func putEntryInInbox() {
        
        Constants.refs.databaseInbox.child(strQuery).child("unread_count_\(strReceiverId)").observeSingleEvent(of: .value) { [self] (snapshot) in
            
            if snapshot.exists(){
                print("exist")
                let snaps = snapshot.value as? String
                let count = Int(snaps!)!+1
                
                
                let refSender = Constants.refs.databaseInbox.child(strQuery)
                let message = ["last_message": self.txtMessage.text!, "last_message_timestamp":Helper.shared.getCurrentTime(date: Date()),
                               "unread_count_\(self.strReceiverId)":"\(count)",
                               "unread_count_\(Helper.shared.userId)":"0"] as [String : Any]
                refSender.setValue(message)
            } else {
                
                let refSender = Constants.refs.databaseInbox.child(strQuery)
                let message = ["last_message": self.txtMessage.text!, "last_message_timestamp":Helper.shared.getCurrentTime(date: Date()),
                               "unread_count_\(self.strReceiverId)":"1",
                               "unread_count_\(Helper.shared.userId)":"0"] as [String : Any]
                refSender.setValue(message)
            }
            self.txtMessage.text = ""
        }
        
        
    }
    
    func setUnreadCount() {
        
        Constants.refs.databaseInbox.child(strQuery).child("unread_count_\(Helper.shared.userId)").setValue("0")
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }

}

extension ChatVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        Constants.refs.databaseInbox.child(strQuery).child("is_typing").setValue("\(Helper.shared.userId)-\(Helper.shared.strUsername) is typing...")
        
        return true
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type a Message" {
            textView.text = nil
            textView.textColor = UIColor.white
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Type a Message"
            textView.textColor = UIColor.white
        }
        
        Constants.refs.databaseInbox.child(strQuery).child("is_typing").setValue("")
    }
}
extension ChatVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrMessages.count == 0 {
            return UITableViewCell()
        }
        let dict = arrMessages[indexPath.row]
        
        if dict["message_type"] as! String == "text" {
            if (dict["sender_id"] as! String) == Helper.shared.userId {
                
                if let msg = dict["text"] as? String {
                    
                    if msg.containsOnlyEmoji {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SenderEmojiCell") as! SenderEmojiCell
                        //print("Message:\(msg)")
                        cell.lblMessage.text = msg
                        
                        if (dict["is_read"] as? Bool) == true {
                         
                            cell.lblSeen.text = "Seen"
                        } else {
                            cell.lblSeen.text = "UnSeen"
                        }
                        
                        /*if (dict["isRead"] as? String) == "1" {
                         
                         cell.imgRead.image = #imageLiteral(resourceName: "seen")
                         } else {
                         cell.imgRead.image = #imageLiteral(resourceName: "unseen")
                         }*/
                        cell.lblName.text = Helper.shared.strUsername
                        
                        cell.lblTime.text = Helper.shared.convertDateFormat(date:dict["timestamp"] as? String ?? "")
                            
                        
                        return cell
                    } else {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SenderChatCell") as! SenderChatCell
                        print("Message:\(msg)")
                        
                        cell.lblMessage.text = msg
                        if (dict["is_read"] as? Bool) == true {
                         
                            cell.lblSeen.text = "Seen"
                        } else {
                            cell.lblSeen.text = "UnSeen"
                        }
                        /*if (dict["isRead"] as? String) == "1" {
                         
                         cell.imgRead.image = #imageLiteral(resourceName: "seen")
                         } else {
                         cell.imgRead.image = #imageLiteral(resourceName: "unseen")
                         }*/
                        cell.lblName.text = Helper.shared.strUsername
                        
                        cell.lblTime.text = Helper.shared.convertDateFormat(date:dict["timestamp"] as? String ?? "")
                        
                        return cell
                    }
                    
                    
                }
            } else {
                
                if let msg = dict["text"] as? String {
                    
                    if msg.containsOnlyEmoji {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverEmojiCell") as! ReceiverEmojiCell
                        
                        cell.lblMessage.text = msg
                        //cell.img.sd_setImage(with: URL(string: self.image!), placeholderImage: UIImage(named: "placeholder.png"))
                        cell.lblName.text = strHeader
                        
                        cell.lblTime.text = Helper.shared.convertDateFormat(date:dict["timestamp"] as? String ?? "")
                        
                        return cell
                    } else {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverChatCell") as! ReceiverChatCell
                        
                        cell.lblMessage.text = msg
                        //cell.img.sd_setImage(with: URL(string: self.image!), placeholderImage: UIImage(named: "placeholder.png"))
                        cell.lblName.text = strHeader
                        
                        
                        cell.lblTime.text = Helper.shared.convertDateFormat(date:dict["timestamp"] as? String ?? "")
                        
                        
                        return cell
                    }
                    
                    
                }
                
            }
        } else {
            
//            if (dict["senderId"] as! String) == Helper.shared.userId {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageCell") as! SenderImageCell
//                cell.lblName.text = dict["name"] as? String
//                cell.lblTime.text = dict["date"] as? String
//                cell.imgSender.sd_setImage(with: URL(string: (dict["pic_url"] ?? "") as! String), placeholderImage: UIImage(named: "placeholder.png"))
//                if (dict["is_seen"] as? Bool) == true {
//
//                    cell.lblSeen.text = "Seen"
//                } else {
//                    cell.lblSeen.text = "UnSeen"
//                }
//                return cell
//            } else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageCell") as! ReceiverImageCell
//                cell.lblName.text = dict["name"] as? String
//                cell.lblTime.text = dict["date"] as? String
//                cell.imgReceiver.sd_setImage(with: URL(string: (dict["pic_url"] ?? "") as! String), placeholderImage: UIImage(named: "placeholder.png"))
//                return cell
//            }
        }
        
        
        return UITableViewCell()
    }
    
}
