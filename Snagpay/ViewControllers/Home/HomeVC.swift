//
//  HomeVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit
import SDWebImage
import AVFoundation

class HomeVC: UIViewController {

    // MARK: - Variables
    var arrDeals:[DealsListData] = []
    var arrCategories:[GetCategoryData]?
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    var searchText = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var collviewDeals: UICollectionView!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblNumCart: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategoryAPI()
        getAvailableCreditsAPI()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        lblCity.text = Helper.shared.strCity
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("SetCity"), object: nil)

    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        
        lblCity.text = Helper.shared.strCity
    }

    func closeScanner() {
        
        self.captureSession.stopRunning()
        self.previewLayer.removeFromSuperlayer()
        self.previewLayer = nil
        self.captureSession = nil
    }
    func openScanner() {
        
        //view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.viewWithTag(102)!.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.viewWithTag(102)!.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func found(code: String) {
        
        view.viewWithTag(101)!.isHidden = true
        print(code)
        
        let myStringArr = code.components(separatedBy: "=")
        
        if myStringArr[0] == "merchant_location" {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealsLocationVC") as! DealsLocationVC
            obj.locationId = myStringArr[1]
            navigationController?.pushViewController(obj, animated: true)
        } else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DealsVC") as! DealsVC
            obj.merchant_id = myStringArr[1]
            obj.strType = myStringArr[0]
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }

    override var prefersStatusBarHidden: Bool {
        return false//true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    //MARK: - Webservice
    func getCategoryAPI() {
        //Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCategory(url: URLs.get_categories, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCategories = response.data?.categories
                self.collectionview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }
    func getAvailableCreditsAPI() { 
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetAvailableCredits(url: "\(URLs.credit_available_in_wallet)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Helper.shared.strAvailableCredits = response.data?.trade_credit ?? ""
                self.lblCredits.text = response.data?.trade_credit ?? ""
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func searchDealsAPI(search:String) {
        Helper.shared.showHUD()
        
        
        let params = ["search":search,
                      "city_id":Helper.shared.strCityId]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallSearchDeals(url: "\(URLs.search_deals)?page=\(currentPage)", parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.view.endEditing(true)
                
                if response.data?.flag == "deals" {
                    if response.data?.deals?.data?.count == 0 {
                        self.collectionview.isHidden = false
                        self.collviewDeals.isHidden = true
                        Toast.show(message: Message.noDealsAvailable, controller: self)
                    } else {
                        self.collectionview.isHidden = true
                        self.collviewDeals.isHidden = false
                        //self.view.bringSubviewToFront(self.collviewDeals)
                    }
                    
                    self.last_page = response.data?.deals?.last_page ?? 0
                    self.arrDeals += response.data?.deals?.data ?? []
                    
                    self.collviewDeals.reloadData()
                } else if response.data?.flag == "category" {
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
                    obj.strCategoryId = "\(response.data?.category_id ?? 0)"
                    
                    if let _ = response.data?.filter_category_id {
                        obj.strCatId = "\(response.data?.filter_category_id ?? 0)"
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    
    // MARK: - IBAction

    @IBAction func btnGotoWishlist(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Wishlist", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnGotoCart(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnScanQR(_ sender: Any) {
        view.viewWithTag(101)!.isHidden = false
        view.bringSubviewToFront(view.viewWithTag(101)!)
        
        openScanner()

    }
    
    @IBAction func btnCloseScanner(_ sender: Any) {
        
        view.viewWithTag(101)!.isHidden = true
        closeScanner()
    }
    
    
    @IBAction func btnSelectCity(_ sender: Any) {
        
        let customView = Bundle.main.loadNibNamed("SelectCityView", owner: self, options: nil)?.first as! SelectCityView
        customView.frame = view.frame
        view.addSubview(customView)
    }
    
    @IBAction func unwindToHomeVC(segue: UIStoryboardSegue) {
    }
}

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionview {
            return arrCategories?.count ?? 0
        } else {
            return arrDeals.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
            
            if arrCategories?[indexPath.row].category_name == "Travel" {
                
                cell.title.text = "\(arrCategories?[indexPath.row].category_name ?? "")\nComing soon"
            } else {
                cell.title.text = arrCategories?[indexPath.row].category_name
            }
            
            
            cell.img.sd_setImage(with: URL(string: arrCategories?[indexPath.row].category_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            cell.viewBg.backgroundColor = Helper.shared.hexStringToUIColor(hex: arrCategories?[indexPath.row].backround_color ?? "")
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.lblCompName.text = arrDeals[indexPath.row].company_name
            cell.lblTitle.text = arrDeals[indexPath.row].title
            cell.lblDistance.text = arrDeals[indexPath.row].city_name
            cell.lblPrice.text = "$ \(arrDeals[indexPath.row].sell_price ?? 0)"
            cell.lblBought.text = "\(arrDeals[indexPath.row].bought ?? "")+ bought"
            cell.lblRating.text = "(\(arrDeals[indexPath.row].total_rating ?? 0) rating)"
            cell.viewRating.rating = Double(arrDeals[indexPath.row].avg_rating ?? 0.0)
            cell.img.sd_setImage(with: URL(string: arrDeals[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionview {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryVC") as! SubCategoryVC
            obj.strCategoryId = "\(arrCategories?[indexPath.row].category_id ?? 0)"
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            
            let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
            obj.strDealId = "\(arrDeals[indexPath.row].deal_id ?? 0)"
            navigationController?.pushViewController(obj, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionview {
            return CGSize(width: (self.collectionview.frame.width/2)-5, height: (self.collectionview.frame.height/3)-5)
        } else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: (self.collectionview.frame.width/2)-5, height: 500)
            }
            return CGSize(width: (self.collectionview.frame.width/2)-5, height: 400)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == collviewDeals {
            
            if intCountArr != self.arrDeals.count {
                if (indexPath.row == (self.arrDeals.count) - 1 ) { //it's your last cell
                    if last_page != currentPage {
                        
                        //Load more data & reload your collection view
                        print("Last row")
                        currentPage = currentPage + 1
                        intCountArr = self.arrDeals.count
                        searchDealsAPI(search: searchText)
                    }
                    
                }
            }
        }
        
    }
}

extension HomeVC:AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        closeScanner()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
        
    }
    
}
extension HomeVC:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtSearch {
            
            searchText = textField.text ?? ""
            
            if searchText == "" {
                self.collectionview.isHidden = false
                self.collviewDeals.isHidden = true
                return
            }
            intCountArr = 0
            self.arrDeals.removeAll()
            currentPage = 1
            searchDealsAPI(search: textField.text ?? "")
        }
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        if textField == txtSearch {
//            
//            NSObject.cancelPreviousPerformRequests(
//                    withTarget: self,
//                    selector: #selector(self.getHintsFromTextField),
//                    object: textField)
//                self.perform(
//                    #selector(self.getHintsFromTextField),
//                    with: textField,
//                    afterDelay: 0.5)
//        }
//        return true
//    }
//    
//    @objc func getHintsFromTextField(textField: UITextField) {
//        print("Hints for textField: \(textField)")
//        searchText = textField.text ?? ""
//        
//        if searchText == "" {
//            self.collectionview.isHidden = false
//            self.collviewDeals.isHidden = true
//            return
//        }
//        intCountArr = 0
//        self.arrDeals.removeAll()
//        currentPage = 1
//        searchDealsAPI(search: textField.text ?? "")
//    }
}
