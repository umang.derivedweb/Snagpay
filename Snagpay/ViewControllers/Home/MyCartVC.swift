//
//  MyCartVC.swift
//  Snagpay
//
//  Created by Apple on 22/02/21.
//

import UIKit
import CoreData

class MyCartVC: UIViewController {

    // MARK: - Variables
    var arrCart:[Cart]?
    var arrQty:[String] = []
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblNoItems: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        arrCart = Helper.shared.fetchFromCoredata()
        for i in 0..<(arrCart?.count ?? 0 ){
            
            arrQty.append(arrCart?[i].qty ?? "1")
        }
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 206
    }
    @objc func btnPlus(sender:UIButton) {
        let qty = arrQty[sender.tag]
        arrQty.remove(at: sender.tag)
        arrQty.insert("\(Int(qty)!+1)", at: sender.tag)
        table.reloadData()
        
        let foundItems = arrCart?.filter { $0.deal_id == arrCart?[sender.tag].deal_id }
        
        if foundItems?.count ?? 0 > 0 {
            
            Helper.shared.updateInCoredata(dict: (foundItems?[0])!, qty:arrQty[sender.tag])
        }
    }
    
    @objc func btnMinus(sender:UIButton) {
        let qty = arrQty[sender.tag]
        
        if qty != "1" {
            arrQty.remove(at: sender.tag)
            arrQty.insert("\(Int(qty)!-1)", at: sender.tag)
            table.reloadData()
            
            let foundItems = arrCart?.filter { $0.deal_id == arrCart?[sender.tag].deal_id }
            
            if foundItems?.count ?? 0 > 0 {
                
                Helper.shared.updateInCoredata(dict: (foundItems?[0])!, qty:arrQty[sender.tag])
            }
        }
    }
    @objc func btnDelete(sender:UIButton) {
        let foundItems = arrCart?.filter { $0.deal_id == arrCart?[sender.tag].deal_id }
        
        if (foundItems?.count ?? 0) > 0 {
            
            Helper.shared.deleteFromCoredata(dict: (foundItems?[0])!)
        }
        arrCart = Helper.shared.fetchFromCoredata()
        arrQty.removeAll()
        for i in 0..<(arrCart?.count ?? 0 ){
            
            arrQty.append(arrCart?[i].qty ?? "1")
        }
        table.reloadData()
    }
    
    @objc func btnSaveForLater(sender:UIButton) {
        
        addWishlistAPI(dealId: arrCart?[sender.tag].deal_id ?? "")
    }
    //MARK: - Webservice
    func addWishlistAPI(dealId:String) {
        Helper.shared.showHUD()
        
        let params = ["deal_id":dealId] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.add_to_wishlist, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                let foundItems = self.arrCart?.filter { $0.deal_id == dealId }
                
                if foundItems?.count ?? 0 > 0 {
                    
                    Helper.shared.deleteFromCoredata(dict: (foundItems?[0])!)
                    self.arrCart = Helper.shared.fetchFromCoredata()
                    self.table.reloadData()
                }
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnCheckout(_ sender: Any) {
        
        if arrCart?.count == 0 {
            return
        }
        let obj = UIStoryboard(name:"Home", bundle:nil).instantiateViewController(withIdentifier: "ReviewOrderVC") as! ReviewOrderVC
        obj.isFrom = "MyStuff"
        Helper.shared.strCatId = ""
        navigationController?.pushViewController(obj, animated: true)
    }
}
extension MyCartVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrCart?.count == 0 {
            table.isHidden = true
        }
        return arrCart?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as! CartCell
        cell.img.sd_setImage(with: URL(string: arrCart?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.lblTitle.text = "\(arrCart?[indexPath.row].title ?? "")"
        cell.lblPrice.text = "$ \(arrCart?[indexPath.row].price ?? "")"
        cell.lblBought.text = "\(arrCart?[indexPath.row].bought ?? "")+Bought"
        cell.lblQty.text = arrQty[indexPath.row]
        cell.btnPlus.addTarget(self, action: #selector(btnPlus(sender:)), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action: #selector(btnMinus(sender:)), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(btnDelete(sender:)), for: .touchUpInside)
        cell.btnSave.addTarget(self, action: #selector(btnSaveForLater(sender:)), for: .touchUpInside)
        cell.btnMinus.tag = indexPath.row
        cell.btnPlus.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        cell.btnSave.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
}
