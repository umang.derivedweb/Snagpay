//
//  ReviewOrderVC.swift
//  Snagpay
//
//  Created by Apple on 04/02/21.
//

import UIKit

class ReviewOrderVC: UIViewController {

    // MARK: - Variable
    var arrOptionDeal:[[String:String]]!
    var tableHeight:CGFloat = 0.0
    var arrDeals:[OptionsDealsData]?
    var arrQty:[String] = []
    var dictResponse:ReviewOrderData?
    var strGiftCheckbox:String?
    var isFrom:String?
    var isUseCreditLineBalance = false
    
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var heightViewPromo: NSLayoutConstraint!
    @IBOutlet weak var btnTax: UIButton!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var lblBucks: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtPromo: UITextField!
    @IBOutlet weak var btnCheckboxGift: UIButton!
    @IBOutlet weak var table: ContentSizedTableView!
    @IBOutlet weak var lblCreditLine: UILabel!
    @IBOutlet weak var btnCheckboxCreditline: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var params:[String:String] = [:]
        
        if isFrom == "Home" {
            for i in 0..<arrOptionDeal.count {
                
                params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = arrOptionDeal[i]["qty"]
                
                arrQty.append(arrOptionDeal[i]["qty"] ?? "0")
            }
        }
        if isFrom == "MyStuff" {
            
           let arrCart = Helper.shared.fetchFromCoredata()
            
            for i in 0..<arrCart.count {
                
                params["deal_option_ids[\(arrCart[i].deal_option_id ?? "")]"] = arrCart[i].qty//arrQty[i]
                arrQty.append(arrCart[i].qty!)
            }
            
        }
        params["promo_code"] = txtPromo.text ?? ""
        reviewOrderAPI(params: params)
        
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        btnCheckboxGift.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        btnCheckboxGift.tag = 1
        strGiftCheckbox = "0"
    }
    
    // MARK: - Function
    func setupUI() {
        
        tableHeight = CGFloat(table.frame.height)
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 121
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
           // Code you want to be delayed
            self.heightTable?.constant = self.table.contentSize.height
        }
    }
    
    @objc func btnPlus(sender:UIButton) {
        let qty = arrQty[sender.tag]
        arrQty.remove(at: sender.tag)
        arrQty.insert("\(Int(qty)!+1)", at: sender.tag)
        table.reloadData()
        
        var params:[String:String] = [:]
        for i in 0..<arrOptionDeal.count {
            
            params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = arrQty[i]
        }
        params["promo_code"] = txtPromo.text ?? ""
        
        reviewOrderAPI(params: params)
    }
    
    @objc func btnMinus(sender:UIButton) {
        let qty = arrQty[sender.tag]
        
        if qty != "1" {
            arrQty.remove(at: sender.tag)
            arrQty.insert("\(Int(qty)!-1)", at: sender.tag)
            table.reloadData()
            var params:[String:String] = [:]
            for i in 0..<arrOptionDeal.count {
                
                params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = arrQty[i]
            }
            params["promo_code"] = txtPromo.text ?? ""
            reviewOrderAPI(params: params)
        }
    }
    
    func setParams() -> [String:String] {
        
        var params:[String:String] = [:]
        if isFrom == "Home" {
            for i in 0..<arrOptionDeal.count {
                
                params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = arrQty[i]
            }
        }
        if isFrom == "MyStuff" {
            
           let arrCart = Helper.shared.fetchFromCoredata()
            
            for i in 0..<arrCart.count {
                
                params["deal_option_ids[\(arrCart[i].deal_option_id ?? "")]"] = arrQty[i]
            }
        }
        
        params["shipping_address_id"] = ""
        params["total_price"] = "\(dictResponse?.total_price ?? 0)"
        params["discount"] = "\(dictResponse?.discount ?? 0)"
        params["snagpay_bucks"] = "\(dictResponse?.snagpay_bucks ?? 0)"
        params["give_as_a_gift"] = strGiftCheckbox ?? "0"
        
        return params
    }
    
    // MARK: - Webservice
    func reviewOrderAPI(params:[String:String]) {
        Helper.shared.showHUD()
        
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        print(params)
        NetworkManager.shared.webserviceCallReviewOrder(url: URLs.review_your_order, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrDeals = response.data?.deals
                
                
                self.arrQty.removeAll()
                for i in 0..<(self.arrDeals?.count ?? 0) {
                    self.arrQty.append(self.arrDeals?[i].qty ?? "")
                }
                
                self.dictResponse = response.data
                self.lblBucks.text = "$ \(response.data?.required_snagpay_bucks ?? 0)"
                self.lblPrice.text = "$ \(response.data?.total_price ?? 0)"
                
                if response.data?.use_credit_line_balance != 0 {
                    self.view.viewWithTag(201)?.isHidden = false
                    self.lblCreditLine.text = "\(response.data?.use_credit_line_balance ?? 0)"
                    self.isUseCreditLineBalance = true
                } else {
                    self.view.viewWithTag(201)?.isHidden = true
                    self.isUseCreditLineBalance = false
                }
                self.table.reloadData()
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func completePurchaseAPI(params:[String:String]) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCreateOrder(url: URLs.create_order, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "FullOrderDetailsVC") as! FullOrderDetailsVC
                    obj.intOrderId = response.data?.order_id
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func btnCheck(_ sender: Any) {
        
        if txtPromo.text == "" {
            return
        }
        
        var params:[String:String] = [:]
        
        if isFrom == "Home" {
            for i in 0..<arrOptionDeal.count {
                
                params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = arrQty[i]
            }
        }
        if isFrom == "MyStuff" {
            
           let arrCart = Helper.shared.fetchFromCoredata()
            
            for i in 0..<arrCart.count {
                
                params["deal_option_ids[\(arrCart[i].deal_option_id ?? "")]"] = arrQty[i]
            }
        }
        params["promo_code"] = txtPromo.text ?? ""
        reviewOrderAPI(params: params)
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAdd(_ sender: UIButton) {
        sender.isHidden = true
        if (UIUserInterfaceIdiom.phone.rawValue != 0) {
            heightViewPromo.constant = 110
        } else {
            heightViewPromo.constant = 120
        }
    }
    
    @IBAction func btnCheckboxGift(_ sender: UIButton) {
        
        if sender.tag == 1 {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            sender.tag = 2
            strGiftCheckbox = "1"
            
            let params = setParams()
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "GiveGiftVC") as! GiveGiftVC
            obj.dictparamsCompletePurchase = params
            obj.isFrom = isFrom
            navigationController?.pushViewController(obj, animated: true)
        } else {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 1
            strGiftCheckbox = "0"
        }
    }
    @IBAction func btnCheckboxTax(_ sender: UIButton) {
        if sender.tag == 1 {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            sender.tag = 2
        } else {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 1
        }
    }
    
    @IBAction func btnCheckboxCreditline(_ sender: UIButton) {
        
        if sender.tag == 1 {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            sender.tag = 2
        } else {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            sender.tag = 1
        }
    }
    
    @IBAction func btnCompletePurchase(_ sender: Any) {
        
        if btnTax.tag == 1 {
            Toast.show(message: Message.checkBox, controller: self)
            return
        }
        if isUseCreditLineBalance == true {
            
            if btnCheckboxCreditline.tag == 1 {
                Toast.show(message: Message.checkBoxCreditline, controller: self)
                return
            }
            
        }
        //completePurchaseAPI()
        let params = setParams()
        if Helper.shared.strCatId != "1" && Helper.shared.strCatId != "8" && Helper.shared.strCatId != "" {
            
            completePurchaseAPI(params: params)
        } else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
            obj.dictparamsCompletePurchase = params
            obj.isFrom = isFrom
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    

}
extension ReviewOrderVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrDeals?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewOrderCell") as! ReviewOrderCell
        cell.img.sd_setImage(with: URL(string: arrDeals?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.lblTitle.text = "\(arrDeals?[indexPath.row].title ?? "")"
        cell.lblPrice.text = "$ \(arrDeals?[indexPath.row].sell_price ?? 0)"
        
        
        if isFrom == "Home" {
            cell.lblQuantity.isHidden = true
            cell.stackview.isHidden = false
            cell.btnQty.text = arrQty[indexPath.row]
            cell.btnPlus.addTarget(self, action: #selector(btnPlus(sender:)), for: .touchUpInside)
            cell.btnMinus.addTarget(self, action: #selector(btnMinus(sender:)), for: .touchUpInside)
            cell.btnMinus.tag = indexPath.row
            cell.btnPlus.tag = indexPath.row
        }
        if isFrom == "MyStuff" {
            cell.lblQuantity.isHidden = false
            cell.stackview.isHidden = true
            cell.lblQuantity.text = "Quantity:- \(arrQty[indexPath.row])"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
