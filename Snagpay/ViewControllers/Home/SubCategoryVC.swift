//
//  SubCategoryVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit
import GoogleMaps
import CoreLocation

class SubCategoryVC: UIViewController, FiterData {
    

    // MARK: - Variables
    var arrDeals:[DealsListData] = []
    var strCategoryId:String!
    var arrSubCatData:[GetCategoryData]?
    
    var dictCat:CategoryDealsData?
    
    var strSort = ""
    var strCatId = ""
    var strStartPrice = ""
    var strEndPrice = ""
    var strDistance = ""
    
    var intCountArr = 0
    var currentPage = 1
    var last_page = 0
    
    var searchText = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var lblNumCart: UILabel!
    @IBOutlet weak var collviewDeals: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getSubCategoryAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
    }

    // MARK: - Function
    func setupUI() {
        //tabBarController?.tabBar.isHidden = true
        lblCredits.text = Helper.shared.strAvailableCredits
        lblCity.text = Helper.shared.strCity
        
        collectionview.register(UINib(nibName: "GoodsHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"GoodsHeaderView")
        collectionview.register(UINib(nibName: "ServicesHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"ServicesHeaderView")
        collectionview.register(UINib(nibName: "TicketsHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"TicketsHeaderView")
        collectionview.register(UINib(nibName: "FoodHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"FoodHeaderView")
        collectionview.register(UINib(nibName: "TravelHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:"TravelHeaderView")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)


    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print(notification.userInfo?["cat_id"] as! String)
        strCatId = notification.userInfo?["cat_id"] as! String
        
        arrDeals.removeAll()
        intCountArr = 0
        currentPage = 1
        
        getSubCategoryAPI()
    }

    func sendData(sort: String, catId: String, StartPrice: String, endPrice: String, distance:String) {
        
        strSort = sort
        strCatId = catId
        strStartPrice = StartPrice
        strEndPrice = endPrice
        strDistance = distance
        
        arrDeals.removeAll()
        intCountArr = 0
        currentPage = 1
        getSubCategoryAPI()
    }
    //MARK: - Webservice
    func getSubCategoryAPI() {
        //Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetCategoryDeals(url: "\(URLs.category_details)?category_id=\(strCategoryId!)&sort_by_deals=\(strSort)&filter_category_id=\(strCatId)&from_price_range=\(strStartPrice)&to_price_range=\(strEndPrice)&filter_distance=\(strDistance)&page=\(currentPage)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                if response.data?.deals?.data?.count == 0 {
                    Toast.show(message: Message.noDealsAvailable, controller: self)
                }
                
                self.last_page = response.data?.deals?.last_page ?? 0
                self.dictCat = response.data
                self.arrDeals += response.data?.deals?.data ?? []
                self.arrSubCatData = response.data?.sub_categories
                
                
                self.collectionview.reloadData()
                
                self.mapView.clear()
                var bounds = GMSCoordinateBounds()
                for i in 0..<(self.arrDeals.count) {
                    
                    let marker: GMSMarker = GMSMarker()
                    marker.accessibilityLabel = "\(i)"
                    marker.position = CLLocationCoordinate2D(latitude: CLLocationSpeed("\(self.arrDeals[i].latitude ?? 0.0)")! , longitude: CLLocationSpeed("\(self.arrDeals[i].longitude ?? 0.0)")!)
                    marker.appearAnimation = .pop
                    marker.map = self.mapView
                    
                    bounds = bounds.includingCoordinate(marker.position)
                }
                
                self.mapView.setMinZoom(1, maxZoom: 15)//prevent to over zoom on fit and animate if bounds be too small
                
                let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
                self.mapView.animate(with: update)
                
                self.mapView.setMinZoom(1, maxZoom: 20)
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }
    
    func searchDealsAPI(search:String) {
        Helper.shared.showHUD()
        
        let params = ["search":search,
                      "city_id":Helper.shared.strCityId]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallSearchDeals(url: "\(URLs.search_deals)?page=\(currentPage)", parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.view.endEditing(true)
                
                if response.data?.flag == "deals" {
                    if response.data?.deals?.data?.count == 0 {
                        self.collectionview.isHidden = false
                        self.collviewDeals.isHidden = true
                        self.btnMap.isHidden = false
                        Toast.show(message: Message.noDealsAvailable, controller: self)
                    } else {
                        self.collectionview.isHidden = true
                        self.collviewDeals.isHidden = false
                        self.btnMap.isHidden = true
                        //self.view.bringSubviewToFront(self.collviewDeals)
                    }
                    
                    self.last_page = response.data?.deals?.last_page ?? 0
                    self.arrDeals += response.data?.deals?.data ?? []
                    
                    self.collviewDeals.reloadData()
                } else if response.data?.flag == "category" {
                    self.collectionview.isHidden = false
                    self.collviewDeals.isHidden = true
                    self.btnMap.isHidden = false
                    
                    self.strCategoryId = "\(response.data?.category_id ?? 0)"
                    
                    if let _ = response.data?.filter_category_id {
                        self.strCatId = "\(response.data?.filter_category_id ?? 0)"
                    }
                    
                    self.arrDeals.removeAll()
                    self.intCountArr = 0
                    self.currentPage = 1
                    self.getSubCategoryAPI()
                }
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnSort(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Category", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.isFilter = false
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnFilter(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Category", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.arrSubCatData = arrSubCatData
        obj.isFilter = true
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMap(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            sender.tag = 2
            collectionview.isHidden = true
            sender.setImage(#imageLiteral(resourceName: "list_view"), for: .normal)
        } else {
            
            sender.tag = 1
            collectionview.isHidden = false
            sender.setImage(#imageLiteral(resourceName: "map"), for: .normal)
        }
    }
    @IBAction func btnGotoCart(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnGotoWishlist(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Wishlist", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
        navigationController?.pushViewController(obj, animated: true)
    }
}

extension SubCategoryVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDeals.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
        cell.lblCompName.text = arrDeals[indexPath.row].company_name
        cell.lblTitle.text = arrDeals[indexPath.row].title
        cell.lblDistance.text = arrDeals[indexPath.row].city_name
        cell.lblPrice.text = "$ \(arrDeals[indexPath.row].sell_price ?? 0)"
        cell.lblBought.text = "\(arrDeals[indexPath.row].bought ?? "")+ bought"
        cell.lblRating.text = "(\(arrDeals[indexPath.row].total_rating ?? 0) rating)"
        cell.viewRating.rating = Double(arrDeals[indexPath.row].avg_rating ?? 0.0)
        cell.img.sd_setImage(with: URL(string: arrDeals[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
        obj.strDealId = "\(arrDeals[indexPath.row].deal_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.collectionview.frame.width/2)-5, height: 500)
        }
        return CGSize(width: (self.collectionview.frame.width/2)-5, height: 400)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == collectionview {
            switch kind {
            
            
            case UICollectionView.elementKindSectionHeader:
                
                if strCategoryId == "1" || strCategoryId == "11" {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GoodsHeaderView", for: indexPath) as! GoodsHeaderView
                    if strCategoryId == "" {
                        headerView.lblTitle.text = "Goods"
                    } else if strCategoryId == "11" {
                        headerView.lblTitle.text = "Advertising and Marketing"
                    }
                    
                    headerView.arrSubCatData = self.arrSubCatData
                    headerView.tagListView.removeAllTags()
                    for i in 0..<(self.arrSubCatData?.count ?? 0) {
                        
                        headerView.tagListView.addTag((self.arrSubCatData?[i].category_name)!)
                    }
                    //headerView.backgroundColor = UIColor.blue
                    return headerView
                }
                if strCategoryId == "7" {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ServicesHeaderView", for: indexPath) as! ServicesHeaderView
                    
                    headerView.arrSubCatData = dictCat?.sub_categories
                    headerView.collview1.reloadData()
                    headerView.collview1.backgroundColor = UIColor.clear
                    headerView.collview2.backgroundColor = UIColor.clear
                    return headerView
                }
                if strCategoryId == "8" {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TicketsHeaderView", for: indexPath) as! TicketsHeaderView
                    
                    headerView.arrSubCatData = dictCat?.sub_categories
                    headerView.collview1.reloadData()
                    headerView.collview2.reloadData()
                    headerView.collview1.backgroundColor = UIColor.clear
                    return headerView
                }
                if strCategoryId == "9" {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TravelHeaderView", for: indexPath) as! TravelHeaderView
                    
                    headerView.arrCitiesData = dictCat?.popular_cities
                    
                    headerView.collview.reloadData()
                    
                    return headerView
                }
                if strCategoryId == "10" {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FoodHeaderView", for: indexPath) as! FoodHeaderView
                    
                    headerView.arrSubCatData = dictCat?.sub_categories
                    
                    headerView.collview.reloadData()
                    
                    return headerView
                }
                return UICollectionReusableView()
                
                
                
            default:
                assert(false, "Unexpected element kind")
                return UICollectionReusableView()
            }
        } else {
            
            return UICollectionReusableView()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView == collectionview {
            if strCategoryId == "1" || strCategoryId == "11" {
                return CGSize(width: collectionView.frame.width, height: 180.0)
            }
            if strCategoryId == "7" {
                return CGSize(width: collectionView.frame.width, height: 275)
            }
            if strCategoryId == "8" {
                return CGSize(width: collectionView.frame.width, height: 290)
            }
            if strCategoryId == "9" {
                return CGSize(width: collectionView.frame.width, height: 725)
            }
            if strCategoryId == "10" {
                return CGSize(width: collectionView.frame.width, height: 280)
                //return CGSize(width: collectionView.frame.width, height: 900)
                
            }
             return CGSize(width: collectionView.frame.width, height: 180.0)
        } else {
            
            return CGSize(width: 0, height: 0)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == collviewDeals {
            
            if intCountArr != self.arrDeals.count {
                if (indexPath.row == (self.arrDeals.count) - 1 ) { //it's your last cell
                    if last_page != currentPage {
                        
                        //Load more data & reload your collection view
                        print("Last row")
                        currentPage = currentPage + 1
                        intCountArr = self.arrDeals.count
                        searchDealsAPI(search: searchText)
                    }
                }
            }
        } else {
            if intCountArr != self.arrDeals.count {
                if (indexPath.row == (self.arrDeals.count) - 1 ) { //it's your last cell
                    if last_page != currentPage {
                        
                        //Load more data & reload your collection view
                        print("Last row")
                        currentPage = currentPage + 1
                        intCountArr = self.arrDeals.count
                        getSubCategoryAPI()
                    }
                    
                }
            }
        }
        
    }
    
}
extension SubCategoryVC:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        customInfoWindow.viewBg.shadowToView()
        customInfoWindow.lblTitle.text = arrDeals[index].title
        customInfoWindow.lblDistance.text = arrDeals[index].city_name
        customInfoWindow.lblPrice.text = "$ \(arrDeals[index].sell_price ?? 0)"
        customInfoWindow.lblBought.text = "\(arrDeals[index].bought ?? "")+ bought"
        customInfoWindow.lblRating.text = "(\(arrDeals[index].total_rating ?? 0) rating)"
        customInfoWindow.viewRating.rating = Double(arrDeals[index].avg_rating ?? 0.0)
        customInfoWindow.img.sd_setImage(with: URL(string: arrDeals[index].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))

        return customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
//        let index:Int! = Int(marker.accessibilityLabel!)
//
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RestDetailsVC") as! RestDetailsVC
//        obj.intRestId = arrSearchedRest?[index].restaurant_id
//
//
//        navigationController?.pushViewController(obj, animated: true)
    }
}
extension SubCategoryVC:UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtSearch {

            searchText = textField.text ?? ""

            if searchText == "" {
                self.collectionview.isHidden = false
                self.collviewDeals.isHidden = true
                self.btnMap.isHidden = false
                return
            }
            intCountArr = 0
            self.arrDeals.removeAll()
            currentPage = 1
            searchDealsAPI(search: textField.text ?? "")
        }
        
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == txtSearch {
//
//            NSObject.cancelPreviousPerformRequests(
//                    withTarget: self,
//                    selector: #selector(self.getHintsFromTextField),
//                    object: textField)
//                self.perform(
//                    #selector(self.getHintsFromTextField),
//                    with: textField,
//                    afterDelay: 0.5)
//        }
//        return true
//    }
//
//    @objc func getHintsFromTextField(textField: UITextField) {
//        print("Hints for textField: \(textField)")
//        searchText = textField.text ?? ""
//
//        if searchText == "" {
//            self.collectionview.isHidden = false
//            self.collviewDeals.isHidden = true
//            self.btnMap.isHidden = false
//            return
//        }
//        intCountArr = 0
//        self.arrDeals.removeAll()
//        currentPage = 1
//        searchDealsAPI(search: textField.text ?? "")
//    }
}
