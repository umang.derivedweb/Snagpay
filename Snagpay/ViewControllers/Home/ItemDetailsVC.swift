//
//  ItemDetailsVC.swift
//  Snagpay
//
//  Created by Apple on 03/02/21.
//

import UIKit
import Cosmos
import SDWebImage
import CoreData

class ItemDetailsVC: UIViewController {

    // MARK: - Variables
    var isSeeAllReviews = false
    var strDealId:String!
    var arrReviews:[ReviewsData]?
    var arrDealOptions:[DealOptionData]?
    var tableHeight:CGFloat = 0.0
    var strDealOptionId = ""
    var strDealOptionQty = "1"
    
    var dictDealDetails:DealsDetailsData?
    var arrCheckbox:[Int] = []
    
    // MARK: - IBOutlet
    @IBOutlet weak var lblDropdown: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var lblSortCust: UILabel!
    @IBOutlet weak var viewRatingCust: CosmosView!
    @IBOutlet weak var lblRatingCust: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblCustRatingText: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var lblRatingText: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnAddToWishlist: UIButton!
    @IBOutlet weak var lblHighlights: UILabel!
    @IBOutlet weak var tableDropdown: ContentSizedTableView!
    @IBOutlet weak var lblHighlights2: UILabel!
    @IBOutlet weak var btnWishlist: UIButton!
    @IBOutlet weak var lblHighlights3: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var lblNumCart: UILabel!
    @IBOutlet weak var btnWriteReview: UIButton!
    @IBOutlet weak var collview: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblCompName: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getDealDetailsAPI()
        tableHeight = CGFloat(tableDropdown.frame.height)
    }
    override func viewWillAppear(_ animated: Bool) {
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
    }
    override func viewWillLayoutSubviews() {
        
        
        if btnDropdown.tag == 1 {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            super.updateViewConstraints()
            if self.tableDropdown.contentSize.height < self.tableHeight {
                self.heightTable?.constant = self.tableDropdown.contentSize.height
            } else {
                self.heightTable?.constant = self.tableHeight
            }
        }
    }
    

    // MARK: - Function
    func setupUI() {
        self.heightTable?.constant = 0.0
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 110
        
        
        
        let arrCart = Helper.shared.fetchFromCoredata()
        
        let foundItems = arrCart.filter { $0.deal_id == strDealId }
        
        if foundItems.count > 0 {
            
            btnAddToCart.setTitle("Remove from cart", for: .normal)
        }
    }
    @objc func btnSelectDealCheckbox(sender:UIButton) {
        
        for i in 0..<arrCheckbox.count {
            arrCheckbox.remove(at: i)
            arrCheckbox.insert(0, at: i)
        }
        arrCheckbox.remove(at: sender.tag)
        arrCheckbox.insert(1, at: sender.tag)
        
        self.lblDropdown.text = self.arrDealOptions?[sender.tag].deal_option_name
        self.lblPrice.text = "$ \(self.arrDealOptions?[sender.tag].sell_price ?? 0)"
        self.strDealOptionId = "\(self.arrDealOptions?[sender.tag].deal_option_id ?? 0)"
        tableDropdown.reloadData()
    }
    
    @objc func btnAddReview() {
        
        if (view.viewWithTag(5001) as! AddReviewView).viewRating.rating == 0.0 {
            Toast.show(message: "Please give star rating", controller: self)
            return
        }
        if (view.viewWithTag(5001) as! AddReviewView).txtview.text == "" {
            Toast.show(message: "Please write your comment", controller: self)
            return
        }
        addReviewAPI()
    }
    
    
    //MARK: - Webservice
    func addRemoveWishlistAPI() {
        Helper.shared.showHUD()
        
        let params = ["deal_id":strDealId!] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.add_to_wishlist, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func addReviewAPI() {
        Helper.shared.showHUD()
        
        let params = ["deal_id":strDealId!,
                      "rating":"\((view.viewWithTag(5001) as! AddReviewView).viewRating.rating)",
                      "review":(view.viewWithTag(5001) as! AddReviewView).txtview.text!] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.add_deal_review, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.getDealDetailsAPI()
                self.view.viewWithTag(5001)?.removeFromSuperview()
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func getDealDetailsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetDealDetails(url: "\(URLs.deal_details)?deal_id=\(strDealId!)&sort_by_rating=MostRecent", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                self.dictDealDetails = response.data
                Helper.shared.strCatId = "\(response.data?.deal_details?.main_category_id ?? 0)"
                self.lblTitle.text = response.data?.deal_details?.title
                self.lblRatingText.text = "\(response.data?.deal_details?.avg_rating ?? 0.0)"
                self.lblRating.text = "\(response.data?.deal_details?.total_rating ?? 0) Ratings"
                self.lblCustRatingText.text = "\(response.data?.deal_details?.avg_rating ?? 0.0)"
                //self.lblPrice.text = "$ \(response.data?.deal_details?.sell_price ?? 0)"
                self.lblAbout.text = response.data?.deal_details?.deal_description
                self.lblCompName.text = response.data?.deal_details?.company_name
                self.lblHighlights.text = response.data?.deal_details?.description
                
                if let _ = response.data?.deal_details?.description_2 {
                    
                    self.lblHighlights2.text = response.data?.deal_details?.description_2
                } else {
                    self.lblHighlights2.text = "No details available"
                }
                if let _ = response.data?.deal_details?.description_3 {
                    
                    self.lblHighlights3.text = response.data?.deal_details?.description_3
                } else {
                    self.lblHighlights3.text = "No details available"
                }
                
                self.lblSortCust.text = "Sort By:Most Recent"
                self.lblRatingCust.text = "\(response.data?.deal_details?.total_rating ?? 0) ratings"
                self.img.sd_setImage(with: URL(string: response.data?.deal_details?.deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.viewRating.rating = Double(response.data?.deal_details?.avg_rating ?? 0.0)
                
                if response.data?.deal_details?.is_deal_purchased == 1 {
                    self.btnWriteReview.isHidden = false
                } else {
                    self.btnWriteReview.isHidden = true
                }
                
                if response.data?.deal_details?.is_wishlist == 0 {
                    self.btnAddToWishlist.setTitle("Add to Saved Items", for: .normal)
                    self.btnWishlist.setImage(UIImage(named: "heart"), for: .normal)
                    self.btnWishlist.tag = 201
                } else {
                    self.btnAddToWishlist.setTitle("Remove from Saved Items", for: .normal)
                    self.btnWishlist.setImage(#imageLiteral(resourceName: "fill_heart"), for: .normal)
                    self.btnWishlist.tag = 202
                }
                
                self.arrReviews = response.data?.customer_reviews
                self.arrDealOptions = response.data?.deal_options
                
                if (self.arrDealOptions?.count ?? 0) > 0 {
                    self.lblDropdown.text = self.arrDealOptions?[0].deal_option_name
                    self.lblPrice.text = "$ \(self.arrDealOptions?[0].sell_price ?? 0)"
                    self.strDealOptionId = "\(self.arrDealOptions?[0].deal_option_id ?? 0)"
                    //self.strDealOptionQty = "\(self.arrDealOptions?[0].qty ?? 0)"
                    self.arrCheckbox.removeAll()
                    for i in 0..<(self.arrDealOptions?.count ?? 0) {
                        if i == 0 {
                            self.arrCheckbox.append(1)
                        } else {
                            self.arrCheckbox.append(0)
                        }
                        
                    }
                } else {
                    self.strDealOptionId = "$ \(self.dictDealDetails?.deal_details?.sell_price ?? 0)"
                }
                self.table.reloadData()
                self.tableDropdown.reloadData()
                self.collview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDropdown(_ sender: UIButton) {
        
        if sender.tag == 1 {
            if self.tableDropdown.contentSize.height < tableHeight {
                self.heightTable?.constant = self.tableDropdown.contentSize.height
            } else {
                self.heightTable?.constant = tableHeight
            }
            sender.tag = 2
            sender.setImage(#imageLiteral(resourceName: "arrow-up"), for: .normal)
        } else {
            self.heightTable?.constant = 0.0
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "arrow-down"), for: .normal)
        }
        
    }
    @IBAction func btnBuyNow(_ sender: Any) {
        
        if Helper.shared.userId == "\(dictDealDetails?.deal_details?.user_id ?? 0)" {
            Toast.show(message: Message.yourDeal, controller: self)
            return
        }
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ReviewOrderVC") as! ReviewOrderVC
        
        let arrOptionDeal = [["id":strDealOptionId,"qty":strDealOptionQty]]
        obj.arrOptionDeal = arrOptionDeal
        obj.isFrom = "Home"
        navigationController?.pushViewController(obj, animated: true)
    }

    @IBAction func btnAddCart(_ sender: UIButton) {
        if Helper.shared.userId == "\(dictDealDetails?.deal_details?.user_id ?? 0)" {
            Toast.show(message: Message.yourDeal, controller: self)
            return
        }
        if sender.titleLabel?.text == "Add to cart" {
            
            Helper.shared.saveToCoredata(dict:dictDealDetails!, strDealOptionId: strDealOptionId)
            sender.setTitle("Remove from cart", for: .normal)
        } else {
            let arrCart = Helper.shared.fetchFromCoredata()
            let foundItems = arrCart.filter { $0.deal_id == strDealId }
            
            if foundItems.count > 0 {
                
                Helper.shared.deleteFromCoredata(dict: foundItems[0])
            }
            
            sender.setTitle("Add to cart", for: .normal)
        }
        lblNumCart.text = "\(Helper.shared.fetchFromCoredata().count)"
    }
    
    @IBAction func btnGiveGift(_ sender: Any) {
        
        if Helper.shared.userId == "\(dictDealDetails?.deal_details?.user_id ?? 0)" {
            Toast.show(message: Message.yourDeal, controller: self)
            return
        }
        
        var params:[String:String] = [:]
        
        let arrOptionDeal = [["id":strDealOptionId,"qty":strDealOptionQty]]
        
        for i in 0..<arrOptionDeal.count {
            
            params["deal_option_ids[\(arrOptionDeal[i]["id"] ?? "")]"] = "\(arrOptionDeal[i]["qty"] ?? "")"
        }

        params["shipping_address_id"] = ""
        params["total_price"] = "\(dictDealDetails?.deal_details?.total_price ?? 0)"
        params["discount"] = "\(dictDealDetails?.deal_details?.discount ?? 0)"
        params["snagpay_bucks"] = "\(dictDealDetails?.deal_details?.snagpay_bucks ?? 0)"
        params["give_as_a_gift"] = "1"
        print(params)
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "GiveGiftVC") as! GiveGiftVC
        obj.dictparamsCompletePurchase = params
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnWishlist(_ sender: UIButton) {
        if Helper.shared.userId == "\(dictDealDetails?.deal_details?.user_id ?? 0)" {
            Toast.show(message: Message.yourDeal, controller: self)
            return
        }
        if sender.tag == 201 || sender.titleLabel?.text == "Add to Saved Items" {
            
            (view.viewWithTag(101) as! UIButton).setTitle("Remove from Saved Items", for: .normal)
            (view.viewWithTag(201) as! UIButton).setImage(#imageLiteral(resourceName: "fill_heart"), for: .normal)
            (view.viewWithTag(201) as! UIButton).tag = 202
        } else {
            (view.viewWithTag(101) as! UIButton).setTitle("Add to Saved Items", for: .normal)
            (view.viewWithTag(202) as! UIButton).setImage(UIImage(named: "heart"), for: .normal)
            (view.viewWithTag(202) as! UIButton).tag = 201
        }
        addRemoveWishlistAPI()
    }
    @IBAction func btnShare(_ sender: Any) {
    }
    @IBAction func btnSeeAll(_ sender: Any) {
        btnSeeAll.isHidden = true
        isSeeAllReviews = true
        table.reloadData()
    }
    @IBAction func btnGotoWishlist(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Wishlist", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnGotoCart(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnShare(sender: AnyObject) {

        let myWebsite = NSURL(string:dictDealDetails?.deal_details?.share_url ?? "")

        guard let url = myWebsite else {
            print("nothing found")
            return
        }

        let shareItems:Array = [url]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)

    }
    
    @IBAction func btnWriteReview(_ sender: Any) {
        
        let customView = Bundle.main.loadNibNamed("AddReviewView", owner: self, options: nil)?.first as! AddReviewView
        customView.btnSubmitReview.addTarget(self, action: #selector(btnAddReview), for: .touchUpInside)
        customView.tag = 5001
        customView.frame = view.frame
        view.addSubview(customView)
    }
    
}

extension ItemDetailsVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == table {
            if isSeeAllReviews == true {
                return arrReviews?.count ?? 0
            } else {
                
                if (arrReviews?.count ?? 0) < 5 {
                    
                    return arrReviews?.count ?? 0
                } else {
                    return 3
                }
            }
            
        }
        return arrDealOptions?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == table {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as! ReviewCell
            //cell.img.sd_setImage(with: URL(string: arrReviews?[indexPath.row]., placeholderImage: UIImage(named: "placeholder.png"))
            cell.lblName.text = "\(arrReviews?[indexPath.row].first_name ?? "") \(arrReviews?[indexPath.row].last_name ?? "")"
            cell.lblDate.text = arrReviews?[indexPath.row].date
            cell.lblReview.text = arrReviews?[indexPath.row].review
            cell.viewRating.rating = Double(arrReviews?[indexPath.row].rating ?? 0.0)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
            cell.lblTitle.text = "\(arrDealOptions?[indexPath.row].deal_option_name ?? "")"
            if arrCheckbox[indexPath.row] == 1 {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            } else {
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            }
            cell.btnCheckbox.tag = indexPath.row
            cell.btnCheckbox.addTarget(self, action: #selector(btnSelectDealCheckbox(sender:)), for: .touchUpInside)
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableDropdown {
            self.strDealOptionId = "\(self.arrDealOptions?[indexPath.row].deal_option_id ?? 0)"
            //self.strDealOptionQty = "\(self.arrDealOptions?[indexPath.row].qty ?? 0)"
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tableDropdown {
            self.viewWillLayoutSubviews()
        }
        
    }
}
extension ItemDetailsVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        pageControl.numberOfPages = dictDealDetails?.images?.count ?? 0
        pageControl.isHidden = !((dictDealDetails?.images?.count ?? 0) > 1)
        return dictDealDetails?.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.img.sd_setImage(with: URL(string: dictDealDetails?.images?[indexPath.row].image_name ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if scrollView == collview {
            pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        
    }
    
}
