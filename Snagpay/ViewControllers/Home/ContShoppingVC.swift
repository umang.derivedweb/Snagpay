//
//  ContShoppingVC.swift
//  Snagpay
//
//  Created by Apple on 04/02/21.
//

import UIKit

class ContShoppingVC: UIViewController {

    @IBOutlet weak var btnContShopping: UIButton!
    @IBOutlet weak var lblCity: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    // MARK: - Function
    
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnContShopping.layer.cornerRadius = 25
        }
        
        lblCity.text = Helper.shared.strCity
    }
    
    

    // MARK: - IBAction
    @IBAction func btnContShopping(_ sender: Any) {
    }

}
