//
//  AddressListVC.swift
//  Snagpay
//
//  Created by Apple on 04/02/21.
//

import UIKit

class AddressListVC: UIViewController {

    // MARK: - Variables
    var arrAddresses:[ShippingAddressesData]?
    var isFromVC = ""
    var isFrom:String?
    var dictparamsCompletePurchase:[String:String]?
    
    // MARK: - IBOutlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        getAddressAPI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 150
        
        if isFromVC == "MyStuff" {
            
            heightConstraint.constant = 0
        }
        
    }
    
    @objc func btnEdit(sender:UIButton) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        if isFromVC == "MyStuff" {
            obj.isFromVC = "MyStuff"
            obj.strHeader = "Edit Shipping Address"
        }
        obj.isEdit = true
        obj.dictEditAddress = arrAddresses?[sender.tag]
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnRemove(sender:UIButton) {
        
        deleteAddressAPI(shippingId: "\(arrAddresses?[sender.tag].shipping_address_id ?? 0)")
    }
    @objc func btnSelectAsDefault(sender:UIButton) {
        sender.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        setDefaultAddressAPI(shippingId: "\(arrAddresses?[sender.tag].shipping_address_id ?? 0)")
    }
    
    
    func deleteAllFromCoreData() {
        let arrCart = Helper.shared.fetchFromCoredata()
        
        for i in 0..<arrCart.count {
            
            Helper.shared.deleteFromCoredata(dict: (arrCart[i]))
        }
    }
    
    
    // MARK: - Webservice
    func getAddressAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetShippingAddresses(url: "\(URLs.get_shipping_addresses)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrAddresses = response.data
                self.table?.reloadData()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func setDefaultAddressAPI(shippingId:String) {
        Helper.shared.showHUD()
        
        let params:[String:String] = ["shipping_address_id":shippingId]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.set_default_shipping_address, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.getAddressAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func deleteAddressAPI(shippingId:String) {
        Helper.shared.showHUD()
        
        let params:[String:String] = ["shipping_address_id":shippingId]
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.delete_shipping_address, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.getAddressAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func completePurchaseAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCreateOrder(url: URLs.create_order, parameters: dictparamsCompletePurchase!, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                if self.isFrom == "MyStuff" {
                    self.deleteAllFromCoreData()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "FullOrderDetailsVC") as! FullOrderDetailsVC
                    obj.intOrderId = response.data?.order_id
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSkip(_ sender: Any) {
        completePurchaseAPI()
    }
    @IBAction func btnAddAddress(_ sender: Any) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        if isFromVC == "MyStuff" {
            obj.isFromVC = "MyStuff"
            obj.strHeader = "Add Shipping Address"
        }
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnContinue(_ sender: Any) {
        
        if arrAddresses?.count == 0 {
            Toast.show(message: Message.addAddress, controller: self)
            return
        }
        if let obj = arrAddresses?.first(where: {$0.is_default == 1}) {
           // it exists, do something
            
            dictparamsCompletePurchase?["shipping_address_id"] = "\(obj.shipping_address_id ?? 0)"
        } else {
           //item could not be found
            Toast.show(message: Message.selectAddress, controller: self)
            return
        }

        //print(dictparamsCompletePurchase)
        completePurchaseAPI()
    }
}

extension AddressListVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrAddresses?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressCell
        cell.btnCheck.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        cell.btnRemove.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEdit(sender:)), for: .touchUpInside)
        cell.btnRemove.addTarget(self, action: #selector(btnRemove(sender:)), for: .touchUpInside)
        cell.btnCheck.addTarget(self, action: #selector(btnSelectAsDefault(sender:)), for: .touchUpInside)
        cell.lblTitle.text = "\(arrAddresses?[indexPath.row].address ?? ""), \(arrAddresses?[indexPath.row].street ?? ""), \(arrAddresses?[indexPath.row].city_name ?? ""), \(arrAddresses?[indexPath.row].state_name ?? ""), \(arrAddresses?[indexPath.row].zip_code ?? ""), \(arrAddresses?[indexPath.row].country_name ?? "")"
        cell.lblMobile.text = arrAddresses?[indexPath.row].phone
        
        if arrAddresses?[indexPath.row].is_default == 1 {
            cell.btnCheck.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        } else {
            cell.btnCheck.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        }
        return cell
    }
}

