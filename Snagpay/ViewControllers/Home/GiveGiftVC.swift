//
//  GiveGiftVC.swift
//  Snagpay
//
//  Created by Apple on 26/02/21.
//

import UIKit
import TextFieldEffects

class GiveGiftVC: UIViewController {

    // MARK: - Variables
    var strGiftType = GiftType.email.rawValue
    var dictparamsCompletePurchase:[String:String]?
    let datePicker = UIDatePicker()
    var isFrom:String?
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnText: UIButton!
    @IBOutlet weak var txtRecName: HoshiTextField!
    @IBOutlet weak var txtRecEmail: HoshiTextField!
    @IBOutlet weak var txtFrom: HoshiTextField!
    @IBOutlet weak var txtPhone: HoshiTextField!
    @IBOutlet weak var txtMessage: HoshiTextField!
    @IBOutlet weak var txtSendGift: HoshiTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        showDatePicker()
    }
    
    
    // MARK: - Function
    func setupUI() {
        
        
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }

       //ToolBar
       let toolbar = UIToolbar();
       toolbar.sizeToFit()
       let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
      let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

     toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

      txtSendGift.inputAccessoryView = toolbar
      txtSendGift.inputView = datePicker

     }

      @objc func donedatePicker(){

       let formatter = DateFormatter()
       formatter.dateFormat = "MMM dd,yyyy"
       txtSendGift.text = formatter.string(from: datePicker.date)
       self.view.endEditing(true)
     }

     @objc func cancelDatePicker(){
        self.view.endEditing(true)
      }
    
    func deleteAllFromCoreData() {
        let arrCart = Helper.shared.fetchFromCoredata()
        
        for i in 0..<arrCart.count {
            
            Helper.shared.deleteFromCoredata(dict: (arrCart[i]))
        }
    }
    
    // MARK: - Webservice
    func completePurchaseAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCreateOrder(url: URLs.create_order, parameters: dictparamsCompletePurchase!, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                if self.isFrom == "MyStuff" {
                    self.deleteAllFromCoreData()
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    let obj = UIStoryboard(name: "MyStuff", bundle: nil).instantiateViewController(withIdentifier: "FullOrderDetailsVC") as! FullOrderDetailsVC
                    obj.intOrderId = response.data?.order_id
                    self.navigationController?.pushViewController(obj, animated: true)
                }
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEmailGifts(_ sender: Any) {
        
        txtPhone.isHidden = true
        txtRecName.isHidden = false
        txtRecEmail.isHidden = false
        txtFrom.isHidden = false
        
        btnEmail.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        btnText.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        strGiftType = GiftType.email.rawValue
    }
    @IBAction func btnTextGifts(_ sender: Any) {
        txtPhone.isHidden = false
        txtRecName.isHidden = true
        txtRecEmail.isHidden = true
        txtFrom.isHidden = true
        
        btnText.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
        btnEmail.setImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        strGiftType = GiftType.text.rawValue
    }
    
    @IBAction func btnCompletePurchase(_ sender: Any) {
        
        if strGiftType == GiftType.email.rawValue {
            
            if txtRecEmail.text == "" || txtRecName.text == "" || txtFrom.text == "" || txtMessage.text == "" || txtSendGift.text == "" {
                Toast.show(message: Message.reqFields, controller: self)
                return
            }
            
            dictparamsCompletePurchase?["recipient_email"] = txtRecEmail.text
            dictparamsCompletePurchase?["recipient_name"] = txtRecName.text
            dictparamsCompletePurchase?["from_name"] = txtFrom.text
            dictparamsCompletePurchase?["message"] = txtMessage.text
            dictparamsCompletePurchase?["send_gift_date"] = txtSendGift.text
            dictparamsCompletePurchase?["gift_type"] = GiftType.email.rawValue
        }
        if strGiftType == GiftType.text.rawValue {
            
            if txtPhone.text == "" || txtMessage.text == "" || txtSendGift.text == "" {
                Toast.show(message: Message.reqFields, controller: self)
                return
            }
            
            
            dictparamsCompletePurchase?["phone_number"] = txtPhone.text
            dictparamsCompletePurchase?["message"] = txtMessage.text
            dictparamsCompletePurchase?["send_gift_date"] = txtSendGift.text
            dictparamsCompletePurchase?["gift_type"] = GiftType.text.rawValue
        }
        print(dictparamsCompletePurchase)
        completePurchaseAPI()
        
    }
}
extension GiveGiftVC:UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField != txtPhone {
//            return false
//        }
//        if let text = textField.text, let textRange = Range(range, in: text) {
//            let updatedText = text.replacingCharacters(in: textRange, with: string)
//            if textField.text?.count == 3 && updatedText.count == 6 {
//                textField.text = textField.text! + "-" + string
//                return false
//            }
//            if textField.text?.count == 5 && updatedText.count == 4 {
//                let text = textField.text!
//                textField.text = String(text.prefix(3))
//                return false
//            }
//        }
//        return true
//    }
}

enum GiftType:String {
    case email
    case text
}
