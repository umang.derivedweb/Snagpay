//
//  SelectCityVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class SelectCityVC: UIViewController {

    // MARK: - Variables
    var arrCities:[GetCitiesData]?
    var picker:UIPickerView?
    var intSelectedRow:Int!
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnUseLocation: UIButton!
    @IBOutlet weak var txtCity: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnUseLocation.layer.cornerRadius = 30
        }
        getCitiesAPI()
        picker = Helper.shared.myPickerView(vc: self, txtfield: txtCity)
        toolBar()
        
        if UserDefaults.standard.value(forKey: "isLogin") != nil && UserDefaults.standard.value(forKey: "isLogin") as! Bool == true {
            Helper.shared.userId = UserDefaults.standard.value(forKey: "userId") as! String
            Helper.shared.api_token = UserDefaults.standard.value(forKey: "api_token") as! String
            Helper.shared.email = UserDefaults.standard.value(forKey: "email") as! String
            Helper.shared.strCity = UserDefaults.standard.value(forKey: "city") as! String
            Helper.shared.strCityId = UserDefaults.standard.value(forKey: "cityId") as! String
            Helper.shared.strBusinessName = UserDefaults.standard.value(forKey: "businessName") as! String
            Helper.shared.strUsername = UserDefaults.standard.value(forKey: "UserName") as! String
            let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
            self.navigationController?.pushViewController(obj, animated: false)
        }
    }
    
    func toolBar() {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtCity.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            return
        }
        
        txtCity.text = arrCities?[intSelectedRow].city_name
        txtCity.resignFirstResponder()
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        obj.strCityId = "\(arrCities?[intSelectedRow].city_id ?? 0)"
        navigationController?.pushViewController(obj, animated: true)
    }
    @objc func cancelClick() {
        txtCity.resignFirstResponder()
    }
    
    //MARK: - Webservice
    func getCitiesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCities(url: "\(URLs.getCities)?state_id=3", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCities = response.data
                self.picker?.reloadAllComponents()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnUseLocation(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
}

extension SelectCityVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return arrCities?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        return arrCities?[row].city_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
}

extension SelectCityVC:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}
