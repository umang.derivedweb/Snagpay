//
//  ForgotPasswordVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    // MARK: - Variables
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnSubmit.layer.cornerRadius = 30
        }
    }
    
    //MARK: - Webservice
    
    func forgotPasswordAPI() {
        Helper.shared.showHUD()
        let params = ["email":txtEmail.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.forgot_password, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOtpVC") as! VerifyOtpVC
                obj.strEmail = self.txtEmail.text
                obj.strMsg = response.ResponseMsg
                self.navigationController?.pushViewController(obj, animated: true)
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction

    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmit(_ sender: Any) {
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        forgotPasswordAPI()
    }
}

