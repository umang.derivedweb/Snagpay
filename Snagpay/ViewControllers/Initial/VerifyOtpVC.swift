//
//  VerifyOtpVC.swift
//  Snagpay
//
//  Created by Apple on 10/03/21.
//

import UIKit

class VerifyOtpVC: UIViewController {
    
    // MARK: Variables
    
    var strEmail:String!
    var strMsg:String!
    
    
    // MARK: IBOutlet
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtFourth: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    //MARK: - Function
    
    func setupUI() {
        txtFirst.background = #imageLiteral(resourceName: "box_grey")
        txtSecond.background = #imageLiteral(resourceName: "box_grey")
        txtThird.background = #imageLiteral(resourceName: "box_grey")
        txtFourth.background = #imageLiteral(resourceName: "box_grey")
        Toast.show(message: strMsg!, controller: self)
        txtFirst.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtSecond.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtThird.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtFourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
    }
    @objc func textFieldDidChange(textField: UITextField){

        let text = textField.text

        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtFirst:
                if txtSecond.text == "" {
                    txtSecond.becomeFirstResponder()
                    
                } else {
                    txtFirst.resignFirstResponder()
                }
                txtFirst.background = #imageLiteral(resourceName: "box_white")
                
            case txtSecond:
                if txtThird.text == "" {
                    txtThird.becomeFirstResponder()
                    
                } else {
                    txtSecond.resignFirstResponder()
                }
                txtSecond.background = #imageLiteral(resourceName: "box_white")
                
            case txtThird:
                if txtFourth.text == "" {
                    txtFourth.becomeFirstResponder()
                    
                } else {
                    txtThird.resignFirstResponder()
                }
                txtThird.background = #imageLiteral(resourceName: "box_white")
                
            case txtFourth:
                
                txtFourth.resignFirstResponder()
                txtFourth.background = #imageLiteral(resourceName: "box_white")
            default:
                break
            }
        }else{
            
            
        }
    }

    //MARK: - Webservice
    func verifyOtpAPI() {
        Helper.shared.showHUD()
        
        let enteredOtp = "\(txtFirst.text ?? "")\(txtSecond.text ?? "")\(txtThird.text ?? "")\(txtFourth.text ?? "")"
        
        
        let params = ["email":strEmail!,
                      "otp":enteredOtp] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.check_otp, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                obj.strEmail = self.strEmail
                self.navigationController?.pushViewController(obj, animated: true)
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    //MARK: - IBAction
    @IBAction func btnSubmit(_ sender: Any) {
        
        verifyOtpAPI()
    }
    @IBAction func btnBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
}
extension VerifyOtpVC:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        textField.background = #imageLiteral(resourceName: "box_grey")
    }
}
