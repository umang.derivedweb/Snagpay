//
//  ChangePasswordVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class ChangePasswordVC: UIViewController {

    // MARK: - Variables
    var strEmail:String!
    var isFrom = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var heightTxtOldPassword: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnChange.layer.cornerRadius = 30
        }
        
        if isFrom == "MyStuff" {
            heightTxtOldPassword.constant = 50
        }
    }
    //MARK: - Webservice
    func newPasswordAPI() {
        Helper.shared.showHUD()
        
        let params = ["email":strEmail!,
                      "new_password":txtNewPassword.text!,
                      "confirm_password":txtConfPassword.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.new_password, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.performSegue(withIdentifier: "unwindLogin", sender: self)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func changePasswordAPI() {
        Helper.shared.showHUD()
        
        let params = ["email":Helper.shared.email,
                      "old_password":txtOldPassword.text!,
                      "new_password":txtNewPassword.text!,
                      "confirm_password":txtConfPassword.text!] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        print(params)
        NetworkManager.shared.webserviceCallCommon(url: URLs.change_password, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnChange(_ sender: Any) {
        if isFrom == "MyStuff" {
            if txtOldPassword.text?.isEmpty ?? true {
                Toast.show(message: Message.enterPassword, controller: self)
                return
            }
        }
        if txtNewPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if txtConfPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.confirmPassword, controller: self)
            return
        }
        if txtConfPassword.text != txtNewPassword.text {
            Toast.show(message: Message.passwordUnmatched, controller: self)
            return
        }
        
        if isFrom == "MyStuff" {
            changePasswordAPI()
        } else {
            newPasswordAPI()
        }
        
    }
    
    @IBAction func btnOldPassword(_ sender: UIButton) {
        
        if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
            sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
            sender.image(for: .normal)
            txtOldPassword.isSecureTextEntry = false
        } else {
            sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
            txtOldPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnNewPassword(_ sender: UIButton) {
        
        if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
            sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
            sender.image(for: .normal)
            txtNewPassword.isSecureTextEntry = false
        } else {
            sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
            txtNewPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnConfPassword(_ sender: UIButton) {
        
        if sender.image(for: .normal) != #imageLiteral(resourceName: "invisible") {
            sender.setImage(#imageLiteral(resourceName: "invisible"), for: .normal)
            sender.image(for: .normal)
            txtConfPassword.isSecureTextEntry = false
        } else {
            sender.setImage(#imageLiteral(resourceName: "visible"), for: .normal)
            txtConfPassword.isSecureTextEntry = true
        }
    }
}

