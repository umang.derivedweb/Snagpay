//
//  LoginVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit
import CoreLocation
import GoogleSignIn
import FBSDKLoginKit
import FirebaseAuth

class LoginVC: UIViewController {

    // MARK: - Variables
    var arrCategories:[GetCategoryData]?
    var picker:UIPickerView?
    var txtfieldTag = 0
    var txtfield:UITextField?
    var strCatType:String?
    var strCostGoods:String?
    var strLocations:String?
    var strHowlong:String?
    var intSelectedRow:Int!
    var lat:String = ""
    var long:String = ""
    var strPassword:String!
    var strCountry:String!
    var strCityId:String = ""
    
    var arrCostGoods:[String] = []
    let arrLocations = ["One location","Two location","Three location","Five or more location"]
    let arrHowlong = ["One year","Two years","Three years","Five or more years"]
    let arrCommon = ["1","2","3","+5"]
    
    var isRemember:Bool = false
    
    let locationManager = CLLocationManager()
    
    // MARK: - IBOutlet
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let seconds = 0.2
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            // Put your code which should be executed with a delay here
            self.btnSignIn.sendActions(for: .touchUpInside)
        }
        arrCostGoods.removeAll()
        for i in 0..<100 {
            
            arrCostGoods.append("\(i+1)%")
        }
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        
    }
    func getFBUserData(){
            if((AccessToken.current) != nil){
                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        print(result!)
                        let dict = result as! [String:Any]
                        
                        let pictureDict = dict["picture"] as? [String:Any]
                        
                        let dataDict = pictureDict?["data"] as? [String:Any]
                        let strPictureURL = dataDict?["url"] ?? ""
                        print(strPictureURL)
                        
                        var dict1:[String:Any] = [:]
                        dict1["first_name"] = (dict["first_name"] ?? "")
                        dict1["last_name"] = (dict["last_name"] ?? "")
                        dict1["email"] = (dict["email"] ?? "")
                        dict1["facebook_id"] = (dict["id"] ?? "")
                        dict1["city_id"] = self.strCityId
                        
                        self.socialLoginAPI(url:URLs.login_with_facebook, params: dict1)
                        
                    }
                })
            }
        }

    func toolBar() {
        
        // ToolBar
        let toolBar = UIToolbar()
        //toolBar.barStyle = .default
        //toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtfield?.inputAccessoryView = toolBar
    }
    @objc func doneClick() {
        if intSelectedRow == nil {
            view.endEditing(true)
            return
        }
        if txtfieldTag == 9 {
            txtfield?.text = arrCategories?[intSelectedRow].category_name
            strCatType = "\(arrCategories?[intSelectedRow].category_id ?? 0)"
        }
        if txtfieldTag == 10 {
            txtfield?.text = arrCostGoods[intSelectedRow]
            strCostGoods = arrCostGoods[intSelectedRow].replacingOccurrences(of: "%", with: "")
        }
        if txtfieldTag == 12 {
            txtfield?.text = arrLocations[intSelectedRow]
            strLocations = arrCommon[intSelectedRow]
        }
        if txtfieldTag == 13 {
            txtfield?.text = arrHowlong[intSelectedRow]
            strHowlong = arrCommon[intSelectedRow]
        }
        view.endEditing(true)
    }
    @objc func cancelClick() {
        view.endEditing(true)
    }
    @objc func btnSignInAction() {
        if (self.view.viewWithTag(2001) as! SignInView).txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !((self.view.viewWithTag(2001) as! SignInView).txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if (self.view.viewWithTag(2001) as! SignInView).txtPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        loginAPI()
    }
    @objc func btnPreSignUpAction() {
        
        if (self.view.viewWithTag(2001) as! PreSignUpView).txtFName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterName, controller: self)
            return
        }
        if (self.view.viewWithTag(2001) as! PreSignUpView).txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !((self.view.viewWithTag(2001) as! PreSignUpView).txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.enterValidEmail, controller: self)
                return
            }
        }
        if (self.view.viewWithTag(2001) as! PreSignUpView).txtPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.enterPassword, controller: self)
            return
        }
        if (self.view.viewWithTag(2001) as! PreSignUpView).txtConfPassword.text?.isEmpty ?? true {
            Toast.show(message: Message.confirmPassword, controller: self)
            return
        }
        if (self.view.viewWithTag(2001) as! PreSignUpView).txtConfPassword.text != (self.view.viewWithTag(2001) as! PreSignUpView).txtPassword.text {
            Toast.show(message: Message.passwordUnmatched, controller: self)
            return
        }
        if (self.view.viewWithTag(2001) as! PreSignUpView).btnCheck.tag == 1 {
            Toast.show(message: Message.tickBox, controller: self)
            return
        }
        
        getCategoryAPI()
        
        let customView = Bundle.main.loadNibNamed("SignUpView", owner: self, options: nil)?.first as! SignUpView
        customView.btnSignUp.addTarget(self, action: #selector(btnSignUpAction), for: .touchUpInside)
        customView.tag = 3001
        strPassword = (self.view.viewWithTag(2001) as! PreSignUpView).txtPassword.text
        customView.txtName.text = (self.view.viewWithTag(2001) as! PreSignUpView).txtFName.text
        customView.txtEmail.text = (self.view.viewWithTag(2001) as! PreSignUpView).txtEmail.text
        customView.txtTypeBusiness.delegate = self
        customView.txtCostGoods.delegate = self
        customView.txtLocations.delegate = self
        customView.txtHowlong.delegate = self
        customView.txtZip.delegate = self
        
        customView.frame = self.viewBg.frame
        self.view.addSubview(customView)
        
    }
    @objc func btnSignUpAction() {
        
        if (self.view.viewWithTag(3001) as! SignUpView).txtPhone.text?.isEmpty ?? true {
            Toast.show(message: Message.enterNumber, controller: self)
            return
        }
        if !((self.view.viewWithTag(3001) as! SignUpView).txtPhone.text)!.validPhoneNumber {
            Toast.show(message: Message.enterValidNumber, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtBusinessName.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtAddress.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtZip.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if strCatType == nil {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if strLocations == nil {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if strHowlong == nil {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtWebsite.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtSales.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        if (self.view.viewWithTag(3001) as! SignUpView).txtCostGoods.text?.isEmpty ?? true {
            Toast.show(message: Message.reqFields, controller: self)
            return
        }
        registerAPI()
        
    }
    @objc func btnForgot() {
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @objc func btnRemember(sender:UIButton) {
        
        if sender.tag == 1 {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            isRemember = true
            sender.tag = 2
        } else {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            isRemember = false
            sender.tag = 1
        }
    }
    @objc func btnFb() {
        
        let fbLoginManager : LoginManager = LoginManager()
          fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
          if (error == nil){
              let fbloginresult : LoginManagerLoginResult = result!
            // if user cancel the login
            if (result?.isCancelled)!{
                    return
            }
            if(fbloginresult.grantedPermissions.contains("email"))
            {
              self.getFBUserData()
              //fbLoginManager.logOut()
            }
          }
        }
    }
    @objc func btnGoogle() {
        
        GIDSignIn.sharedInstance().signIn()
    }
    private func localZipToAddress(zip: String?, onSuccess: @escaping (String, String, String) -> Void, onFail: ((Error?) -> Void)?) {

        guard let zip = zip else {
            onFail?(nil)
            return
        }

        CLGeocoder().geocodeAddressString(zip) { placemarks, error in
            if let result = placemarks?.first, let city = result.locality, let state = result.administrativeArea, let country = result.country {
                onSuccess(city, state, country)
            } else {
                onFail?(error)
            }
        }
    }
    
    func redirectToHome(response:LoginRegisterResponse) {
        
        self.view.viewWithTag(2001)?.removeFromSuperview()
        //Toast.show(message: response.ResponseMsg ?? "", controller: self)
        Helper.shared.userId = "\(response.data?.user_id ?? 0)"
        Helper.shared.api_token = response.data?.api_token ?? ""
        Helper.shared.strCity = response.data?.city ?? ""
        Helper.shared.strCityId = "\(response.data?.city_id ?? 0)"
        Helper.shared.strBusinessName = response.data?.business_name ?? ""
        Helper.shared.strUsername = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
        
        if self.isRemember == true {
            UserDefaults.standard.set(true, forKey: "isLogin")
            UserDefaults.standard.set("\(response.data?.user_id ?? 0)", forKey: "userId")
            UserDefaults.standard.set(response.data?.api_token, forKey: "api_token")
            UserDefaults.standard.set(response.data?.email, forKey: "email")
            UserDefaults.standard.set(response.data?.city ?? "", forKey: "city")
            UserDefaults.standard.set("\(response.data?.city_id ?? 0)", forKey: "cityId")
            UserDefaults.standard.set(response.data?.business_name, forKey: "businessName")
            UserDefaults.standard.set("\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")", forKey: "UserName")
            UserDefaults.standard.synchronize()
        } else {
            UserDefaults.standard.set(false, forKey: "isLogin")
            UserDefaults.standard.synchronize()
        }
        
        let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
        self.navigationController?.pushViewController(obj, animated: true)
    }
    //MARK: - Webservice
    func getCategoryAPI() {
        //Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallGetCategory(url: URLs.get_categories, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCategories = response.data?.categories
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            //Helper.shared.hideHUD()
        }
    }
    
    //register in firebase
    func createUserinFirebase(email: String, password: String, callback: @escaping ((Bool) -> ())) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let _ = error{
                
                Toast.show(message: error?.localizedDescription ?? "something is wrong while registering in Firebase", controller: self)
                callback(false)
                return
            }
            callback(true)
            
        }
    }
    
    //login firebase
    func loginToFirebase(email: String, password: String, callback: @escaping ((Bool) -> ())){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let _ = error{
                //Toast.show(message: error?.localizedDescription ?? "something is wrong while login to Firebase", controller: self)
                callback(false)
                return
            }
            callback(true)
        }
    }
    
    func loginAPI() {
        Helper.shared.showHUD()
        let params = ["email":(self.view.viewWithTag(2001) as! SignInView).txtEmail.text!,
                      "password":(self.view.viewWithTag(2001) as! SignInView).txtPassword.text!,
                      "city_id":strCityId] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        print(params)
        NetworkManager.shared.webserviceCallLoginSignUp(url: URLs.login, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.loginToFirebase(email: response.data?.email ?? "", password: "123456") { (isSuccess) in
                    
                    if isSuccess == true {
                        
                        let dict = ["id":Auth.auth().currentUser?.uid ?? "",
                                    "user_id":"\(response.data?.user_id ?? 0)",
                                    "name": "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")",
                                    "online_status":"online"] as? [String : Any]
                        Constants.refs.databaseUsers.child(Auth.auth().currentUser!.uid).setValue(dict)
                        
                        self.redirectToHome(response: response)
                        
                    } else {
                        
                        self.createUserinFirebase(email: response.data?.email ?? "", password: "123456") { (isSuccess) in
                            
                            if isSuccess == true {
                                
                                let dict = ["id":Auth.auth().currentUser?.uid ?? "",
                                            "user_id":"\(response.data?.user_id ?? 0)",
                                            "name": "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")",
                                            "online_status":"online"] as? [String : Any]
                                Constants.refs.databaseUsers.child(Auth.auth().currentUser!.uid).setValue(dict)
                                
                                self.redirectToHome(response: response)
                            }
                        }
                    }
                    
                }
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    func registerAPI() {
        Helper.shared.showHUD()
        let params = ["email":(self.view.viewWithTag(3001) as! SignUpView).txtEmail.text!,
                       "full_name":(self.view.viewWithTag(3001) as! SignUpView).txtName.text!,
                       "password":strPassword!,
                       "phone_no":(self.view.viewWithTag(3001) as! SignUpView).txtPhone.text!,
                       "business_name":(self.view.viewWithTag(3001) as! SignUpView).txtBusinessName.text!,
                       "address":(self.view.viewWithTag(3001) as! SignUpView).txtAddress.text!,
                       "postcode":(self.view.viewWithTag(3001) as! SignUpView).txtZip.text!,
                       "longitude":long,
                       "latitude":lat,
                       "city":(self.view.viewWithTag(3001) as! SignUpView).txtCity.text!,
                       "state":(self.view.viewWithTag(3001) as! SignUpView).txtState.text!,
                       "country":strCountry ?? "",
                       "type_of_business":strCatType!,
                       "website_or_page":(self.view.viewWithTag(3001) as! SignUpView).txtWebsite.text!,
                       "no_of_physical_locations":strLocations!,
                       "how_long_have_you":strHowlong!,
                       "avg_sales_per_month":(self.view.viewWithTag(3001) as! SignUpView).txtSales.text!,
                       "cost_of_goods":strCostGoods!,
                       "city_id":strCityId] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        print(params)
        NetworkManager.shared.webserviceCallLoginSignUp(url: URLs.register, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.view.viewWithTag(3001)?.removeFromSuperview()
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
                let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ThanksVC") as! ThanksVC
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
            Toast.show(message: response.ResponseMsg ?? "", controller: self)
        }
        Helper.shared.hideHUD()
    }
}
    func socialLoginAPI(url:String, params:[String:Any]) {
        
        
        Helper.shared.showHUD()
        let headers = ["Accept":"application/json"]
        NetworkManager.shared.webserviceCallLoginSignUp(url: url, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Helper.shared.userId = "\(response.data?.user_id ?? 0)"
                Helper.shared.api_token = response.data?.api_token ?? ""
                Helper.shared.strCity = response.data?.city ?? ""
                Helper.shared.strCityId = "\(response.data?.city_id ?? 0)"
                Helper.shared.strBusinessName = response.data?.business_name ?? ""
                Helper.shared.strUsername = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
//                Helper.shared.profile_pic = response.data?.profile_pic ?? ""
//                Helper.shared.firebase_token = response.data?.firebase_token ?? ""
//                Helper.shared.user_name = "\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")"
                UserDefaults.standard.set(true, forKey: "isLogin")
                UserDefaults.standard.set("\(response.data?.user_id ?? 0)", forKey: "userId")
                UserDefaults.standard.set(response.data?.api_token, forKey: "api_token")
                UserDefaults.standard.set(response.data?.email, forKey: "email")
                UserDefaults.standard.set(response.data?.city ?? "", forKey: "city")
                UserDefaults.standard.set("\(response.data?.city_id ?? 0)", forKey: "cityId")
                UserDefaults.standard.set(response.data?.business_name, forKey: "businessName")
                UserDefaults.standard.set("\(response.data?.first_name ?? "") \(response.data?.last_name ?? "")", forKey: "UserName")
                UserDefaults.standard.synchronize()
                
                let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "tabbar") as! UITabBarController
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnSignIn(_ sender: Any) {
        
        btnSignIn.titleLabel?.textColor = #colorLiteral(red: 0.02927529439, green: 0.3647535443, blue: 0.7482612133, alpha: 1)
        self.view.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.02927529439, green: 0.3647535443, blue: 0.7482612133, alpha: 1)
        btnSignUp.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        self.view.viewWithTag(2)?.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        self.view.viewWithTag(2001)?.removeFromSuperview()
        let customView = Bundle.main.loadNibNamed("SignInView", owner: self, options: nil)?.first as! SignInView
        customView.btnSignInAction.addTarget(self, action: #selector(btnSignInAction), for: .touchUpInside)
        customView.btnFb.addTarget(self, action: #selector(btnFb), for: .touchUpInside)
        customView.btnGoogle.addTarget(self, action: #selector(btnGoogle), for: .touchUpInside)
        customView.btnForgot.addTarget(self, action: #selector(btnForgot), for: .touchUpInside)
        customView.btnCheckbox.addTarget(self, action: #selector(btnRemember(sender:)), for: .touchUpInside)
        customView.tag = 2001
        
        customView.frame = self.viewBg.frame
        self.view.addSubview(customView)
    }
    @IBAction func btnSignUp(_ sender: Any) {
        btnSignIn.titleLabel?.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.view.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnSignUp.setTitleColor(#colorLiteral(red: 0.02927529439, green: 0.3647535443, blue: 0.7482612133, alpha: 1), for: .normal)
        self.view.viewWithTag(2)?.backgroundColor = #colorLiteral(red: 0.02927529439, green: 0.3647535443, blue: 0.7482612133, alpha: 1)
        
        self.view.viewWithTag(2001)?.removeFromSuperview()
        let customView = Bundle.main.loadNibNamed("PreSignUpView", owner: self, options: nil)?.first as! PreSignUpView
        customView.btnSignUp.addTarget(self, action: #selector(btnPreSignUpAction), for: .touchUpInside)
        customView.btnFb.addTarget(self, action: #selector(btnFb), for: .touchUpInside)
        customView.btnGoogle.addTarget(self, action: #selector(btnGoogle), for: .touchUpInside)
        customView.tag = 2001
        
        customView.frame = self.viewBg.frame
        self.view.addSubview(customView)
    }
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
    }
}
extension LoginVC:UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if txtfieldTag == 9 {
            return arrCategories?.count ?? 0
        }
        if txtfieldTag == 10 {
            return arrCostGoods.count
        }
        if txtfieldTag == 12 {
            return arrLocations.count
        }
        if txtfieldTag == 13 {
            return arrHowlong.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if txtfieldTag == 9 {
            return arrCategories?[row].category_name
        }
        if txtfieldTag == 10 {
            return arrCostGoods[row]
        }
        if txtfieldTag == 12 {
            return arrLocations[row]
        }
        if txtfieldTag == 13 {
            return arrHowlong[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        intSelectedRow = row
    }
    
    
}
extension LoginVC:UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 9 || textField.tag == 10 || textField.tag == 12 || textField.tag == 13 {
            return false
        }
        if textField.tag == 6 {
            
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length == 5 {
                localZipToAddress(zip: "US\(textField.text ?? "")\(string)") { (city, state, country) in
                    
                    (self.view.viewWithTag(3001) as! SignUpView).txtCity.text = city
                    (self.view.viewWithTag(3001) as! SignUpView).txtState.text = state
                    self.strCountry = country
                } onFail: { (error) in
                    print(error)
                }
            }
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 9 || textField.tag == 10 || textField.tag == 12 || textField.tag == 13 {
            txtfieldTag = textField.tag
            txtfield = textField
            
            picker = Helper.shared.myPickerView(vc: self, txtfield: textField)
            toolBar()
        }
        self.picker?.reloadAllComponents()
    }
}
extension LoginVC:CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        lat = "\(locValue.latitude)"
        long = "\(locValue.longitude)"
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}
extension LoginVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let email = user.profile.email
        
        if user.profile.hasImage
        {
            let pic = user.profile.imageURL(withDimension: 100) //100 dimension which can be varied according to need
            print(pic)
        }
        
        var dict:[String:Any] = [:]
        dict["first_name"] = user.profile.givenName
        dict["last_name"] = user.profile.familyName
        dict["email"] = user.profile.email
        dict["google_id"] = "\(user.userID!)"
        dict["city_id"] = strCityId
        
        GIDSignIn.sharedInstance()?.signOut()
        socialLoginAPI(url: URLs.login_with_google, params: dict)
        
        
    }
}
