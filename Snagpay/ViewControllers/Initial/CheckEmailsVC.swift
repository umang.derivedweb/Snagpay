//
//  CheckEmailsVC.swift
//  Snagpay
//
//  Created by Apple on 02/02/21.
//

import UIKit

class CheckEmailsVC: UIViewController {

    // MARK: - Variables
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnEmail: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Function
    func setupUI() {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            btnEmail.layer.cornerRadius = 30
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}

