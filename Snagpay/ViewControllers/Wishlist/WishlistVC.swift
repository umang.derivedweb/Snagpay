//
//  WishlistVC.swift
//  Snagpay
//
//  Created by Apple on 10/02/21.
//

import UIKit

class WishlistVC: UIViewController {

    // MARK: - Variable
    var arrDeleteWishlist:[String] = []
    var arrDealId:[Int] = []
    var arrWishlist:[DealsListData]?
    var arrRecentlyViewed:[DealsListData]?
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblSelectAll: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        getWishlistAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapSelectAll(sender:)))
        lblSelectAll.addGestureRecognizer(tap)
    }
    
    @objc func tapSelectAll(sender:UITapGestureRecognizer) {
        print("tap working")
        
        self.arrDeleteWishlist.removeAll()
        for _ in 0..<(self.arrWishlist?.count ?? 0) {
            
            self.arrDeleteWishlist.append("1")
        }
        
        self.table.reloadData()
    }
    
    @objc func editButtonTapped(sender: UIButton) {
        //Button Tapped and open your another ViewController

    }
    //MARK: - Webservice
    func getWishlistAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        NetworkManager.shared.webserviceCallGetWishlist(url: "\(URLs.get_wishlist)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrWishlist = response.data?.wishlist
                self.arrRecentlyViewed = response.data?.recently_viewed
                
                self.lblSelectAll.text =  "Saved Items(\(self.arrWishlist?.count ?? 0))"
                self.btnEdit.isHidden = false
                self.btnDelete.isHidden = true
                self.widthConstraint.constant = 0
                
                self.arrDeleteWishlist.removeAll()
                for _ in 0..<(self.arrWishlist?.count ?? 0) {
                    
                    self.arrDeleteWishlist.append("0")
                }
                
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func removeWishlistAPI() {
        Helper.shared.showHUD()
        
        let params = ["deal_ids":arrDealId] as [String : Any]
        let headers = ["Accept":"application/json",
                       "Authorization":"Bearer \(Helper.shared.api_token)"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.remove_wishlist_deals, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.btnCancel.sendActions(for: .touchUpInside)
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
                self.getWishlistAPI()
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction
    @IBAction func btnCancel(_ sender: Any) {
        if lblSelectAll.text == "Select All" {
            lblSelectAll.text = "Saved Items(\(arrWishlist?.count ?? 0))"
            btnEdit.isHidden = false
            btnDelete.isHidden = true
            widthConstraint.constant = 0
        } else {
            lblSelectAll.text = "Select All"
            btnEdit.isHidden = true
            btnDelete.isHidden = false
            widthConstraint.constant = 20
        }
        table.reloadData()
    }
    @IBAction func btnDelete(_ sender: Any) {
        if arrDeleteWishlist.count == 0 {
            return
        }
        arrDealId.removeAll()
        for i in 0..<arrDeleteWishlist.count {
            
            if arrDeleteWishlist[i] == "1" {
                arrDealId.append((arrWishlist?[i].deal_id)!)
            }
        }
        removeWishlistAPI()
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        if lblSelectAll.text == "Select All" {
            lblSelectAll.text = "Saved Items(\(arrWishlist?.count ?? 0)"
            btnEdit.isHidden = false
            btnDelete.isHidden = true
            widthConstraint.constant = 0
        } else {
            lblSelectAll.text = "Select All"
            btnEdit.isHidden = true
            btnDelete.isHidden = false
            widthConstraint.constant = 20
        }
        table.reloadData()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension WishlistVC:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrWishlist?.count ?? 0
        }
        return arrRecentlyViewed?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell") as! WishlistCell
        
        if indexPath.section == 0 {
            if lblSelectAll.text == "Select All" {
                if arrDeleteWishlist[indexPath.row] == "1" {
                    cell.btnSelect.setImage(#imageLiteral(resourceName: "check-mark.png"), for: .normal)
                } else {
                    cell.btnSelect.setImage(#imageLiteral(resourceName: "unCheck-mark.png"), for: .normal)
                }
                cell.viewBg.isHidden = false
                cell.btnSelect.isHidden = false
            } else {
                cell.viewBg.isHidden = true
                cell.btnSelect.isHidden = true
            }
            
            cell.lblTitle.text = "\(arrWishlist?[indexPath.row].company_name ?? "")\n\(arrWishlist?[indexPath.row].title ?? "")"
            cell.lblBusiness.text = arrWishlist?[indexPath.row].business_name
            cell.lblDate.text = arrWishlist?[indexPath.row].deal_date
            cell.lblDesc.text = "\(arrWishlist?[indexPath.row].bought ?? "")+ Bought"
            cell.lblPrice.text = "$ \(arrWishlist?[indexPath.row].sell_price ?? 0)"
            cell.img.sd_setImage(with: URL(string: arrWishlist?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            
        } else {
            cell.viewBg.isHidden = true
            cell.btnSelect.isHidden = true
            cell.lblTitle.text = "\(arrRecentlyViewed?[indexPath.row].company_name ?? "")\n\(arrRecentlyViewed?[indexPath.row].title ?? "")"
            cell.lblBusiness.text = arrRecentlyViewed?[indexPath.row].business_name
            cell.lblDate.text = arrRecentlyViewed?[indexPath.row].deal_date
            cell.lblDesc.text = "\(arrRecentlyViewed?[indexPath.row].bought ?? "")+ Bought"
            cell.lblPrice.text = "$ \(arrRecentlyViewed?[indexPath.row].sell_price ?? 0)"
            cell.img.sd_setImage(with: URL(string: arrRecentlyViewed?[indexPath.row].deal_image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            if lblSelectAll.text == "Select All" {
                
                let str = arrDeleteWishlist[indexPath.row]
                
                arrDeleteWishlist.remove(at: indexPath.row)
                
                if str == "0" {
                    arrDeleteWishlist.insert("1", at: indexPath.row)
                } else {
                    arrDeleteWishlist.insert("0", at: indexPath.row)
                }
                table.reloadData()
            } else {
                
                let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
                obj.strDealId = "\(arrWishlist?[indexPath.row].deal_id ?? 0)"
                navigationController?.pushViewController(obj, animated: true)
            }
            
        } else {
            
            let obj = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ItemDetailsVC") as! ItemDetailsVC
            obj.strDealId = "\(arrRecentlyViewed?[indexPath.row].deal_id ?? 0)"
            navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = UIColor.white
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-100, height: headerView.frame.height-10)
        
        if section == 0 {
            
        } else {
            label.text = "Recently Viewed"
            let bottomView: UILabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: headerView.frame.width, height: 4)) //
            bottomView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            headerView.addSubview(bottomView)
        }
        
        label.font = UIFont(name:"Roboto-Bold", size: 17.0) // my custom font
        label.textColor = UIColor.black // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
           return 0
        }
        return 50
    }
}
